﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using VKMessages.API;
using VKMessages.API.JSON;
using VKMessages.Tools;

namespace VKMessages.ViewModels
{
    public enum LoginStatus
    {
        WaitingForInput,
        LoggingIn,
        CheckingToken,
        InvalidCredentials,
        NetProblem
    }

    public class LoginViewModel : BaseViewModel
    {
        public bool IsLoggingOut { get; set; }

        public LoginViewModel()
        {
            logIn = new Commands.DelegateCommand(StartLogIn);
            signUp = new Commands.DelegateCommand(StartSignUp);

            if (!IsLoggingOut && !string.IsNullOrEmpty(SharedPreferences.AccessToken))
            {
                Status = LoginStatus.CheckingToken;
                VKApi.CheckToken(CheckTokenComplited);
            }
            else
            {
                Status = LoginStatus.WaitingForInput;
            }

            IsLoggingOut = false;
        }

        public void PageLoaded(object sender, System.Windows.RoutedEventArgs e)
        {
            if (!IsLoggingOut && !string.IsNullOrEmpty(SharedPreferences.AccessToken))
            {
                Status = LoginStatus.CheckingToken;
                VKApi.CheckToken(CheckTokenComplited);
            }
            else
            {
                Status = LoginStatus.WaitingForInput;
            }

            IsLoggingOut = false;
        }

        private void CheckTokenComplited(object sender, VKApiOperationCompletedEventArgs e)
        {
            if (e.Cancelled || !e.IsSuccess)
            {
                Status = LoginStatus.WaitingForInput;
            }
            else
            {
                VKApi.ConnectToLongPool(ConnectToLongPoolComplited);
            }
        }

        private void GetTokenComplited(object sender, VKApiOperationCompletedEventArgs e)
        {
            if (e.Cancelled || !e.IsSuccess)
            {
                if (e.ErrorCode == 0 || e.ErrorCode == 113)
                    Status = LoginStatus.InvalidCredentials;
                else
                    Status = LoginStatus.NetProblem;
            }
            else
            {
                VKApi.ConnectToLongPool(ConnectToLongPoolComplited);
            }
        }

        private void ConnectToLongPoolComplited(object sender, VKApiOperationCompletedEventArgs e)
        {
            if (e.Cancelled || !e.IsSuccess)
            {
                if (e.ErrorCode == 0 || e.ErrorCode == 113)
                    Status = LoginStatus.InvalidCredentials;
                else
                    Status = LoginStatus.NetProblem;
            }
            else
            {
                var data = e.Result as JsonLongPoolData;
                if (data != null)
                    CrossPageParameters.LongPoolData = data;

                Navigate("MainPage");
            }
        }

        private readonly ICommand logIn;
        public ICommand LogIn
        {
            get { return logIn; }
        }

        private readonly ICommand signUp;
        public ICommand SignUp
        {
            get { return signUp; }
        }

        public string ErrorText
        {
            get
            {
                switch (Status)
                {
                    case LoginStatus.InvalidCredentials:
                        return Resource.LOGIN_INVALID_CREDENTIALS;
                    case LoginStatus.NetProblem:
                        return Resource.LOGIN_NETPROBLEM;
                    default:
                        return string.Empty;
                }
            }
            set
            {
                NotifyPropertyChanged("ErrorText");
            }
        }

        public string ProgressBarText
        {
            get
            {
                switch (Status)
                {
                    case LoginStatus.LoggingIn:
                    case LoginStatus.CheckingToken:
                        return Resource.PBAR_CONNECTING;
                    default:
                        return string.Empty;
                }
            }
            set
            {
                NotifyPropertyChanged("ProgressBarText");
            }
        }

        public Visibility PageVisible
        {
            get
            {
                return Status == LoginStatus.CheckingToken ? Visibility.Collapsed : Visibility.Visible;
            }
            set
            {
                NotifyPropertyChanged("PageVisible");
            }
        }

        public bool ProgressBarVisible
        {
            get
            {
                return Status == LoginStatus.CheckingToken || Status == LoginStatus.LoggingIn;
            }
            set
            {
                NotifyPropertyChanged("ProgressBarVisible");
            }
        }

        private string login;
        public string Login
        {
            get
            {
                return login;
            }
            set
            {
                if (value != login)
                {
                    login = value;
                    NotifyPropertyChanged("Login");
                }
            }
        }

        private string password;
        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                if (value != password)
                {
                    password = value;
                    NotifyPropertyChanged("Password");
                }
            }
        }

        private LoginStatus status;
        public LoginStatus Status
        {
            get
            {
                return status;
            }
            set
            {
                if (value != status)
                {
                    status = value;
                    NotifyPropertyChanged("Status");
                    NotifyPropertyChanged("PageVisible");
                    NotifyPropertyChanged("ErrorText");
                    NotifyPropertyChanged("ProgressBarVisible");
                    NotifyPropertyChanged("ProgressBarText");                    
                }
            }
        }

        public void StartLogIn(object o)
        {
            Status = LoginStatus.LoggingIn;
            VKApi.RequestAccessToken(Login, Password, GetTokenComplited);
        }

        public void StartSignUp(object o)
        {
            Navigate("SignUpPage");
        }
    }
}
