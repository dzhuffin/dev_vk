﻿using System.Collections.ObjectModel;
using Microsoft.Phone.UserData;
using VKMessages.Tools;

namespace VKMessages.ViewModels
{
    public class ContactCategoryViewModel : BaseViewModel
    {
        public ContactCategoryViewModel()
        {
            Items = new ObservableCollection<Contact>();
        }

        private string header;
        public string Header
        {
            get
            {
                return header;
            }
            set
            {
                if (value != header)
                {
                    header = value;
                    NotifyPropertyChanged("Header");
                }
            }
        }

        private Contact selectedContact;
        public Contact SelectedContact
        {
            get
            {
                return selectedContact;
            }
            set
            {
                NotifyPropertyChanged("selectedFriend");

                CrossPageParameters.AttachmentsCount = 0;
                Navigate("ConversationPage");
                //if (value != selectedContact)
                //{
                //    selectedContact = value;
                //    NotifyPropertyChanged("selectedFriend");
                //}
            }
        }

        public ObservableCollection<Contact> Items { get; set; }
    }
}
