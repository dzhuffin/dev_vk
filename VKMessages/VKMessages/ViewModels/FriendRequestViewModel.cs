﻿using System;
using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Microsoft.Phone.Shell;
using VKMessages.API;
using VKMessages.Tools;

namespace VKMessages.ViewModels
{
    public enum FriendRequestStatus
    {
        WaitingForInput,
        SendingRequest,
        RecievingData,
        NetProblem
    }

    public class FriendRequestViewModel : BaseViewModel
    {
        public FriendRequestViewModel()
        {
            add = new Commands.DelegateCommand(StartAdd);
            deny = new Commands.DelegateCommand(StartDeny);
        }

        private readonly ICommand add;

        public ICommand Add
        {
            get { return add; }
        }

        private readonly ICommand deny;

        public ICommand Deny
        {
            get { return deny; }
        }

        public bool ProgressBarVisible
        {
            get { return Status != FriendRequestStatus.WaitingForInput; }
            set { NotifyPropertyChanged("ProgressBarVisible"); }
        }

        public ApplicationBarMenuItem DeleteFriendItem { get; set; }

        public string ProgressBarText
        {
            get
            {
                switch (Status)
                {
                    case FriendRequestStatus.RecievingData:
                        return Resource.PBAR_RECEIVING_DATA;
                    case FriendRequestStatus.SendingRequest:
                        return Resource.PBAR_SENDING_REQUEST;
                    case FriendRequestStatus.NetProblem:
                        return Resource.PBAR_CONNECTING;
                    default:
                        return string.Empty;
                }
            }
            set { NotifyPropertyChanged("ProgressBarText"); }
        }

        private FriendRequestStatus status = FriendRequestStatus.WaitingForInput;

        public FriendRequestStatus Status
        {
            get { return status; }
            set
            {
                if (value != status)
                {
                    status = value;
                    NotifyPropertyChanged("Status");
                    NotifyPropertyChanged("ProgressBarVisible");
                    NotifyPropertyChanged("ProgressBarText");
                }
            }
        }

        public string ContactName
        {
            get
            {
                if (CrossPageParameters.CurrentFriendVM != null)
                    return CrossPageParameters.CurrentFriendVM.VKName;

                return string.Empty;
            }
        }

        private WriteableBitmap image;
        public WriteableBitmap Image
        {
            get
            {
                return image;
            }
            set
            {
                if (image != value)
                {
                    image = value;
                    NotifyPropertyChanged("Image");
                }
            }
        }

        public void PageLoaded(object sender, RoutedEventArgs e)
        {
            Image = null;

            Status = FriendRequestStatus.RecievingData;

            var request = (HttpWebRequest)WebRequest.Create(CrossPageParameters.CurrentFriendVM.Image100X100Url);
            request.AllowReadStreamBuffering = false;
            request.BeginGetResponse(DownloadImageCompleted, new HttpRequestParams { Request = request, Param = CrossPageParameters.CurrentFriendVM });
        }

        private void DownloadImageCompleted(IAsyncResult result)
        {
            var param = result.AsyncState as HttpRequestParams;

            if (param == null)
                return;

            var item = param.Param as FriendItemViewModel;

            if (item == null)
            {
                return;
            }

            if (param.Request == null)
                return;

            Stream responseStream;
            HttpWebResponse response;

            try
            {
                response = (HttpWebResponse)param.Request.EndGetResponse(result);
                responseStream = response.GetResponseStream();
            }
            catch (Exception)
            {
                return;
            }

            if (responseStream.CanRead)
            {
                item.Image100X100Data = new byte[response.ContentLength];
                responseStream.Read(item.Image100X100Data, 0, item.Image100X100Data.Length);

                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    lock (item.Image100X100Data)
                    {
                        try
                        {
                            var stream = new MemoryStream(item.Image100X100Data);
                            Image = Microsoft.Phone.PictureDecoder.DecodeJpeg(stream);
                            Status = FriendRequestStatus.WaitingForInput;

                            stream.Close();
                        }
                        catch (Exception)
                        {
                        }
                    }
                });
            }

            responseStream.Close();            
        }

        public void StartAdd(object o)
        {
            Status = FriendRequestStatus.SendingRequest;
            VKApi.FriendAdd(CrossPageParameters.CurrentFriendVM.UID, FriendAddComplited);
        }

        public void StartDeny(object o)
        {
            Status = FriendRequestStatus.SendingRequest;
            VKApi.FriendDelete(CrossPageParameters.CurrentFriendVM.UID, FriendDeleteComplited);
        }

        private void FriendAddComplited(object sender, VKApiOperationCompletedEventArgs e)
        {
            Status = FriendRequestStatus.WaitingForInput;

            if (!e.Cancelled && e.IsSuccess)
            {
                App.FriendsRequestViewModel.Refresh();
                MessageBox.Show(Resource.OPERATION_SUCCESS);
                GoBack();
            }
            else
            {
                MessageBox.Show(Resource.ERROR_OCCURRED);
            }
        }

        private void FriendDeleteComplited(object sender, VKApiOperationCompletedEventArgs e)
        {
            Status = FriendRequestStatus.WaitingForInput;

            if (!e.Cancelled && e.IsSuccess)
            {
                App.FriendsRequestViewModel.Refresh();
                MessageBox.Show(Resource.OPERATION_SUCCESS);
                GoBack();
            }
            else
            {
                MessageBox.Show(Resource.ERROR_OCCURRED);
            }
        }
    }
}
