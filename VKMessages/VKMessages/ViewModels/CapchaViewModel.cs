﻿using System;
using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using VKMessages.API;
using VKMessages.Tools;

namespace VKMessages.ViewModels
{
    public class CapchaViewModel : BaseViewModel
    {
        public CapchaViewModel()
        {
            send = new Commands.DelegateCommand(StartSend);
            cancel = new Commands.DelegateCommand(StartCancel);
        }

        private readonly ICommand send;

        public ICommand Send
        {
            get { return send; }
        }

        private readonly ICommand cancel;

        public ICommand Cancel
        {
            get { return cancel; }
        }

        private byte[] imageData;
        public byte[] ImageData
        {
            get { return imageData; }
            set
            {
                imageData = value;

                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    lock (imageData)
                    {
                        try
                        {
                            var stream = new MemoryStream(imageData);

                            Image = Microsoft.Phone.PictureDecoder.DecodeJpeg(stream);

                            stream.Close();
                        }
                        catch (Exception)
                        {
                        }
                    }
                });
            }
        }
        private WriteableBitmap image;
        public WriteableBitmap Image
        {
            get
            {
                return image;
            }
            set
            {
                if (value != image)
                {
                    image = value;
                    NotifyPropertyChanged("Image");
                }
            }
        }

        private string code = string.Empty;
        public string Code
        {
            get
            {
                return code;
            }
            set
            {
                if (value != code)
                {
                    code = value;
                    NotifyPropertyChanged("Code");
                }
            }
        }

        public void PageLoaded(object sender, RoutedEventArgs e)
        {
            var request = (HttpWebRequest)WebRequest.Create(CrossPageParameters.LastError.CaptchaImageUrl);
            request.AllowReadStreamBuffering = false;
            request.BeginGetResponse(DownloadImageCompleted, new HttpRequestParams { Request = request });
        }

        public void StartSend(object o)
        {
            VKApi.ByRequestParams(Code, CrossPageParameters.LastError, ByRequestParamsComplited);
        }

        private void DownloadImageCompleted(IAsyncResult result)
        {
            var param = result.AsyncState as HttpRequestParams;

            if (param == null || param.Request == null)
                return;

            Stream responseStream;
            HttpWebResponse response;

            try
            {
                response = (HttpWebResponse)param.Request.EndGetResponse(result);
                responseStream = response.GetResponseStream();
            }
            catch (Exception)
            {
                return;
            }

            if (responseStream.CanRead)
            {
                ImageData = new byte[response.ContentLength];
                responseStream.Read(ImageData, 0, ImageData.Length);
            }

            responseStream.Close();
        }

        public void StartCancel(object o)
        {
            GoBack();
        }

        private void ByRequestParamsComplited(object sender, VKApiOperationCompletedEventArgs e)
        {
            GoBack();
        }
    }
}
