﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Tasks;
using VKMessages.API;
using VKMessages.API.JSON;
using VKMessages.DB.Entities;
using VKMessages.Tools;

namespace VKMessages.ViewModels
{
    public enum AttachmentsStatus
    {
        WaitingForInput,
        Uploading,
        NetProblem
    }    

    public class UploadAttachmentParams
    {
        public HttpWebRequest Request;
        public int AttachmentIndex;
    }

    public class AttachmentsViewModel : BaseViewModel
    {
        public AttachmentsViewModel()
        {
            
        }

        public string AttachmentsCountText
        {
            get
            {
                if (Attachments.Count == 0)
                    return Resource.ATTACHMENTS_NO;

                return Attachments.Count + " " + Resource.ATTACHMENTS_COUNT;
            }
        }

        private AttachmentsStatus status = AttachmentsStatus.WaitingForInput;
        public AttachmentsStatus Status
        {
            get
            {
                return status;
            }
            set
            {
                if (value != status)
                {
                    status = value;
                    NotifyPropertyChanged("Status");
                    NotifyPropertyChanged("ProgressBarVisible");
                    NotifyPropertyChanged("ProgressBarText");
                }
            }
        }

        public bool ProgressBarVisible
        {
            get
            {
                return Status != AttachmentsStatus.WaitingForInput;
            }
            set
            {
                NotifyPropertyChanged("ProgressBarVisible");
            }
        }

        public string ProgressBarText
        {
            get
            {
                switch (Status)
                {
                    case AttachmentsStatus.Uploading:
                        return Resource.APPBAR_UPLOADING_ATTCHMENTS;
                    case AttachmentsStatus.NetProblem:
                        return Resource.PBAR_CONNECTING;
                    default:
                        return string.Empty;
                }
            }
            set
            {
                NotifyPropertyChanged("ProgressBarText");
            }
        }

        private ObservableCollection<Attachment> attachments = new ObservableCollection<Attachment>();
        public ObservableCollection<Attachment> Attachments
        {
            get
            {
                return attachments;
            }
            set
            {
                if (value != attachments)
                {
                    attachments = value;
                    NotifyPropertyChanged("Attachments");
                }
            }
        }

        public void PageLoaded(object sender, RoutedEventArgs e)
        {
            messageServer = string.Empty;
            VKApi.GetMessagesUploadServer(GetMessagesUploadServerComplited); 
        }

        public string LocationImageUrl
        {
            get
            {
                return string.Format("http://maps.googleapis.com/maps/api/staticmap?center={0},{1}&zoom=12&size=230x230&sensor=false&language={2}",
                    CrossPageParameters.PickedLocation.Latitude.ToString(CultureInfo.InvariantCulture).Replace(',', '.'),
                    CrossPageParameters.PickedLocation.Longitude.ToString(CultureInfo.InvariantCulture).Replace(',', '.'),
                                     CultureInfo.CurrentCulture.TwoLetterISOLanguageName);
            }
        }

        public byte[] LocationImageData { get; set; }

        private void DownloadImageCompleted(IAsyncResult result)
        {
            var param = result.AsyncState as HttpRequestParams;

            if (param == null || param.Request == null)
                return;

            Stream responseStream;
            HttpWebResponse response;

            try
            {
                response = (HttpWebResponse)param.Request.EndGetResponse(result);
                responseStream = response.GetResponseStream();
            }
            catch (Exception)
            {
                return;
            }

            if (responseStream.CanRead)
            {
                LocationImageData = new byte[response.ContentLength];
                responseStream.Read(LocationImageData, 0, LocationImageData.Length);

                var attachment = new Attachment
                                     {
                                         Type = AttachmentType.Location,
                                         Latitude = CrossPageParameters.PickedLocation.Latitude.ToString(CultureInfo.InvariantCulture).Replace(',', '.'),
                                         Longitude = CrossPageParameters.PickedLocation.Longitude.ToString(CultureInfo.InvariantCulture).Replace(',', '.'),
                                         ImageData = LocationImageData
                                     };

                Deployment.Current.Dispatcher.BeginInvoke(() =>
                                                              {
                                                                  Attachments.Add(attachment);
                                                                  AddToPanel(attachment);
                                                              });
            }

            responseStream.Close();
        }

        private void GetMessagesUploadServerComplited(object sender, VKApiOperationCompletedEventArgs e)
        {
            if (CrossPageParameters.PickedLocation != null)
            {
                var request = (HttpWebRequest)WebRequest.Create(LocationImageUrl);
                request.AllowReadStreamBuffering = false;
                request.BeginGetResponse(DownloadImageCompleted, new HttpRequestParams { Request = request });
            }

            messageServer = string.Empty;

            if (!e.Cancelled && e.Error == null && e.IsSuccess)
            {
                var jsonProfileServer = e.Result as JsonProfileUploadServer;
                if (jsonProfileServer != null)
                {
                    messageServer = jsonProfileServer.Server;
                }
            }
        }

        private WrapPanel panel;
        public void WrapPanelLoaded(object sender, RoutedEventArgs e)
        {
            if (CrossPageParameters.PickedLocation != null)
                return;

            panel = sender as WrapPanel;

            if (panel == null)
                return;

            if (CrossPageParameters.AttachmentsCount < 1)
                Attachments.Clear();
            else
            {
                foreach (var attachment in Attachments)
                    AddToPanel(attachment);
            }
        }

        public void AppBarAddPhoto(object sender, EventArgs e)
        {
            var photoChooserTask = new PhotoChooserTask();
            photoChooserTask.Completed += PhotoChooserTaskCompleted;

            try
            {
                photoChooserTask.Show();
            }
            catch (InvalidOperationException)
            {
                MessageBox.Show(Resource.ERROR_OCCURRED);
            }
        }

        public BuildApplicationBarDelegate BuildApplicationBar;

        private void AddToPanel(Attachment attachment)
        {
            if (attachment.Type == AttachmentType.Location && BuildApplicationBar != null)
                BuildApplicationBar.Invoke();

            var grid = new Grid {Margin = new Thickness(0, 5, 5, 0), Tag = attachment};
            var image = new Image { Source = DBEntityBase.ByteArrayToImage(attachment.ImageData), Height = 230, Width = 230, Stretch = Stretch.Fill};

            var bitmapImage = new BitmapImage(new Uri("/Images/Components/Appbar_Icons/attachment.delete.png", UriKind.Relative));
            var closeImage = new Image
                                 {
                                     Margin = new Thickness(230 - 48 - 5, 5, 5, 230 - 48 - 5),
                                     Stretch = Stretch.Fill,
                                     Width = 48,
                                     Height = 48,
                                     HorizontalAlignment = HorizontalAlignment.Center,
                                     VerticalAlignment = VerticalAlignment.Center,
                                     Source = bitmapImage
                                 };
            closeImage.MouseLeftButtonUp += closeImageTap;
            grid.Children.Add(image);
            grid.Children.Add(closeImage);
            panel.Children.Add(grid);

            CrossPageParameters.AttachmentsCount = Attachments.Count;
            NotifyPropertyChanged("AttachmentsCountText");
        }

        void closeImageTap(object sender, MouseButtonEventArgs e)
        {
            var image = sender as Image;
            if (image == null)
                return;

            var grid = image.Parent as Grid;
            if (grid == null)
                return;

            var attachment = grid.Tag as Attachment;
            if (attachment == null)
                return;

            Attachments.Remove(attachment);
            panel.Children.Remove(grid);

            if (BuildApplicationBar != null)
                BuildApplicationBar.Invoke();

            CrossPageParameters.AttachmentsCount = Attachments.Count;
            NotifyPropertyChanged("AttachmentsCountText");
        }

        private string messageServer;
        private string _boundary = String.Format("--{0}", Guid.NewGuid().ToString().Replace("-", string.Empty));
        private const string _templateFile = "--{0}\r\nContent-Disposition: form-data; name=\"{1}\"; filename=\"{2}\"\r\nContent-Type: {3}\r\n\r\n";
        private const string _templateEnd = "--{0}--\r\n\r\n";
        private void PhotoChooserTaskCompleted(object sender, PhotoResult e)
        {
            if (e.TaskResult == TaskResult.OK)
            {
                var newImage = new Attachment {ImageData = new byte[e.ChosenPhoto.Length], Type = AttachmentType.Photo};
                e.ChosenPhoto.Read(newImage.ImageData, 0, newImage.ImageData.Length);

                Attachments.Add(newImage);

                AddToPanel(newImage);
            }
        }

        private VKApi.VKApiOperationCompletedEventHandler uploadEventHandler;
        public void StartUploadAttachments(VKApi.VKApiOperationCompletedEventHandler eventHandler)
        {
            uploadEventHandler = eventHandler;

            if (Attachments.Count < 1)
                return;

            try
            {
                var index = -1;
                currentAttachmentIndex = -1;
                for (var i = 0; i < Attachments.Count; i++)
                {
                    if (Attachments[i].Type == AttachmentType.Photo)
                    {
                        index = i;
                        currentAttachmentIndex = i;
                        break;
                    }
                }

                if (index == -1)
                    return;

                Status = AttachmentsStatus.Uploading;

                var request = (HttpWebRequest)WebRequest.Create(messageServer);

                request.Method = "POST";
                request.ContentType = string.Format("multipart/form-data; boundary={0}", _boundary);
                request.BeginGetRequestStream(UploadGetStreamCallback, new UploadAttachmentParams { Request = request, AttachmentIndex = index });
            }
            catch (Exception)
            {

            }
        }

        private void UploadGetStreamCallback(IAsyncResult result)
        {
            var p = result.AsyncState as UploadAttachmentParams;
            if (p == null)
            {
                MessageBox.Show(Resource.ERROR_OCCURRED);
                Deployment.Current.Dispatcher.BeginInvoke(() => { Status = AttachmentsStatus.WaitingForInput; });
                return;
            }
            var req = p.Request;
            if (req == null)
            {
                MessageBox.Show(Resource.ERROR_OCCURRED);
                Deployment.Current.Dispatcher.BeginInvoke(() => { Status = AttachmentsStatus.WaitingForInput; });
                return;
            }
            var postStream = req.EndGetRequestStream(result);
            var FilePath = "vkmsg.jpg";
            var FileType = "application/octet-stream";
            var Name = "file1";
            var contentFile = Encoding.UTF8.GetBytes(String.Format(_templateFile, _boundary, Name, FilePath, FileType));
            postStream.Write(contentFile, 0, contentFile.Length);
            postStream.Write(Attachments[p.AttachmentIndex].ImageData, 0, Attachments[p.AttachmentIndex].ImageData.Length);
            var _lineFeed = Encoding.UTF8.GetBytes("\r\n");
            postStream.Write(_lineFeed, 0, _lineFeed.Length);
            var contentEnd = Encoding.UTF8.GetBytes(String.Format(_templateEnd, _boundary));
            postStream.Write(contentEnd, 0, contentEnd.Length);
            postStream.Close();

            req.BeginGetResponse(UploadComplitedCallback, p);
        }

        private void UploadComplitedCallback(IAsyncResult result)
        {
            var p = result.AsyncState as UploadAttachmentParams;
            if (p == null)
            {
                MessageBox.Show(Resource.ERROR_OCCURRED);
                Deployment.Current.Dispatcher.BeginInvoke(() => { Status = AttachmentsStatus.WaitingForInput; });
                return;
            }
            var req = p.Request;
            if (req == null)
            {
                MessageBox.Show(Resource.ERROR_OCCURRED);
                Deployment.Current.Dispatcher.BeginInvoke(() => { Status = AttachmentsStatus.WaitingForInput; });
                return;
            }
            var webResponse = req.EndGetResponse(result);

            var read = new StreamReader(webResponse.GetResponseStream());

            var serializer = new DataContractJsonSerializer(typeof(JsonUploadPhotoResponse));

            var stream = new MemoryStream(Encoding.UTF8.GetBytes(read.ReadToEnd()));

            var responce = serializer.ReadObject(stream) as JsonUploadPhotoResponse;

            read.Close();
            stream.Close();

            if (responce != null)
            {
                VKApi.SaveMessagePhoto(responce.Server, responce.Photo, responce.Hash, SavePhotoComplited);
            }
            else
            {
                MessageBox.Show(Resource.ERROR_OCCURRED);
                Deployment.Current.Dispatcher.BeginInvoke(() => { Status = AttachmentsStatus.WaitingForInput; });
                return;
            }
        }

        private int currentAttachmentIndex;
        private void SavePhotoComplited(object sender, VKApiOperationCompletedEventArgs e)
        {
            if (!e.Cancelled && e.IsSuccess)
            {
                var data = e.Result as List<JsonSaveMessagePhotoData>;
                if (data != null && data.Count > 0 && currentAttachmentIndex > -1 && currentAttachmentIndex < Attachments.Count)
                {
                    Attachments[currentAttachmentIndex].Id = data[0].Id;
                    var newIndex = currentAttachmentIndex + 1;
                    var ok = false;
                    while (newIndex < Attachments.Count)
                    {
                        if (Attachments[newIndex].Type == AttachmentType.Photo)
                        {
                            ok = true;
                            break;
                        }

                        newIndex++;
                    }

                    if (ok)
                    {
                        currentAttachmentIndex++;
                        var request = (HttpWebRequest)WebRequest.Create(messageServer);

                        request.Method = "POST";
                        request.ContentType = string.Format("multipart/form-data; boundary={0}", _boundary);
                        request.BeginGetRequestStream(UploadGetStreamCallback, new UploadAttachmentParams { Request = request, AttachmentIndex = newIndex });
                    }
                    else
                    {                        
                        Deployment.Current.Dispatcher.BeginInvoke(() =>
                        {
                            Status = AttachmentsStatus.WaitingForInput;
                            if (uploadEventHandler != null)
                                uploadEventHandler.Invoke(null,
                                            new VKApiOperationCompletedEventArgs
                                                {
                                                    IsSuccess = true,
                                                    ErrorCode = 0
                                                });
                        });
                        return;
                    }
                }
                else
                {
                    MessageBox.Show(Resource.ERROR_OCCURRED);
                    Deployment.Current.Dispatcher.BeginInvoke(() => { Status = AttachmentsStatus.WaitingForInput; });
                    return;
                }
            }
            else
            {
                MessageBox.Show(Resource.ERROR_OCCURRED);
                Deployment.Current.Dispatcher.BeginInvoke(() => { Status = AttachmentsStatus.WaitingForInput; });
                return;
            }
        }

        public void AppBarChackIn(object sender, EventArgs e)
        {
            Navigate("CheckInPage");
        }
    }
}
