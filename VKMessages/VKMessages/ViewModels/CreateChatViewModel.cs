﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Input;
using Microsoft.Phone.Controls;
using VKMessages.API;
using VKMessages.DB.Entities;
using VKMessages.Tools;

namespace VKMessages.ViewModels
{
    public enum CreateChatStatus
    {
        WaitingForInput,
        CreatingChat,
        NetProblem
    }

    public class CreateChatViewModel : BaseViewModel
    {
        public CreateChatViewModel()
        {
            create = new Commands.DelegateCommand(StartCreate);
        }

        private readonly ICommand create;

        public ICommand Create
        {
            get { return create; }
        }

        public ObservableCollection<FriendCategory> FriendsList
        {
            get { return App.ViewModel.FriendsList; }
        }

        private FriendItemViewModel selectedFriend;

        public FriendItemViewModel SelectedFriend
        {
            get { return selectedFriend; }
            set
            {
                if (selectedFriend != value)
                {
                    selectedFriend = value;
                    NotifyPropertyChanged("SelectedFriend");
                }
            }
        }

        private CreateChatStatus status = CreateChatStatus.WaitingForInput;

        public CreateChatStatus Status
        {
            get { return status; }
            set
            {
                if (value != status)
                {
                    status = value;
                    NotifyPropertyChanged("Status");
                    NotifyPropertyChanged("ProgressBarVisible");
                    NotifyPropertyChanged("ProgressBarText");
                }
            }
        }

        public bool ProgressBarVisible
        {
            get { return Status != CreateChatStatus.WaitingForInput; }
            set { NotifyPropertyChanged("ProgressBarVisible"); }
        }

        public string ProgressBarText
        {
            get
            {
                switch (Status)
                {
                    case CreateChatStatus.CreatingChat:
                        return Resource.PBAR_SENDING_REQUEST;
                    case CreateChatStatus.NetProblem:
                        return Resource.PBAR_CONNECTING;
                    default:
                        return string.Empty;
                }
            }
            set { NotifyPropertyChanged("ProgressBarText"); }
        }

        public string PageTitle
        {
            get { return IsEditMode ? Resource.CHAT_EDIT : Resource.CHAT_CREATE; }
        }

        private string title;

        public string Title
        {
            get { return title; }
            set
            {
                if (value != title)
                {
                    title = value;
                    NotifyPropertyChanged("Title");
                }
            }
        }

        public void FriendsLink(object sender, LinkUnlinkEventArgs e)
        {
            var category = e.ContentPresenter.Content as FriendCategory;

            if (category != null)
            {
                foreach (var item in category.Where(x => x.Image == null))
                {
                    if (MainViewModel.FriendsImages.ContainsKey(item.UID))
                    {
                        item.Image = MainViewModel.FriendsImages[item.UID];
                        continue;
                    }
                    var request = (HttpWebRequest) WebRequest.Create(item.Image50X50Url);
                    request.AllowReadStreamBuffering = false;
                    request.BeginGetResponse(DownloadImageCompleted,
                                             new HttpRequestParams {Request = request, Param = item});
                }
            }
        }

        private void DownloadImageCompleted(IAsyncResult result)
        {
            var param = result.AsyncState as HttpRequestParams;

            if (param == null)
                return;

            var item = param.Param as FriendItemViewModel;

            if (param.Request == null)
                return;

            Stream responseStream;
            HttpWebResponse response;

            try
            {
                response = (HttpWebResponse)param.Request.EndGetResponse(result);
                responseStream = response.GetResponseStream();
            }
            catch (Exception)
            {
                return;
            }

            if (responseStream.CanRead)
            {
                item.Image50X50Data = new byte[response.ContentLength];
                responseStream.Read(item.Image50X50Data, 0, item.Image50X50Data.Length);
            }

            responseStream.Close();
        }

        private bool isEditMode = false;

        public bool IsEditMode
        {
            get { return isEditMode; }
            private set
            {
                if (isEditMode != value)
                {
                    isEditMode = value;
                    NotifyPropertyChanged("IsEditMode");
                    NotifyPropertyChanged("PageTitle");
                }
            }
        }

        public void PageLoaded(object sender, System.Windows.RoutedEventArgs e)
        {
            List<FriendItemViewModel> friends = null;
            if (FriendsList != null && FriendsList.Count > 0)
                friends = FriendsList.SelectMany(friendCategory => friendCategory).ToList();

            if (CrossPageParameters.CurrentChatVM == null || CrossPageParameters.CurrentChatUIDs == null)
            {
                IsEditMode = false;

                Title = Resource.CHAT_DEFAULT;

                if (friends == null || friends.Count == 0)
                    return;

                foreach (var friend in friends)
                {
                    friend.IsChecked = false;
                }

                return;
            }

            IsEditMode = true;

            Title = CrossPageParameters.CurrentChatVM.Name;

            if (friends == null || friends.Count == 0)
                return;

            foreach (var friend in friends)
            {
                friend.IsChecked = CrossPageParameters.CurrentChatUIDs.Contains(friend.UID);
            }
        }

        public void FriendClick(object sender, MouseButtonEventArgs e)
        {
            if (SelectedFriend != null)
            {
                SelectedFriend.IsChecked = !SelectedFriend.IsChecked;
            }
        }

        public void StartCreate(object o)
        {

        }

        private void CreateChatComplited(object sender, VKApiOperationCompletedEventArgs e)
        {
            Status = CreateChatStatus.WaitingForInput;

            if (!e.Cancelled && e.IsSuccess)
            {
                App.ViewModel.AddDialog(new DialogItemViewModel
                                            {
                                                ActiveChatIds = new List<long>(uids),
                                                ChatId = (long) e.Result,
                                                Date = DateTime.Now,
                                                Title = Title
                                            });
                var chatActiveIds = uids.Aggregate(string.Empty, (list, uid1) => list + "," + uid1);
                if (!string.IsNullOrEmpty(chatActiveIds) && chatActiveIds[0] == ',' && chatActiveIds.Length > 1)
                    chatActiveIds = chatActiveIds.Substring(1, chatActiveIds.Length - 1);
                DB.DB.InsertDialog(new DBDialog
                                       {
                                           Body = string.Empty,
                                           ChatId = (long)e.Result,
                                           UID = (long) e.Result + 2000000000,
                                           ChatActiveIds = chatActiveIds,
                                           Date = (long)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds,
                                           Title = Title
                                       });
                GoBack();
            }
            else
            {
                MessageBox.Show(Resource.ERROR_OCCURRED);
            }
        }

        private List<long> uids;

        public void AppBarSave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Title) || string.IsNullOrEmpty(Title.Trim()))
            {
                MessageBox.Show(Resource.CHAT_ENTER_TITLE);
                return;
            }

            uids = FriendsList.
                SelectMany(friendCategory => friendCategory).
                Where(x => x.IsChecked).
                Select(y => y.UID).ToList();

            if (uids.Count < 2)
            {
                MessageBox.Show(Resource.CHAT_SELECT_ERROR);
                return;
            }

            Status = CreateChatStatus.CreatingChat;

            if (!IsEditMode)
            {
                VKApi.CreateChat(uids, Title, CreateChatComplited);
            }
            else
            {
                var addUids = uids.Except(CrossPageParameters.CurrentChatUIDs).ToList();
                var delUids = CrossPageParameters.CurrentChatUIDs.Except(uids).ToList();
                VKApi.EditChat(CrossPageParameters.ChatId, Title, addUids, delUids, EditChatComplited);
            }
        }

        private void EditChatComplited(object sender, VKApiOperationCompletedEventArgs e)
        {
            Status = CreateChatStatus.WaitingForInput;

            if (!e.Cancelled && e.IsSuccess)
            {
                CrossPageParameters.CurrentChatVM.Name = Title;
                CrossPageParameters.CurrentDialog.Title = Title;
                CrossPageParameters.ChatName = Title;
                CrossPageParameters.CurrentTitle = Title;
                DB.DB.UpdateDialog(CrossPageParameters.ChatId + 2000000000, CrossPageParameters.CurrentChatUIDs, Title, null);
                GoBack();
            }
            else
            {
                MessageBox.Show(Resource.ERROR_OCCURRED);
            }
        }
    }
}
