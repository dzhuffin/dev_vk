﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using Microsoft.Phone.UserData;
using VKMessages.API;
using VKMessages.Tools;

namespace VKMessages.ViewModels
{
    public enum ContactStatus
    {
        WaitingForInput,
        SendingRequest,
        RecievingData,
        NetProblem
    }

    public class PhoneNumberItem
    {
        public string PhoneNumber { get; set; }
    }

    public class ContactViewModel : BaseViewModel
    {
        public ContactViewModel()
        {
            send = new Commands.DelegateCommand(StartSend);
            cancel = new Commands.DelegateCommand(StartCancel);
        }

        private readonly ICommand send;

        public ICommand Send
        {
            get { return send; }
        }

        private readonly ICommand cancel;

        public ICommand Cancel
        {
            get { return cancel; }
        }

        public bool ProgressBarVisible
        {
            get { return Status != ContactStatus.WaitingForInput; }
            set { NotifyPropertyChanged("ProgressBarVisible"); }
        }

        public ApplicationBarMenuItem DeleteFriendItem { get; set; }

        public Visibility IsContactRegistered
        {
            get { return CrossPageParameters.CurrentContactVM.UID > 0 ? Visibility.Visible : Visibility.Collapsed; }
        }

        public bool IsContactRegisteredBool
        {
            get { return IsContactRegistered == Visibility.Visible; }
        }

        public Visibility IsContactRegisteredInv
        {
            get { return IsContactRegistered == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible; }
        }

        public string ProgressBarText
        {
            get
            {
                switch (Status)
                {
                    case ContactStatus.RecievingData:
                        return Resource.PBAR_RECEIVING_DATA;
                    case ContactStatus.SendingRequest:
                        return Resource.PBAR_SENDING_REQUEST;
                    case ContactStatus.NetProblem:
                        return Resource.PBAR_CONNECTING;
                    default:
                        return string.Empty;
                }
            }
            set { NotifyPropertyChanged("ProgressBarText"); }
        }

        private ContactStatus status = ContactStatus.WaitingForInput;

        public ContactStatus Status
        {
            get { return status; }
            set
            {
                if (value != status)
                {
                    status = value;
                    NotifyPropertyChanged("Status");
                    NotifyPropertyChanged("ProgressBarVisible");
                    NotifyPropertyChanged("ProgressBarText");
                }
            }
        }

        public string ContactName
        {
            get
            {
                if (CrossPageParameters.CurrentContactVM != null)
                    return CrossPageParameters.CurrentContactVM.ContactName;

                return string.Empty;
            }
        }

        public ObservableCollection<PhoneNumberItem> PhoneNumbers { get; set; }

        public void RefreshPhones()
        {
            if (CrossPageParameters.CurrentContactVM == null ||
                CrossPageParameters.CurrentContactVM.Phones.Count < 1)
                return;

            PhoneNumbers = new ObservableCollection<PhoneNumberItem>();
            foreach (var contactPhoneNumber in CrossPageParameters.CurrentContactVM.Phones)
                PhoneNumbers.Add(new PhoneNumberItem {PhoneNumber = contactPhoneNumber});

            NotifyPropertyChanged("PhoneNumbers");
        }

        public WriteableBitmap Image
        {
            get
            {
                if (CrossPageParameters.CurrentContactVM != null)
                    return CrossPageParameters.CurrentContactVM.Image;

                return null;
            }
        }

        private PhoneNumberItem selectedPhoneItem;

        public PhoneNumberItem SelectedPhoneItem
        {
            get { return selectedPhoneItem; }
            set
            {
                if (selectedPhoneItem != value)
                {
                    selectedPhoneItem = value;
                    NotifyPropertyChanged("SelectedPhoneItem");
                }
            }
        }

        public void StartSend(object o)
        {
        }

        public void StartCancel(object o)
        {
        }

        public void PageLoaded(object sender, RoutedEventArgs e)
        {
            if (DeleteFriendItem != null)
                DeleteFriendItem.IsEnabled = IsContactRegisteredBool;

            RefreshPhones();
        }

        public void Call(object sender, MouseButtonEventArgs e)
        {
            if (SelectedPhoneItem != null)
            {
                var task = new PhoneCallTask {PhoneNumber = SelectedPhoneItem.PhoneNumber};
                task.Show();
            }
        }

        public void OpenChat(object sender, MouseButtonEventArgs e)
        {
            CrossPageParameters.FriendUId = CrossPageParameters.CurrentContactVM.UID;
            CrossPageParameters.FriendVKName = CrossPageParameters.CurrentContactVM.VKName;;
            CrossPageParameters.AttachmentsCount = 0;
            Navigate("ConversationPage");
        }

        public void Invite(object sender, RoutedEventArgs e)
        {
            var sms = new SmsComposeTask();

            var phone = PhoneNumbers.FirstOrDefault();
            if (phone != null)
            {
                sms.To = phone.PhoneNumber;
                sms.Body = Resource.CONTACT_INVITE_TEXT;

                sms.Show();
            }
        }

        public void DeleteFriend(object sender, EventArgs e)
        {
            VKApi.FriendDelete(CrossPageParameters.CurrentContactVM.UID, FriendDeleteComplited);
        }

        private void FriendDeleteComplited(object sender, VKApiOperationCompletedEventArgs e)
        {
            if (!e.Cancelled && e.IsSuccess)
            {
                MessageBox.Show(Resource.OPERATION_SUCCESS);
                GoBack();
                SharedPreferences.IsContactsSynced = false;
                App.ViewModel.ExecuteSyncContacts(null);
            }
            else
            {
                MessageBox.Show(Resource.ERROR_OCCURRED);
            }
        }
    }
}
