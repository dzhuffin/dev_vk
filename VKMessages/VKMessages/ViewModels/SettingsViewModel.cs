﻿using System;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Microsoft.Phone.Tasks;
using VKMessages.API;
using VKMessages.API.JSON;
using VKMessages.Tools;

namespace VKMessages.ViewModels
{
    public enum SettingsStatus
    {
        WaitingForInput,
        SendingRequest,
        RecievingData,
        NetProblem
    }    

    public class SettingsViewModel : BaseViewModel
    {
        public SettingsViewModel()
        {
            DoNotDisturbEnable = false;
            DoNotDisturbEnable8 = false;
            CancelEnable = false;
            UploadPhotoEnable = false;

            logOut = new Commands.DelegateCommand(StartLogOut);
            doNotDisturb = new Commands.DelegateCommand(StartDoNotDisturb);
            doNotDisturb8 = new Commands.DelegateCommand(StartDoNotDisturb8);
            cancel = new Commands.DelegateCommand(StartCancel);
            photoChoose = new Commands.DelegateCommand(StartPhotoChoose);

            NotifyPropertyChanged("DoNotDisturbEnable");
            NotifyPropertyChanged("DoNotDisturbEnable8");
            NotifyPropertyChanged("CancelEnable");
        }

        private SettingsStatus status = SettingsStatus.WaitingForInput;
        public SettingsStatus Status
        {
            get
            {
                return status;
            }
            set
            {
                if (value != status)
                {
                    status = value;
                    NotifyPropertyChanged("Status");
                    NotifyPropertyChanged("ProgressBarVisible");
                    NotifyPropertyChanged("ProgressBarText");
                }
            }
        }

        public bool ProgressBarVisible
        {
            get
            {
                return Status != SettingsStatus.WaitingForInput;
            }
            set
            {
                NotifyPropertyChanged("ProgressBarVisible");
            }
        }

        public string ProgressBarText
        {
            get
            {
                switch (Status)
                {
                    case SettingsStatus.RecievingData:
                        return Resource.PBAR_RECEIVING_DATA;
                    case SettingsStatus.SendingRequest:
                        return Resource.PBAR_SENDING_REQUEST;
                    case SettingsStatus.NetProblem:
                        return Resource.PBAR_CONNECTING;
                    default:
                        return string.Empty;
                }
            }
            set
            {
                NotifyPropertyChanged("ProgressBarText");
            }
        }

        public string UserName
        {
            get
            {
                return SharedPreferences.UserName;
            }
            set
            {
                NotifyPropertyChanged("UserName");
            }
        }

        public bool VibrateOn
        {
            get
            {
                return SharedPreferences.IsVibrateOn;
            }
            set
            {
                if (value != SharedPreferences.IsVibrateOn)
                {
                    SharedPreferences.IsVibrateOn = value;
                    NotifyPropertyChanged("VibrateOn");
                }
            }
        }

        public bool SoundOn
        {
            get
            {
                return SharedPreferences.IsSoundOn;
            }
            set
            {
                if (value != SharedPreferences.IsSoundOn)
                {
                    SharedPreferences.IsSoundOn = value;
                    NotifyPropertyChanged("SoundOn");
                }
            }
        }

        public bool PushOn
        {
            get
            {
                return SharedPreferences.IsPushOn;
            }
            set
            {
                if (value != SharedPreferences.IsPushOn)
                {
                    if (value)
                        PushNotifications.Enable();
                    else
                        PushNotifications.Disable();

                    SharedPreferences.IsPushOn = value;
                    NotifyPropertyChanged("PushOn");
                }
            }
        }

        private bool doNotDisturbEnable;
        public bool DoNotDisturbEnable
        {
            get
            {
                return doNotDisturbEnable;
            }
            set
            {
                if (value != doNotDisturbEnable)
                {
                    doNotDisturbEnable = value;
                    NotifyPropertyChanged("DoNotDisturbEnable");
                }
            }
        }

        private bool doNotDisturbEnable8;
        public bool DoNotDisturbEnable8
        {
            get
            {
                return doNotDisturbEnable8;
            }
            set
            {
                if (value != doNotDisturbEnable8)
                {
                    doNotDisturbEnable8 = value;
                    NotifyPropertyChanged("DoNotDisturbEnable8");
                }
            }
        }

        private bool uploadPhotoEnable;
        public bool UploadPhotoEnable
        {
            get
            {
                return uploadPhotoEnable;
            }
            set
            {
                if (value != uploadPhotoEnable)
                {
                    uploadPhotoEnable = value;
                    NotifyPropertyChanged("UploadPhotoEnable");
                }
            }
        }

        private byte[] accountPhotoData;
        public byte[] AccountPhotoData
        {
            get { return accountPhotoData; }
            set
            {
                accountPhotoData = value;

                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    lock (accountPhotoData)
                    {
                        try
                        {
                            var stream = new MemoryStream(accountPhotoData);
                            AccountPhoto = Microsoft.Phone.PictureDecoder.DecodeJpeg(stream);

                            stream.Close();
                        }
                        catch (Exception)
                        {
                        }
                    }
                });
            }
        }

        private WriteableBitmap accountPhoto;
        public WriteableBitmap AccountPhoto
        {
            get
            {
                return accountPhoto;
            }
            set
            {
                if (value != accountPhoto)
                {
                    accountPhoto = value;
                    NotifyPropertyChanged("AccountPhoto");
                }
            }
        }

        private bool cancelEnable;
        public bool CancelEnable
        {
            get
            {
                return cancelEnable;
            }
            set
            {
                if (value != cancelEnable)
                {
                    cancelEnable = value;
                    NotifyPropertyChanged("CancelEnable");
                }
            }
        }

        public DateTime SoundsDisableTillDate { get; set; }

        public string SoundsDisableTill
        {
            get
            {
                return (CancelEnable) ?
                    Resource.SETTINGS_SOUNDS_DISABLED_TILL.Replace("{time}", SoundsDisableTillDate.ToShortTimeString()) :
                    Resource.SETTINGS_SOUNDS_ENABLED;
            }
        }

        private readonly ICommand logOut;
        public ICommand LogOut
        {
            get { return logOut; }
        }

        private readonly ICommand doNotDisturb;
        public ICommand DoNotDisturb
        {
            get { return doNotDisturb; }
        }

        private readonly ICommand doNotDisturb8;
        public ICommand DoNotDisturb8
        {
            get { return doNotDisturb8; }
        }

        private readonly ICommand cancel;
        public ICommand Cancel
        {
            get { return cancel; }
        }

        private readonly ICommand photoChoose;
        public ICommand PhotoChoose
        {
            get { return photoChoose; }
        }

        public void StartLogOut(object o)
        {
            SharedPreferences.AccessToken = string.Empty;
            App.LoginViewModel.IsLoggingOut = true;
            Navigate("LoginPage");
        }

        public void StartDoNotDisturb(object o)
        {
            Status = SettingsStatus.SendingRequest;
            PushNotifications.SetSilenceModeForHour(DoNotDisturbHourComplited);
        }

        public void StartDoNotDisturb8(object o)
        {
            Status = SettingsStatus.SendingRequest;
            PushNotifications.SetSilenceModeForHour(DoNotDisturb8HoursComplited);
        }

        public void StartCancel(object o)
        {
            Status = SettingsStatus.SendingRequest;
            PushNotifications.DisableSilenceMode(CancelDoNotDisturbComplited);
        }

        private void CancelDoNotDisturbComplited(object sender, VKApiOperationCompletedEventArgs e)
        {
            if (!e.Cancelled && e.Error == null && e.IsSuccess)
            {
                Status = SettingsStatus.RecievingData;
                PushNotifications.GetPushSettings(GetPushSettingsComplited);
            }
        }

        private void DoNotDisturbHourComplited(object sender, VKApiOperationCompletedEventArgs e)
        {
            if (!e.Cancelled && e.Error == null && e.IsSuccess)
            {
                Status = SettingsStatus.RecievingData;
                PushNotifications.GetPushSettings(GetPushSettingsComplited);
            }
        }

        private void DoNotDisturb8HoursComplited(object sender, VKApiOperationCompletedEventArgs e)
        {
            if (!e.Cancelled && e.Error == null && e.IsSuccess)
            {
                Status = SettingsStatus.RecievingData;
                PushNotifications.GetPushSettings(GetPushSettingsComplited);
            }
        }

        public void StartPhotoChoose(object o)
        {
            var photoChooserTask = new PhotoChooserTask {PixelWidth = 100, PixelHeight = 100};
            photoChooserTask.Completed += photoChooserTask_Completed;            

            try
            {
                photoChooserTask.Show();
            }
            catch (InvalidOperationException)
            {
                MessageBox.Show(Resource.ERROR_OCCURRED);
            }
        }

        private const string _templateFile = "--{0}\r\nContent-Disposition: form-data; name=\"{1}\"; filename=\"{2}\"\r\nContent-Type: {3}\r\n\r\n";
        private const string _templateEnd = "--{0}--\r\n\r\n";
        private string _boundary = String.Format("--{0}", Guid.NewGuid().ToString().Replace("-", string.Empty));
        private byte[] tempPhotoData;
        void photoChooserTask_Completed(object sender, PhotoResult e)
        {
            if (e.TaskResult == TaskResult.OK)
            {
                tempPhotoData = new byte[e.ChosenPhoto.Length];
                e.ChosenPhoto.Read(tempPhotoData, 0, tempPhotoData.Length);

                var request = (HttpWebRequest)WebRequest.Create(profileServer);
                
                request.Method = "POST";
                request.ContentType = String.Format("multipart/form-data; boundary={0}", _boundary);
                request.BeginGetRequestStream(new AsyncCallback(UploadGetStreamCallback), request);

                //e.ChosenPhoto.Position = 0;
                //AccountPhoto.SetSource(e.ChosenPhoto);
            }
        }

        private void UploadGetStreamCallback(IAsyncResult result)
        {
            var req = (HttpWebRequest)result.AsyncState;
            var postStream = req.EndGetRequestStream(result);
            var FilePath = "test.jpg";
            var FileType = "application/octet-stream";
            var Name = "file1";
            var contentFile = Encoding.UTF8.GetBytes(String.Format(_templateFile, _boundary, Name, FilePath, FileType));
            postStream.Write(contentFile, 0, contentFile.Length);
            postStream.Write(tempPhotoData, 0, tempPhotoData.Length);
            var _lineFeed = Encoding.UTF8.GetBytes("\r\n");
            postStream.Write(_lineFeed, 0, _lineFeed.Length);
            var contentEnd = Encoding.UTF8.GetBytes(String.Format(_templateEnd, _boundary));
            postStream.Write(contentEnd, 0, contentEnd.Length);
            postStream.Close();

            req.BeginGetResponse((new AsyncCallback(UploadComplitedCallback)), req);            
        }

        private void UploadComplitedCallback(IAsyncResult result)
        {
            var req = (HttpWebRequest)result.AsyncState;
            var webResponse = req.EndGetResponse(result);

            var read = new StreamReader(webResponse.GetResponseStream());

            var serializer = new DataContractJsonSerializer(typeof(JsonUploadPhotoResponse));

            var stream = new MemoryStream(Encoding.UTF8.GetBytes(read.ReadToEnd()));

            var responce = serializer.ReadObject(stream) as JsonUploadPhotoResponse;

            read.Close();
            stream.Close();

            if (responce != null)
            {
                VKApi.SaveProfilePhoto(responce.Server, responce.Photo, responce.Hash, SavePhotoComplited);
            }
        }

        private void SavePhotoComplited(object sender, VKApiOperationCompletedEventArgs e)
        {
            if (!e.Cancelled && e.IsSuccess)
            {
                var data = e.Result as JsonSavePhotoData;
                if (data != null)
                {
                    SharedPreferences.UserPhotoUrl = data.PhotoUrl;

                    if (!string.IsNullOrEmpty(SharedPreferences.UserPhotoUrl))
                    {
                        var request = (HttpWebRequest)WebRequest.Create(SharedPreferences.UserPhotoUrl);
                        request.AllowReadStreamBuffering = false;
                        request.BeginGetResponse(DownloadImageCompleted, new HttpRequestParams { Request = request });
                    }
                }
            }
        }

        public void PageLoaded(object sender, RoutedEventArgs e)
        {
            Status = SettingsStatus.RecievingData;
            profileServer = string.Empty;
            PushNotifications.GetPushSettings(GetPushSettingsComplited);
            VKApi.GetProfileUploadServer(GetProfileUploadServerComplited); 

            if (!string.IsNullOrEmpty(SharedPreferences.UserPhotoUrl))
            {
                var request = (HttpWebRequest) WebRequest.Create(SharedPreferences.UserPhotoUrl);
                request.AllowReadStreamBuffering = false;
                request.BeginGetResponse(DownloadImageCompleted, new HttpRequestParams {Request = request});
            }
        }

        private string profileServer;

        private void GetProfileUploadServerComplited(object sender, VKApiOperationCompletedEventArgs e)
        {
            profileServer = string.Empty;

            if (!e.Cancelled && e.Error == null && e.IsSuccess)
            {
                var jsonProfileServer = e.Result as JsonProfileUploadServer;
                if (jsonProfileServer != null)
                {
                    profileServer = jsonProfileServer.Server;

                    UploadPhotoEnable = true;
                }
            }
        }

        private void DownloadImageCompleted(IAsyncResult result)
        {
            var param = result.AsyncState as HttpRequestParams;

            if (param == null)
                return;

            if (param.Request == null)
                return;

            var response = (HttpWebResponse)param.Request.EndGetResponse(result);

            var responseStream = response.GetResponseStream();

            if (responseStream.CanRead)
            {
                AccountPhotoData = new byte[response.ContentLength];
                responseStream.Read(AccountPhotoData, 0, AccountPhotoData.Length);
            }

            responseStream.Close();
        }

        private void GetPushSettingsComplited(object sender, VKApiOperationCompletedEventArgs e)
        {
            Status = SettingsStatus.WaitingForInput;

            if (!e.Cancelled && e.Error == null && e.IsSuccess)
            {
                var settings = e.Result as JsonGetPushSettings;

                if (settings != null)
                {
                    if (settings.DisabledUntil > 0)
                    {
                        DoNotDisturbEnable = false;
                        DoNotDisturbEnable8 = false;
                        CancelEnable = true;

                        SoundsDisableTillDate =
                            (new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc) +
                             TimeSpan.FromSeconds(settings.DisabledUntil)).ToLocalTime();

                        NotifyPropertyChanged("SoundsDisableTill");
                    }
                    else
                    {
                        DoNotDisturbEnable = true;
                        DoNotDisturbEnable8 = true;
                        CancelEnable = false;

                        NotifyPropertyChanged("SoundsDisableTill");
                    }
                }

            }
        }
    }
}
