﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media;
using Microsoft.Phone.Controls;

namespace VKMessages.ViewModels
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        private static readonly TransitionFrame frame;
        private const string PathFormat = "/Pages/{0}.xaml";

        static BaseViewModel()
        {
            var app = Application.Current as App;
            if (app == null)
                throw new Exception("Wrong type of App object!");

            frame = app.RootFrame;
        }

        public static void Navigate(string pageName)
        {
            frame.Navigate(new Uri(string.Format(PathFormat, pageName), UriKind.Relative));
        }

        public static void GoBack()
        {
            if (frame.CanGoBack)
                frame.GoBack();
        }

        public static void RemoveBackEntry()
        {
            frame.RemoveBackEntry();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(String propertyName)
        {
            var handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        // Detecting the current theme.
        private static Color lightThemeBackground = Color.FromArgb(255, 255, 255, 255);
        private static Color darkThemeBackground = Color.FromArgb(255, 0, 0, 0);
        private static SolidColorBrush backgroundBrush;
        public Color ThemeInvColor
        {
            get
            {
                if (backgroundBrush == null)
                    backgroundBrush = Application.Current.Resources["PhoneBackgroundBrush"] as SolidColorBrush;

                if (backgroundBrush.Color == lightThemeBackground)
                    return darkThemeBackground;
                else if (backgroundBrush.Color == darkThemeBackground)
                    return lightThemeBackground;

                return lightThemeBackground;
            }
            set
            {
                NotifyPropertyChanged("ThemeInvColor");
            }
        }
    }
}
