﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Microsoft.Phone.UserData;

namespace VKMessages.ViewModels
{
    public class ContactItemViewModel : BaseViewModel
    {
        public ContactItemViewModel()
        {
            openConversation = new Commands.DelegateCommand(ExecuteOpenConversation);
        }

        private readonly ICommand openConversation;
        public ICommand OpenConversation
        {
            get { return openConversation; }
        }

        public Contact Contact { get; set; }

        public byte[] ImageData { get; set; }

        private WriteableBitmap image;
        public WriteableBitmap Image
        {
            get
            {
                return image;
            }
            set
            {
                if (value != image)
                {
                    image = value;
                    NotifyPropertyChanged("Image");
                }
            }
        }

        private string contactName;
        public string ContactName
        {
            get
            {
                return contactName;
            }
            set
            {
                if (value != contactName)
                {
                    contactName = value;
                    NotifyPropertyChanged("ContactName");
                }
            }
        }

        private List<string> phones;
        public List<string> Phones
        {
            get
            {
                return phones;
            }
            set
            {
                if (value != phones)
                {
                    phones = value;
                    NotifyPropertyChanged("Phones");
                }
            }
        }

        private string vkName;
        public string VKName
        {
            get
            {
                return vkName;
            }
            set
            {
                if (value != vkName)
                {
                    vkName = value;
                    NotifyPropertyChanged("VKName");
                }
            }
        }

        private long uid;
        public long UID
        {
            get
            {
                return uid;
            }
            set
            {
                if (value != uid)
                {
                    uid = value;
                    NotifyPropertyChanged("UID");
                }
            }
        }

        public void ExecuteOpenConversation(object o)
        {

        }
    }

    public class ContactCategory : ObservableCollection<ContactItemViewModel>
    {
        public ContactCategory(IEnumerable<ContactItemViewModel> a)
            : base(a)
        {

        }

        public string Key { get; set; }

        public bool HasItems { get { return Count > 0; } }
    }
}
