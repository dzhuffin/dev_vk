﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace VKMessages.ViewModels
{
    public class FriendItemViewModel : BaseViewModel
    {
        public FriendItemViewModel()
        {
            openConversation = new Commands.DelegateCommand(ExecuteOpenConversation);
        }

        private readonly ICommand openConversation;
        public ICommand OpenConversation
        {
            get { return openConversation; }
        }

        private bool isOnline;
        public bool IsOnline
        {
            get
            {
                return isOnline;
            }
            set
            {
                if (value != isOnline)
                {
                    isOnline = value;
                    NotifyPropertyChanged("IsOnline");
                }
            }
        }

        private bool isChecked;
        public bool IsChecked
        {
            get
            {
                return isChecked;
            }
            set
            {
                if (value != isChecked)
                {
                    isChecked = value;
                    NotifyPropertyChanged("IsChecked");
                }
            }
        }

        private long uid;
        public long UID
        {
            get
            {
                return uid;
            }
            set
            {
                if (value != uid)
                {
                    uid = value;
                    NotifyPropertyChanged("UID");
                }
            }
        }

        private long rate;
        public long Rate
        {
            get
            {
                return rate;
            }
            set
            {
                if (value != rate)
                {
                    rate = value;
                    NotifyPropertyChanged("UserRate");
                }
            }
        }

        public string Image50X50Url { get; set; }
        public string Image100X100Url { get; set; }
        private byte[] image50X50Data;
        public byte[] Image50X50Data
        {
            get { return image50X50Data; }
            set
            {
                image50X50Data = value;

                if (image50X50Data == null)
                    return;

                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    lock (image50X50Data)
                    {
                        try
                        {
                            var stream = new MemoryStream(image50X50Data);
                            Image = Microsoft.Phone.PictureDecoder.DecodeJpeg(stream);

                            if (MainViewModel.FriendsImages.ContainsKey(UID))
                                MainViewModel.FriendsImages[UID] = Image;
                            else
                                MainViewModel.FriendsImages.Add(UID, Image);

                            stream.Close();
                        }
                        catch (Exception)
                        {
                        }                        
                    }
                });                
            }
        }
        public byte[] Image100X100Data { get; set; }

        private WriteableBitmap image;
        public WriteableBitmap Image
        {
            get
            {
                return image;
            }
            set
            {
                if (value != image)
                {
                    image = value;
                    NotifyPropertyChanged("Image");
                }
            }
        }

        private string vkName;
        public string VKName
        {
            get
            {
                return vkName;
            }
            set
            {
                if (value != vkName)
                {
                    vkName = value;
                    NotifyPropertyChanged("VKName");
                }
            }
        }

        private string phone;
        public string Phone
        {
            get
            {
                return phone;
            }
            set
            {
                if (value != phone)
                {
                    phone = value;
                    NotifyPropertyChanged("Phone");
                }
            }
        }

        private string mobilePhone;
        public string MobilePhone
        {
            get
            {
                return mobilePhone;
            }
            set
            {
                if (value != mobilePhone)
                {
                    mobilePhone = value;
                    NotifyPropertyChanged("MobilePhone");
                }
            }
        }

        private string homePhone;
        public string HomePhone
        {
            get
            {
                return homePhone;
            }
            set
            {
                if (value != homePhone)
                {
                    homePhone = value;
                    NotifyPropertyChanged("HomePhone");
                }
            }
        }

        public void ExecuteOpenConversation(object o)
        {

        }
    }

    public class FriendCategory : ObservableCollection<FriendItemViewModel>
    {
        public FriendCategory(IEnumerable<FriendItemViewModel> a) 
            : base(a)
        {
            
        }

        public string Key { get; set; }

        public bool HasItems { get { return Count > 0; } }

        public Visibility IsVisible { get { return string.IsNullOrEmpty(Key) ? Visibility.Collapsed : Visibility.Visible; } }
    }
}
