﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using VKMessages.Tools;

namespace VKMessages.ViewModels
{
    public enum SearchStatus
    {
        WaitingForInput,
        Searching,
        NetProblem
    } 

    public class SearchViewModel : BaseViewModel
    {
        private SearchStatus status = SearchStatus.WaitingForInput;
        public SearchStatus Status
        {
            get
            {
                return status;
            }
            set
            {
                if (value != status)
                {
                    status = value;
                    NotifyPropertyChanged("Status");
                    NotifyPropertyChanged("ProgressBarVisible");
                    NotifyPropertyChanged("ProgressBarText");
                }
            }
        }

        public bool ProgressBarVisible
        {
            get
            {
                return Status != SearchStatus.WaitingForInput;
            }
            set
            {
                NotifyPropertyChanged("ProgressBarVisible");
            }
        }

        public string ProgressBarText
        {
            get
            {
                switch (Status)
                {
                    case SearchStatus.Searching:
                        return Resource.PBAR_SEARCHING;
                    case SearchStatus.NetProblem:
                        return Resource.PBAR_CONNECTING;
                    default:
                        return string.Empty;
                }
            }
            set
            {
                NotifyPropertyChanged("ProgressBarText");
            }
        }

        private string searchText;
        public string SearchText
        {
            get
            {
                return searchText;
            }
            set
            {
                if (value != searchText)
                {
                    Search(value);
                    searchText = value;
                    NotifyPropertyChanged("SearchText");
                }
            }
        }

        private FriendItemViewModel selectedUser;
        public FriendItemViewModel SelectedUser
        {
            get
            {
                return selectedUser;
            }
            set
            {
                if (selectedUser != value)
                {
                    selectedUser = value;
                    NotifyPropertyChanged("SelectedUser");
                }
            }
        }

        private ObservableCollection<FriendItemViewModel> usersList = new ObservableCollection<FriendItemViewModel>();
        public ObservableCollection<FriendItemViewModel> UsersList
        {
            get
            {
                return usersList;
            }
            set
            {
                if (value != usersList)
                {
                    usersList = value;
                    NotifyPropertyChanged("UsersList");
                }
            }
        }

        public void PageLoaded(object sender, System.Windows.RoutedEventArgs e)
        {
            SearchText = string.Empty;
            Search(SearchText);
        }

        public void UserClick(object sender, MouseButtonEventArgs e)
        {
            if (SelectedUser != null)
            {
                CrossPageParameters.FriendUId = SelectedUser.UID;
                CrossPageParameters.FriendVKName = SelectedUser.VKName;
                CrossPageParameters.FriendIsOnline = SelectedUser.IsOnline;
                Navigate("ConversationPage");
            }
        }

        private void Search(string text)
        {
            var result = new List<FriendItemViewModel>();
            foreach (var cat in App.ViewModel.FriendsList)
                result.AddRange(cat.Where(x => x.VKName.ToLower().Contains(text)));

            UsersList = new ObservableCollection<FriendItemViewModel>(result);
        }
    }
}
