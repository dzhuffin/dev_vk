﻿using System.Windows;
using System.Windows.Input;
using VKMessages.API;
using VKMessages.Tools;

namespace VKMessages.ViewModels
{

    public class ConfirmViewModel : BaseViewModel
    {
        public ConfirmViewModel()
        {
            confirm = new Commands.DelegateCommand(StartConfirm);
        }

        private readonly ICommand confirm;
        public ICommand Confirm
        {
            get { return confirm; }
        }

        private string code;
        public string Code
        {
            get
            {
                return code;
            }
            set
            {
                if (value != code)
                {
                    code = value;
                    NotifyPropertyChanged("Code");
                    NotifyPropertyChanged("DataIsOk");
                }
            }
        }

        private string password;
        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                if (value != password)
                {
                    password = value;
                    NotifyPropertyChanged("Password");
                    NotifyPropertyChanged("DataIsOk");
                }
            }
        }

        public bool DataIsOk
        {
            get
            {
                return !string.IsNullOrEmpty(Password) && !string.IsNullOrEmpty(Code) &&
                    Password.Length > 3 && Code.Length == 4;
            }
            set
            {
                NotifyPropertyChanged("DataIsOk");
            }
        }

        public void StartConfirm(object o)
        {
            VKApi.SignUpConfirm(CrossPageParameters.ConfirmPhoneNumber, Code, Password, ConfirmComplited);
        }

        private void ConfirmComplited(object sender, VKApiOperationCompletedEventArgs e)
        {
            if (!e.Cancelled && e.IsSuccess)
            {
                MessageBox.Show(Resource.SIGNUP_SUCCESS);

                Navigate("LoginPage");

                return;
            }

            MessageBox.Show(Resource.SIGNUP_FAILED);
        }
    }
}
