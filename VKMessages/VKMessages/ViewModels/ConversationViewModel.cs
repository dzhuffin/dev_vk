﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Device.Location;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Tasks;
using VKMessages.API;
using VKMessages.Controls;
using VKMessages.DB.Comparers;
using VKMessages.Tools;

namespace VKMessages.ViewModels
{
    public enum ConversationStatus
    {
        WaitingForInput,
        ReceivingMessages,
        NetProblem
    }

    public delegate void BuildApplicationBarDelegate();

    public class ConversationViewModel : BaseViewModel
    {
        private readonly long friendUID = CrossPageParameters.FriendUId;
        private readonly string friendVKName = CrossPageParameters.FriendVKName;

        public ConversationViewModel()
        {
        }

        public BuildApplicationBarDelegate BuildApplicationBar;

        private string newMessage = string.Empty;
        public string NewMessage
        {
            get
            {
                return newMessage;
            }
            set
            {
                if (value != newMessage)
                {
                    newMessage = value;
                    NotifyPropertyChanged("NewMessage");
                }
            }
        }

        public void Send()
        {
            if (((!string.IsNullOrEmpty(NewMessage) && !string.IsNullOrEmpty(NewMessage.Trim())) ||
                (App.AttachmentsViewModel.Attachments != null && App.AttachmentsViewModel.Attachments.Count > 0)) &&
                 App.AttachmentsViewModel.Status == AttachmentsStatus.WaitingForInput)
            {
                if (App.AttachmentsViewModel.Attachments == null ||
                    App.AttachmentsViewModel.Attachments.Count < 1)
                {
                    VKApi.MessageSend(friendUID, NewMessage, null, false, MessageSendComplited);
                    NewMessage = string.Empty;
                }
                else
                {
                    App.AttachmentsViewModel.StartUploadAttachments(UploadAttachmentsComplited);
                }                
            }
        }

        private void UploadAttachmentsComplited(object sender, VKApiOperationCompletedEventArgs e)
        {
            if (!e.Cancelled && e.IsSuccess)
            {
                VKApi.MessageSend(friendUID, NewMessage, App.AttachmentsViewModel.Attachments.ToList(), 
                    false, MessageSendComplited);
                NewMessage = string.Empty;
                App.AttachmentsViewModel.Attachments.Clear();
                CrossPageParameters.AttachmentsCount = 0;
                BuildApplicationBar();
            }
            else
            {
                MessageBox.Show(Resource.ERROR_OCCURRED);
            }

            VKApi.RequestToLongPool(CrossPageParameters.LongPoolData, App.ViewModel.LongPoolRequestComplited);
        }

        public void SendMessage(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Send();
            }
        }

        private CollectionViewSource itemsSort;
        public CollectionViewSource ItemsSort
        {
            get { return itemsSort; }
            set
            {
                itemsSort = value;
                NotifyPropertyChanged("ItemsSort");
                NotifyPropertyChanged("ItemsSort.View");
            }
        }

        private ConversationStatus status = ConversationStatus.WaitingForInput;
        public ConversationStatus Status
        {
            get
            {
                return status;
            }
            set
            {
                if (value != status)
                {
                    status = value;
                    NotifyPropertyChanged("Status");
                    NotifyPropertyChanged("ProgressBarVisible");
                    NotifyPropertyChanged("ProgressBarText");
                }
            }
        }

        public bool ProgressBarVisible
        {
            get
            {
                return Status != ConversationStatus.WaitingForInput;
            }
            set
            {
                NotifyPropertyChanged("ProgressBarVisible");
            }
        }

        public string ProgressBarText
        {
            get
            {
                switch (Status)
                {
                    case ConversationStatus.ReceivingMessages:
                        return Resource.PBAR_RECEIVING_MESSAGES;
                    case ConversationStatus.NetProblem:
                        return Resource.PBAR_CONNECTING;
                    default:
                        return string.Empty;
                }
            }
            set
            {
                NotifyPropertyChanged("ProgressBarText");
            }
        }

        private bool isOnline;
        public bool IsOnline
        {
            get
            {
                return isOnline;
            }
            set
            {
                if (value != isOnline)
                {
                    isOnline = value;
                    NotifyPropertyChanged("IsOnline");
                }
            }
        }

        private string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (value != name)
                {
                    name = value;
                    NotifyPropertyChanged("Name");
                }
            }
        }

        public ObservableCollection<MessageItemViewModel> Messages { get; set; }

        private void GetMessagesComplited(object sender, VKApiOperationCompletedEventArgs e)
        {
            if (e.Cancelled || !e.IsSuccess)
            {
                Status = ConversationStatus.NetProblem;
                VKApi.MessagesGet(friendUID, 0, false, GetMessagesComplited);
            }
            else
            {
                Status = ConversationStatus.WaitingForInput;

                if (Messages.Count == 0)
                {
                    Messages = DB.DB.GetMessagesList(friendUID, false);
                    ItemsSort = new CollectionViewSource();
                    ItemsSort.SortDescriptions.Add(new SortDescription("MID", ListSortDirection.Ascending));
                    ItemsSort.Source = Messages;

                    if (Messages.Count > 0)
                        firstMessage = Messages.First();
                }
                else
                {
                    var dbMessages = DB.DB.GetMessagesList(friendUID, false).Except(Messages, new MessageItemDistinctComparer());

                    MessageItemViewModel firstMessageTmp = null;
                    foreach (var dbMessage in dbMessages)
                    {
                        if (firstMessageTmp == null)
                            firstMessageTmp = dbMessage;

                        Messages.Add(dbMessage);
                    }

                    if (firstMessage != null && listControl != null)
                        listControl.ScrollTo(firstMessage);

                    firstMessage = firstMessageTmp;
                }
            }
        }

        public void PageLoaded(object sender, System.Windows.RoutedEventArgs e)
        {
            if (Name != CrossPageParameters.CurrentTitle)
            {
                CrossPageParameters.AttachmentsCount = 0;
                App.AttachmentsViewModel.Attachments.Clear();
                CrossPageParameters.CurrentTitle = friendVKName.ToUpper();
            }

            Name = friendVKName.ToUpper();
            IsOnline = CrossPageParameters.FriendIsOnline;

            if (BuildApplicationBar != null)
                BuildApplicationBar.Invoke();

            if (Messages == null || Messages.Count == 0)
            {
                Status = ConversationStatus.ReceivingMessages;

                if (Messages == null || Messages.Count == 0)
                    Messages = DB.DB.GetMessagesList(friendUID, false);

                if (Messages.Count > 0 && ItemsSort == null)
                {
                    ItemsSort = new CollectionViewSource();
                    ItemsSort.SortDescriptions.Add(new SortDescription("MID", ListSortDirection.Ascending));
                    ItemsSort.Source = Messages;
                }

                NotifyPropertyChanged("Messages");
                NotifyPropertyChanged("ItemsSort");
                NotifyPropertyChanged("ItemsSort.View");
            }

            if (Messages != null || Messages.Count > 0)
            {
                var lastMsg = Messages.LastOrDefault();
                if (lastMsg != null && listControl != null)
                    listControl.ScrollTo(lastMsg);
            }

            VKApi.MessagesGet(friendUID, 0, false, GetMessagesComplited);
        }

        public void RefreshLocationImage(MessageItemViewModel item)
        {
            if (item.LocationVisibility == Visibility.Visible && item.LocationImage == null)
            {
                var request = (HttpWebRequest)WebRequest.Create(item.LocationImageUrl);
                request.AllowReadStreamBuffering = false;
                //request.BeginGetResponse(DownloadImageCompleted, new HttpRequestParams { Request = request, Param = item, IntParam = -2 });
                ImageDownloader.AddTask(new HttpRequestParams { Request = request, Param = item, IntParam = -2, Callback = DownloadImageCompleted });
            }
        }

        public void RefreshAttachmentsImages(MessageItemViewModel item)
        {
            if (item.Attachments == null || item.Attachments.Count < 1)
                return;

            for (var i = 0; i < item.Attachments.Count; i++)
            {
                if ((item.Attachments[i].Type == AttachmentType.Photo ||
                    item.Attachments[i].Type == AttachmentType.Video) &&
                    item.Attachments[i].Image == null)
                {
                    var request = (HttpWebRequest) WebRequest.Create(item.Attachments[i].Url);
                    request.AllowReadStreamBuffering = false;
                    //request.BeginGetResponse(DownloadImageCompleted,
                      //                       new HttpRequestParams {Request = request, Param = item, IntParam = i});
                    ImageDownloader.AddTask(new HttpRequestParams { Request = request, Param = item, IntParam = i, Callback = DownloadImageCompleted });
                }
            }
        }

        public void MessageLink(object sender, LinkUnlinkEventArgs e)
        {
            var item = e.ContentPresenter.Content as MessageItemViewModel;

            if (item == null)
                return;

            RefreshAttachmentsImages(item);

            RefreshLocationImage(item);
        }

        private byte[] data;
        private void DownloadImageCompleted(IAsyncResult result)
        {
            var param = result.AsyncState as HttpRequestParams;

            if (param == null)
                return;

            var item = param.Param as MessageItemViewModel;

            if (item == null)
                return;

            if (param.Request == null)
                return;

            Stream responseStream;
            HttpWebResponse response;

            try
            {
                response = (HttpWebResponse)param.Request.EndGetResponse(result);
                responseStream = response.GetResponseStream();
            }
            catch (Exception)
            {
                return;
            }

            if (responseStream.CanRead)
            {
                switch (param.IntParam)
                {
                    case -2:
                        item.LocationImageData = new byte[response.ContentLength];
                        responseStream.Read(item.LocationImageData, 0, item.LocationImageData.Length);
                        break;
                    default:
                        if (param.IntParam >= 0 || param.IntParam < item.Attachments.Count)
                        {
                            data = new byte[response.ContentLength];
                            responseStream.Read(data, 0, data.Length);
                        }
                        //Deployment.Current.Dispatcher.BeginInvoke(() =>
                        //                                              {
                                                                          if (param.IntParam >= 0 || param.IntParam < item.Attachments.Count)
                                                                          {
                                                                              item.Attachments[param.IntParam].ImageData
                                                                                  =
                                                                                  (new List<byte>(data)).ToArray();
                                                                              //item.NotifyPropertyChanged("Attachments");
                                                                              //item.Attachments[param.IntParam].NotifyPropertyChanged("Image");
                                                                          }
                                                                      //});
                        break;
                }                
            }

            responseStream.Close();
        }

        public void MessageUnink(object sender, LinkUnlinkEventArgs e)
        {

        }

        private bool scrollToBottomDone;
        public void PrepareToOpen() { scrollToBottomDone = false; } 
        private FixedLongListSelector listControl;
        public void OnItemsSourceChanged(object sender, EventArgs e)
        {
            listControl = sender as FixedLongListSelector;
            if (listControl == null)
                return;

            if (Messages.Count > 0 && !scrollToBottomDone)
            {
                var curPos = ItemsSort.View.CurrentPosition;
                ItemsSort.View.MoveCurrentToLast();
                var lastMsg = ItemsSort.View.CurrentItem as MessageItemViewModel;
                ItemsSort.View.MoveCurrentToPosition(curPos);

                listControl.ScrollTo(lastMsg);
                scrollToBottomDone = true;
            }
        }

        private MessageItemViewModel firstMessage;
        public void OnMessagesStretching(object sender, StretchingEventArgs e)
        {
            if (e.IsTop && Status == ConversationStatus.WaitingForInput)
            {
                Status = ConversationStatus.ReceivingMessages;
                VKApi.MessagesGet(friendUID, Messages.Count, false, GetMessagesComplited);
            }
        }

        public void AddMessage(MessageItemViewModel newMsgVM)
        {
            if (Messages != null)
            {
                Messages.Add(newMsgVM);
                if (listControl != null)
                    listControl.ScrollTo(newMsgVM);
            }
        }

        private void MessageSendComplited(object sender, VKApiOperationCompletedEventArgs e)
        {
            //if (!e.Cancelled && e.IsSuccess)
            //{
            //    var newMsg = e.Result as MessageItemViewModel;
            //    if (newMsg != null)
            //    {
            //        Messages.Add(newMsg);
            //        if (listControl != null)
            //            listControl.ScrollTo(newMsg);
            //    }
            //}
        }

        public void OnListLoaded(object sender, System.Windows.RoutedEventArgs e)
        {
            listControl = sender as FixedLongListSelector;

            if (Messages != null && Messages.Count > 0 && listControl != null)
            {
                var curPos = ItemsSort.View.CurrentPosition;
                ItemsSort.View.MoveCurrentToLast();
                var lastMsg = ItemsSort.View.CurrentItem as MessageItemViewModel;
                ItemsSort.View.MoveCurrentToPosition(curPos);

                listControl.ScrollTo(lastMsg);
            }
        }

        public void MapClick(string locationString)
        {
            var arr = locationString.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
            if (arr.Length != 2 || string.IsNullOrEmpty(arr[0]) || string.IsNullOrEmpty(arr[1]))
                return;

            var task = new BingMapsTask { Center = new GeoCoordinate(Convert.ToDouble(arr[0], CultureInfo.InvariantCulture), Convert.ToDouble(arr[1], CultureInfo.InvariantCulture)), ZoomLevel = 12 };
            task.Show();
        }

        public void AppBarSend(object sender, EventArgs e)
        {
            Send();
        }

        public void AppBarAddPhoto(object sender, EventArgs e)
        {
            Navigate("AttachmentsPage");
        }

        public void AppBarChackIn(object sender, EventArgs e)
        {
            Navigate("AttachmentsPage");
        }
    }
}
