﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Input;
using Microsoft.Phone.Controls;
using VKMessages.API;
using VKMessages.API.JSON;
using VKMessages.Controls;
using VKMessages.Tools;

namespace VKMessages.ViewModels
{
    public class FriendsRequestViewModel : BaseViewModel
    {
        public enum FriendsRequestsStatus
        {
            WaitingForInput,
            ReceivingData,
            NetProblem
        } 

        public FriendsRequestViewModel()
        {

        }

        private FriendsRequestsStatus status = FriendsRequestsStatus.WaitingForInput;
        public FriendsRequestsStatus Status
        {
            get
            {
                return status;
            }
            set
            {
                if (value != status)
                {
                    status = value;
                    NotifyPropertyChanged("Status");
                    NotifyPropertyChanged("ProgressBarVisible");
                    NotifyPropertyChanged("ProgressBarText");
                }
            }
        }

        public bool ProgressBarVisible
        {
            get
            {
                return Status != FriendsRequestsStatus.WaitingForInput;
            }
            set
            {
                NotifyPropertyChanged("ProgressBarVisible");
            }
        }

        public string ProgressBarText
        {
            get
            {
                switch (Status)
                {
                    case FriendsRequestsStatus.ReceivingData:
                        return Resource.PBAR_RECEIVING_DATA;
                    case FriendsRequestsStatus.NetProblem:
                        return Resource.PBAR_CONNECTING;
                    default:
                        return string.Empty;
                }
            }
            set
            {
                NotifyPropertyChanged("ProgressBarText");
            }
        }

        private ObservableCollection<FriendCategory> usersList = new ObservableCollection<FriendCategory>();
        public ObservableCollection<FriendCategory> UsersList
        {
            get
            {
                return usersList;
            }
            set
            {
                if (value != usersList)
                {
                    usersList = value;
                    NotifyPropertyChanged("UsersList");
                }
            }
        }

        private FriendItemViewModel selectedUser;
        public FriendItemViewModel SelectedUser
        {
            get
            {
                return selectedUser;
            }
            set
            {
                if (selectedUser != value)
                {
                    selectedUser = value;
                    NotifyPropertyChanged("SelectedUser");
                }
            }
        }

        public void UsersLink(object sender, LinkUnlinkEventArgs e)
        {
            var item = e.ContentPresenter.Content as FriendItemViewModel;

            if (item != null)
            {
                if (MainViewModel.FriendsImages.ContainsKey(item.UID))
                {
                    item.Image = MainViewModel.FriendsImages[item.UID];
                }
                var request = (HttpWebRequest)WebRequest.Create(item.Image50X50Url);
                request.AllowReadStreamBuffering = false;
                request.BeginGetResponse(DownloadImageCompleted, new HttpRequestParams { Request = request, Param = item });
            }
        }

        private void DownloadImageCompleted(IAsyncResult result)
        {
            var param = result.AsyncState as HttpRequestParams;

            if (param == null)
                return;

            var item = param.Param as FriendItemViewModel;

            if (item == null)
            {
                return;
            }

            if (param.Request == null)
                return;
            try
            {
                Stream responseStream;
                HttpWebResponse response;

                try
                {
                    response = (HttpWebResponse)param.Request.EndGetResponse(result);
                    responseStream = response.GetResponseStream();
                }
                catch (Exception)
                {
                    return;
                }

                if (responseStream.CanRead)
                {
                    item.Image50X50Data = new byte[response.ContentLength];
                    responseStream.Read(item.Image50X50Data, 0, item.Image50X50Data.Length);
                }

                responseStream.Close();
            }
            catch (Exception e)
            {
            }            
        }

        public void UsersUnlink(object sender, LinkUnlinkEventArgs e)
        {
        }

        public void UserClick(object sender, MouseButtonEventArgs e)
        {
            if (SelectedUser != null)
            {
                CrossPageParameters.CurrentFriendVM = SelectedUser;
                Navigate("FriendRequestPage");
            }
        }

        public void Refresh()
        {
            VKApi.GetFriendRequests(GetFriendRequestsComplited);
        }

        public void PageLoaded(object sender, System.Windows.RoutedEventArgs e)
        {
            Status = FriendsRequestsStatus.ReceivingData;
            Refresh();
        }

        private void GetFriendRequestsComplited(object sender, VKApiOperationCompletedEventArgs e)
        {
            Status = FriendsRequestsStatus.WaitingForInput;

            if (!e.Cancelled && e.IsSuccess)
            {
                var list = e.Result as ObservableCollection<FriendCategory>;
                if (list != null)
                {
                    UsersList = list;
                }
            }
        }

        private void GetFriendSuggestionsComplited(object sender, VKApiOperationCompletedEventArgs e)
        {
            if (!e.Cancelled && e.IsSuccess)
            {
            }
        }

        public void OnUsersStretching(object sender, StretchingEventArgs e)
        {
            if (!e.IsTop)
            {
                Status = FriendsRequestsStatus.ReceivingData;
                var sugCategory = UsersList.LastOrDefault();
                VKApi.GetFriendSuggestions(sugCategory != null ? sugCategory.Count : 0, GetFriendsSuggestionsComplited);
            }
        }

        private void GetFriendsSuggestionsComplited(object sender, VKApiOperationCompletedEventArgs e)
        {
            Status = FriendsRequestsStatus.WaitingForInput;

            if (!e.Cancelled && e.IsSuccess)
            {
                var friends = e.Result as List<JsonFriend>;
                //var sugCategory = UsersList.LastOrDefault();
                if (friends != null)
                {
                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        var newCat = new FriendCategory(UsersList[1]);
                        foreach (var jsonFriend in friends)
                        {
                            newCat.Add(new FriendItemViewModel
                            {
                                UID = jsonFriend.UID,
                                Image50X50Url = jsonFriend.Photo50X50Sq.ToString(),
                                Image100X100Url = jsonFriend.Photo100X100Sq.ToString(),
                                VKName = jsonFriend.FirstName + " " + jsonFriend.LastName,
                                IsOnline = jsonFriend.IsOnline
                            });
                        }

                        if (UsersList.Count == 2)
                        {
                            UsersList[1] = new FriendCategory(newCat);
                        }
                    });
                }
            }
        }
    }
}
