﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Microsoft.Phone.Tasks;
using VKMessages.Tools;

namespace VKMessages.ViewModels
{
    public enum AttachmentType
    {
        Photo,
        Video,
        Document,
        Audio,
        Location
    }

    public class Attachment : BaseViewModel
    {
        public static AttachmentType String2AttachmentType(string val)
        {
            switch (val.ToLower())
            {
                case "photo":
                    return AttachmentType.Photo;
                case "audio":
                    return AttachmentType.Audio;
                case "video":
                    return AttachmentType.Video;
                case "doc":
                    return AttachmentType.Document;
                default:
                    return AttachmentType.Photo;
            }
        }

        public void DocumentUrlClick(object sender, RoutedEventArgs e)
        {
            var task = new WebBrowserTask {Uri = new Uri(Url, UriKind.Absolute)};

            task.Show();
        }

        private AttachmentType type;
        public AttachmentType Type
        {
            get { return type; }
            set
            {
                if (type != value)
                {
                    type = value;
                    NotifyPropertyChanged("Type");
                    NotifyPropertyChanged("PhotoVisibility");
                    NotifyPropertyChanged("AudioVisibility");
                    NotifyPropertyChanged("VideoVisibility");
                    NotifyPropertyChanged("DocumentVisibility");
                }
            }
        }

        private string id;
        public string Id
        {
            get { return id; }
            set
            {
                if (id != value)
                {
                    id = value;
                    NotifyPropertyChanged("Id");
                }
            }
        }

        public Visibility PhotoVisibility
        {
            get { return Type == AttachmentType.Photo && Image != null ? Visibility.Visible : Visibility.Collapsed; }
        }

        public Visibility AudioVisibility
        {
            get { return Type == AttachmentType.Audio ? Visibility.Visible : Visibility.Collapsed; }
        }

        public Visibility VideoVisibility
        {
            get { return Type == AttachmentType.Video && Image != null ? Visibility.Visible : Visibility.Collapsed; }
        }

        public Visibility DocumentVisibility
        {
            get { return Type == AttachmentType.Document ? Visibility.Visible : Visibility.Collapsed; }
        }

        public Visibility LocationVisibility
        {
            get { return Type == AttachmentType.Location ? Visibility.Visible : Visibility.Collapsed; }
        }

        private string latitude;
        public string Latitude
        {
            get { return latitude; }
            set
            {
                if (latitude != value)
                {
                    latitude = value;
                    NotifyPropertyChanged("Latitude");
                    NotifyPropertyChanged("LocationVisibility");
                }
            }
        }

        private string longitude;
        public string Longitude
        {
            get { return longitude; }
            set
            {
                if (longitude != value)
                {
                    longitude = value;
                    NotifyPropertyChanged("Longitude");
                    NotifyPropertyChanged("LocationVisibility");
                }
            }
        }

        private string url;
        public string Url
        {
            get { return url; }
            set
            {
                if (url != value)
                {
                    url = value;
                    NotifyPropertyChanged("AudioUri");
                }
            }
        }
        public string UrlBig { get; set; }

        private byte[] imageData;
        public byte[] ImageData
        {
            get { return imageData; }
            set
            {
                imageData = value;

                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    lock (imageData)
                    {
                        try
                        {
                            var stream = new MemoryStream(imageData);
                            Image = Microsoft.Phone.PictureDecoder.DecodeJpeg(stream);
                            stream.Close();
                        }
                        catch (Exception)
                        {
                        }
                    }
                });
            }
        }

        public Uri AudioUri
        {
            get
            {
                return !string.IsNullOrEmpty(Url) ? new Uri(Url, UriKind.Absolute) : null;
            }
        }

        private WriteableBitmap image;
        public WriteableBitmap Image
        {
            get
            {
                return image;
            }
            set
            {
                if (value != image)
                {
                    image = value;
                    NotifyPropertyChanged("Image");
                }
            }
        }

        private long videoDuration;
        public long VideoDuration
        { 
            get { return videoDuration; }
            set
            {
                if (value != videoDuration)
                {
                    videoDuration = value;
                    NotifyPropertyChanged("VideoDuration");
                    NotifyPropertyChanged("VideoDurationString");
                }
            } 
        }
        public string VideoDurationString
        {
            get
            {
                var timeStamp = TimeSpan.FromSeconds(videoDuration);
                return string.Format("{0:d2}:{1:d2}", timeStamp.Minutes, timeStamp.Seconds);
            }
        }

        private long audioDuration;
        public long AudioDuration
        {
            get { return audioDuration; }
            set
            {
                if (value != audioDuration)
                {
                    audioDuration = value;
                    NotifyPropertyChanged("AudioDuration");
                    NotifyPropertyChanged("AudioDurationString");
                }
            }
        }
        public string AudioDurationString
        {
            get
            {
                var timeStamp = TimeSpan.FromSeconds(audioDuration);
                return string.Format("{0:d2}:{1:d2}", timeStamp.Minutes, timeStamp.Seconds);
            }
        }

        private string audioArtist;
        public string AudioArtist
        {
            get { return audioArtist; }
            set
            {
                if (value != audioArtist)
                {
                    audioArtist = value;
                    NotifyPropertyChanged("AudioArtist");
                }
            }
        }

        private string audioTitle;
        public string AudioTitle
        {
            get { return audioTitle; }
            set
            {
                if (value != audioTitle)
                {
                    audioTitle = value;
                    NotifyPropertyChanged("AudioTitle");
                }
            }
        }

        private string docTitle;
        public string DocTitle
        {
            get { return docTitle; }
            set
            {
                if (value != docTitle)
                {
                    docTitle = value;
                    NotifyPropertyChanged("DocTitle");
                }
            }
        }

        private MediaElement mediaElement;
        public void MediaElementLoaded(object sender, RoutedEventArgs e)
        {
            mediaElement = sender as MediaElement;
        }

        private bool isPlaying;
        public bool IsPlaying
        {
            get { return isPlaying; }
            set
            {
                if (isPlaying != value)
                {
                    isPlaying = value;
                    NotifyPropertyChanged("PlayVisibility");
                    NotifyPropertyChanged("PauseVisibility");
                }
            }
        }

        public void PlayClick()
        {
            if (mediaElement != null)
            {
                if (!IsPlaying)
                {
                    mediaElement.Play();
                    IsPlaying = true;
                }
            }
        }

        public void PhotoClick()
        {
            CrossPageParameters.PreviewImageUrl = UrlBig;
            CrossPageParameters.PreviewVideoUrl = string.Empty;
            Navigate("PreviewPage");
        }

        public void VideoClick()
        {
            //CrossPageParameters.PreviewImageUrl = string.Empty;
            //CrossPageParameters.PreviewVideoUrl = Url;
            //Navigate("PreviewPage");
        }

        public void PauseClick()
        {
            if (mediaElement != null)
            {
                if (IsPlaying)
                {
                    if (mediaElement.CanPause)
                    {
                        mediaElement.Pause();                        
                    }
                }

                IsPlaying = false;
            }
        }

        public Visibility PlayVisibility
        {
            get { return IsPlaying ? Visibility.Collapsed : Visibility.Visible; }
        }

        public Visibility PauseVisibility
        {
            get { return IsPlaying ? Visibility.Visible: Visibility.Collapsed; }
        }
    }

    public class MessageItemViewModel : BaseViewModel
    {
        public MessageItemViewModel()
        {
        }

        public ObservableCollection<Attachment> Attachments { get; set; }

        private Attachment selectedAttachment;
        public Attachment SelectedAttachment
        {
            get { return selectedAttachment; }
            set
            {
                if (selectedAttachment != value)
                {
                    selectedAttachment = value;
                    NotifyPropertyChanged("SelectedAttachment");
                    NotifyPropertyChanged("AttachmentVisibility");
                }
            }
        }

        public Visibility AttachmentVisibility
        {
            get { return Attachments != null ? (Attachments.Count > 0 ? Visibility.Visible : Visibility.Collapsed) : Visibility.Collapsed; }
        }

        public void Tap(object sender, MouseButtonEventArgs e)
        {
            if (SelectedAttachment != null)
            {
                
            }
        }

        private bool @checked;
        public bool Checked
        {
            get
            {
                return @checked;
            }
            set
            {
                if (value != @checked)
                {
                    @checked = value;
                    NotifyPropertyChanged("Checked");
                }
            }
        }

        private DateTime date;
        public DateTime Date
        {
            get
            {
                return date;
            }
            set
            {
                if (value != date)
                {
                    date = value;
                    NotifyPropertyChanged("Date");
                }
            }
        }

        private long mid;
        public long MID
        {
            get
            {
                return mid;
            }
            set
            {
                if (value != mid)
                {
                    mid = value;
                    NotifyPropertyChanged("MID");
                }
            }
        }

        private long fromId;
        public long FromId
        {
            get
            {
                return fromId;
            }
            set
            {
                if (value != fromId)
                {
                    fromId = value;
                    NotifyPropertyChanged("FromId");
                }
            }
        }

        private string body;
        public string Body
        {
            get
            {
                return body;
            }
            set
            {
                if (value != body)
                {
                    body = value;
                    NotifyPropertyChanged("Body");
                }
            }
        }

        private bool readState;
        public bool ReadState
        {
            get
            {
                return readState;
            }
            set
            {
                if (value != readState)
                {
                    readState = value;
                    NotifyPropertyChanged("ReadState");
                }
            }
        }

        private bool _out;
        public bool Out
        {
            get
            {
                return _out;
            }
            set
            {
                if (value != _out)
                {
                    _out = value;
                    NotifyPropertyChanged("Out");
                }
            }
        }

        private long uid;
        public long UID
        {
            get
            {
                return uid;
            }
            set
            {
                if (value != uid)
                {
                    uid = value;
                    NotifyPropertyChanged("UID");
                }
            }
        }

        private string latitude;
        public string Latitude
        {
            get { return latitude; }
            set
            {
                if (latitude != value)
                {
                    latitude = value;
                    NotifyPropertyChanged("Latitude");
                    NotifyPropertyChanged("LocationString");
                    NotifyPropertyChanged("LocationVisibility");
                }
            }
        }

        private string longitude;
        public string Longitude
        {
            get { return longitude; }
            set
            {
                if (longitude != value)
                {
                    longitude = value;
                    NotifyPropertyChanged("Longitude");
                    NotifyPropertyChanged("LocationString");
                    NotifyPropertyChanged("LocationVisibility");
                }
            }
        }

        public Visibility LocationVisibility
        {
            get { return !string.IsNullOrEmpty(Latitude) && !string.IsNullOrEmpty(Longitude) ? Visibility.Visible : Visibility.Collapsed; }
        }

        public string Image50X50Url { get; set; }

        private byte[] image50X50Data;
        public byte[] Image50X50Data
        {
            get { return image50X50Data; }
            set
            {
                image50X50Data = value;

                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    lock (image50X50Data)
                    {
                        try
                        {
                            var stream = new MemoryStream(image50X50Data);
                            Image = Microsoft.Phone.PictureDecoder.DecodeJpeg(stream);

                            if (MainViewModel.FriendsImages.ContainsKey(FromId))
                                MainViewModel.FriendsImages[FromId] = Image;
                            else
                                MainViewModel.FriendsImages.Add(FromId, Image);

                            stream.Close();
                        }
                        catch (Exception)
                        {
                        }
                    }
                });
            }
        }

        private WriteableBitmap image;
        public WriteableBitmap Image
        {
            get
            {
                return image;
            }
            set
            {
                if (value != image)
                {
                    image = value;
                    NotifyPropertyChanged("Image");
                }
            }
        }

        private const string LocationUrlFormat = "http://maps.googleapis.com/maps/api/staticmap?center={0},{1}&zoom=12&size=180x70&sensor=false&language={2}";
        public string LocationImageUrl 
        { 
            get
            {
                return string.Format(LocationUrlFormat, Latitude, Longitude,
                                     CultureInfo.CurrentCulture.TwoLetterISOLanguageName);
            }
        }

        public string LocationString
        {
            get { return string.Format("{0} {1}", Latitude, Longitude); }
        }

        private byte[] locationImageData;
        public byte[] LocationImageData
        {
            get { return locationImageData; }
            set
            {
                locationImageData = value;

                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    lock (locationImageData)
                    {
                        try
                        {
                            var stream = new MemoryStream(locationImageData);
                            LocationImage = Microsoft.Phone.PictureDecoder.DecodeJpeg(stream);

                            stream.Close();
                        }
                        catch (Exception)
                        {
                        }
                    }
                });
            }
        }

        private WriteableBitmap locationImage;
        public WriteableBitmap LocationImage
        {
            get
            {
                return locationImage;
            }
            set
            {
                if (value != locationImage)
                {
                    locationImage = value;
                    NotifyPropertyChanged("LocationImage");
                }
            }
        }
    }
}
