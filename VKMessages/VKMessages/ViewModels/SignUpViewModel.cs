﻿using System.Windows;
using System.Windows.Input;
using VKMessages.API;
using VKMessages.Tools;

namespace VKMessages.ViewModels
{
    public class SignUpViewModel : BaseViewModel
    {
        public SignUpViewModel()
        {
            newAccount = new Commands.DelegateCommand(StartNewAccount);
        }

        private readonly ICommand newAccount;
        public ICommand NewAccount
        {
            get { return newAccount; }
        }

        private string phone;
        public string Phone
        {
            get
            {
                return phone;
            }
            set
            {
                if (value != phone)
                {
                    if (value.Length > 7)
                        VKApi.CheckPhone(value, CheckPhoneComplited);
                    phone = value;                    
                    NotifyPropertyChanged("Phone");
                }
            }
        }

        public bool DataIsOk
        {
            get
            {
                return Name != null && LastName != null && Name.Length > 2 && LastName.Length > 2 && PhoneIsOk;
            }
            set
            {
                NotifyPropertyChanged("DataIsOk");
            }
        }

        private bool phoneIsOk = false;
        public bool PhoneIsOk
        {
            get
            {
                return phoneIsOk;
            }
            set
            {
                if (value != phoneIsOk)
                {
                    phoneIsOk = value;
                    NotifyPropertyChanged("PhoneIsOk");
                    NotifyPropertyChanged("DataIsOk");
                }
            }
        }

        private string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (value != name)
                {
                    name = value;
                    NotifyPropertyChanged("Name");
                    NotifyPropertyChanged("DataIsOk");
                }
            }
        }

        private string lastName;
        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                if (value != lastName)
                {
                    lastName = value;
                    NotifyPropertyChanged("LastName");
                    NotifyPropertyChanged("DataIsOk");
                }
            }
        }

        public void StartNewAccount(object o)
        {
            CrossPageParameters.ConfirmPhoneNumber = Phone;
            VKApi.SignUp(Phone, Name, LastName, SignUpComplited);
        }

        private void CheckPhoneComplited(object sender, VKApiOperationCompletedEventArgs e)
        {
            PhoneIsOk = !e.Cancelled && e.IsSuccess;
        }

        private void SignUpComplited(object sender, VKApiOperationCompletedEventArgs e)
        {
            if (!e.Cancelled && e.IsSuccess)
            {
                MessageBox.Show(Resource.SIGNUP_WAIT_FOR_CODE);

                Navigate("ConfirmPage");

                return;
            }

            MessageBox.Show(Resource.SIGNUP_FAILED);
        }
    }
}
