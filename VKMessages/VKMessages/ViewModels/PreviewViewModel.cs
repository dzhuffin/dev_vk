﻿using VKMessages.Tools;

namespace VKMessages.ViewModels
{
    public class PreviewViewModel : BaseViewModel
    {
        public string VideoUrl
        {
            get { return CrossPageParameters.PreviewVideoUrl; }
        }

        public string ImageUrl
        {
            get { return CrossPageParameters.PreviewImageUrl; }
        }

        public void PageLoaded(object sender, System.Windows.RoutedEventArgs e)
        {
        }
    }
}
