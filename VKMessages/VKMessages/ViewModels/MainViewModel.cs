﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows;
using System.Collections.ObjectModel;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Microsoft.Devices;
using Microsoft.Phone.Controls;
using Microsoft.Phone.UserData;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using VKMessages.API;
using VKMessages.API.JSON;
using VKMessages.Controls;
using VKMessages.DB.Entities;
using VKMessages.Tools;

namespace VKMessages.ViewModels
{
    public enum MainStatus
    {
        WaitingForInput,
        ReceivingFriends,
        ReceivingDialogs,
        SyncingContacts,
        NetProblem
    }    

    public class HttpRequestParams
    {
        public HttpWebRequest Request;
        public int IntParam;
        public object Param;
        public AsyncCallback Callback;
    }

    public class MainViewModel : BaseViewModel
    {
        private readonly SoundEffect newMessageSoundEffect;

        public MainViewModel()
        {
            if (SharedPreferences.IsPushOn)
                PushNotifications.Enable();
            else
                PushNotifications.Disable();

            var stream = TitleContainer.OpenStream("Sounds/sound.wav");
            newMessageSoundEffect = SoundEffect.FromStream(stream);
            FrameworkDispatcher.Update();

            syncContacts = new Commands.DelegateCommand(ExecuteSyncContacts);
            RemoveBackEntry();
        }

        private MainStatus status = MainStatus.WaitingForInput;
        public MainStatus Status
        {
            get
            {
                return status;
            }
            set
            {
                if (value != status)
                {
                    status = value;
                    NotifyPropertyChanged("Status");
                    NotifyPropertyChanged("ProgressBarVisible");
                    NotifyPropertyChanged("ProgressBarText");
                    NotifyPropertyChanged("ContactSyncIsEnabled");
                    NotifyPropertyChanged("ContactsListVisibility");
                    NotifyPropertyChanged("ContactsListVisibilityInv");
                }
            }
        }

        private readonly ICommand syncContacts;
        public ICommand SyncContacts
        {
            get { return syncContacts; }
        }

        public Visibility UnreadMessagesVisibility
        {
            get { return UnreadMessagesCount > 0 ? Visibility.Visible : Visibility.Collapsed; }
        }
        private int unreadMessagesCount = 0;
        /// <summary>
        /// Unread messages count
        /// </summary>
        /// <returns></returns>
        public int UnreadMessagesCount
        {
            get
            {
                return unreadMessagesCount;
            }
            set
            {
                if (value != unreadMessagesCount)
                {
                    unreadMessagesCount = value;
                    NotifyPropertyChanged("UnreadMessagesCount");
                    NotifyPropertyChanged("UnreadMessagesVisibility");
                }
            }
        }

        private CollectionViewSource dialogsSort;
        public CollectionViewSource DialogsSort
        {
            get { return dialogsSort; }
            set
            {
                dialogsSort = value;
                NotifyPropertyChanged("DialogsSort");
                NotifyPropertyChanged("DialogsSort.View");
            }
        }

        public long FriendRequestsCount
        {
            get
            {
                return CrossPageParameters.FriendRequestsCount;
            }
            set
            {
                NotifyPropertyChanged("FriendRequestsCount");
                NotifyPropertyChanged("FriendRequestsVisibility");
            }
        }

        public void RefreshCounters()
        {
            NotifyPropertyChanged("FriendRequestsCount");
            NotifyPropertyChanged("FriendRequestsVisibility");
        }

        public Visibility FriendRequestsVisibility
        {
            get { return FriendRequestsCount > 0 ? Visibility.Visible : Visibility.Collapsed; }
        }

        public bool ProgressBarVisible
        {
            get
            {
                return Status != MainStatus.WaitingForInput;
            }
            set
            {
                NotifyPropertyChanged("ProgressBarVisible");
            }
        }

        public string ProgressBarText
        {
            get
            {
                switch (Status)
                {
                    case MainStatus.SyncingContacts:
                        return Resource.PBAR_SYNCING_CONTACTS;
                    case MainStatus.ReceivingDialogs:
                        return Resource.PBAR_RECEIVING_DIALOGS;
                    case MainStatus.ReceivingFriends:
                        return Resource.PBAR_RECEIVING_FRIENDS;
                    case MainStatus.NetProblem:
                        return Resource.PBAR_CONNECTING;
                    default:
                        return string.Empty;
                }
            }
            set
            {
                NotifyPropertyChanged("ProgressBarText");
            }
        }

        private ObservableCollection<ContactCategory> contactList = new ObservableCollection<ContactCategory>();
        /// <summary>
        /// Contact list
        /// </summary>
        /// <returns></returns>
        public ObservableCollection<ContactCategory> ContactList
        {
            get
            {
                return contactList;
            }
            set
            {
                if (value != contactList)
                {
                    contactList = value;
                    NotifyPropertyChanged("ContactList");
                }
            }
        }

        public bool ContactSyncIsEnabled
        {
            get
            {
                return Status == MainStatus.WaitingForInput && !SharedPreferences.IsContactsSynced;
            }
            set
            {
                NotifyPropertyChanged("ContactSyncIsEnabled");
            }
        }

        public Visibility ContactsListVisibility
        {
            get
            {
                return SharedPreferences.IsContactsSynced ? Visibility.Visible : Visibility.Collapsed;
            }
            set
            {
                NotifyPropertyChanged("ContactsListVisibility");
            }
        }

        public Visibility ContactsListVisibilityInv
        {
            get
            {
                return SharedPreferences.IsContactsSynced ? Visibility.Collapsed : Visibility.Visible;
            }
            set
            {
                NotifyPropertyChanged("ContactsListVisibilityInv");
            }
        }

        private ObservableCollection<FriendCategory> friendsList = new ObservableCollection<FriendCategory>();
        /// <summary>
        /// Friends list
        /// </summary>
        /// <returns></returns>
        public ObservableCollection<FriendCategory> FriendsList
        {
            get
            {
                return friendsList;
            }
            set
            {
                if (value != friendsList)
                {
                    friendsList = value;
                    NotifyPropertyChanged("FriendsList");
                }
            }
        }

        private ObservableCollection<DialogItemViewModel> dialogsList = new ObservableCollection<DialogItemViewModel>();
        /// <summary>
        /// Dialogs list
        /// </summary>
        /// <returns></returns>
        public ObservableCollection<DialogItemViewModel> DialogsList
        {
            get
            {
                return dialogsList;
            }
            set
            {
                if (value != dialogsList)
                {
                    dialogsList = value;
                    NotifyPropertyChanged("DialogsList");
                }
            }
        }

        private FriendItemViewModel selectedFriend;
        public FriendItemViewModel SelectedFriend
        {
            get
            {
                return selectedFriend;
            }
            set
            {
                if (selectedFriend != value)
                {
                    selectedFriend = value;
                    NotifyPropertyChanged("SelectedFriend");
                }
            }
        }

        private ContactItemViewModel selectedContact;
        public ContactItemViewModel SelectedContact
        {
            get
            {
                return selectedContact;
            }
            set
            {
                if (selectedContact != value)
                {
                    selectedContact = value;
                    NotifyPropertyChanged("SelectedContact");
                }
            }
        }

        private PivotItem selectedPivotItem;
        public PivotItem SelectedPivotItem
        {
            get
            {
                return selectedPivotItem;
            }
            set
            {
                if (selectedPivotItem != value)
                {
                    selectedPivotItem = value;
                    NotifyPropertyChanged("SelectedPivotItem");
                }
            }
        }

        private DialogItemViewModel selectedDialog;
        public DialogItemViewModel SelectedDialog
        {
            get
            {
                return selectedDialog;
            }
            set
            {
                if (selectedDialog != value)
                {
                    selectedDialog = value;
                    NotifyPropertyChanged("SelectedDialog");
                }
            }
        }

        public bool IsDataLoaded
        {
            get;
            private set;
        }

        public void GoToUnreadMessages(object sender, RoutedEventArgs e)
        {
        }

        public void GoToFriendsRequests(object sender, RoutedEventArgs e)
        {
            Navigate("FriendRequestsPage");
        }

        public static Dictionary<long, WriteableBitmap> FriendsImages = new Dictionary<long, WriteableBitmap>();

        public void FriendsLink(object sender, LinkUnlinkEventArgs e)
        {
            var category = e.ContentPresenter.Content as FriendCategory;

            if (category != null)
            {
                foreach (var item in category.Where(x => x.Image == null))
                {            
                    if (FriendsImages.ContainsKey(item.UID))
                    {
                        item.Image = FriendsImages[item.UID];
                        continue;
                    }
                    var request = (HttpWebRequest)WebRequest.Create(item.Image50X50Url);
                    request.AllowReadStreamBuffering = false;                    
                    //request.BeginGetResponse(DownloadImageCompleted, new HttpRequestParams { Request = request, Param = item });
                    ImageDownloader.AddTask(new HttpRequestParams { Request = request, Param = item, Callback = DownloadImageCompleted });
                }
            }
        }

        private void DownloadImageCompleted(IAsyncResult result)
        {
            var param = result.AsyncState as HttpRequestParams;

            if (param == null)
                return;

            var item = param.Param as FriendItemViewModel;

            if (item == null)
            {
                var ditem = param.Param as DialogItemViewModel;

                if (ditem == null)
                    return;

                if (param.Request == null)
                    return;

                Stream dresponseStream;
                HttpWebResponse dresponse;

                try
                {
                    dresponse = (HttpWebResponse)param.Request.EndGetResponse(result);
                    dresponseStream = dresponse.GetResponseStream();
                }
                catch (Exception)
                {
                    return;
                }
                

                if (dresponseStream.CanRead)
                {
                    switch (param.IntParam)
                    {
                        case 0:
                            ditem.Image50X50Data = new byte[dresponse.ContentLength];
                            dresponseStream.Read(ditem.Image50X50Data, 0, ditem.Image50X50Data.Length);
                            DB.DB.UpdateDialog(ditem.ChatId + 2000000000, null, string.Empty, ditem.Image50X50Data);
                            break;
                        case 1:
                            ditem.Image50X50Data2 = new byte[dresponse.ContentLength];
                            dresponseStream.Read(ditem.Image50X50Data2, 0, ditem.Image50X50Data2.Length);
                            break;
                        case 2:
                            ditem.Image50X50Data3 = new byte[dresponse.ContentLength];
                            dresponseStream.Read(ditem.Image50X50Data3, 0, ditem.Image50X50Data3.Length);
                            break;
                        case 3:
                            ditem.Image50X50Data4 = new byte[dresponse.ContentLength];
                            dresponseStream.Read(ditem.Image50X50Data4, 0, ditem.Image50X50Data4.Length);
                            break;
                    }                    
                }

                dresponseStream.Close();

                return;
            }

            if (param.Request == null)
                return;

            var response = (HttpWebResponse)param.Request.EndGetResponse(result);
 
            var responseStream = response.GetResponseStream();

            if (responseStream.CanRead)
            {
                item.Image50X50Data = new byte[response.ContentLength];
                responseStream.Read(item.Image50X50Data, 0, item.Image50X50Data.Length);
                DB.DB.UpdateFriendImage(item.UID, item.Image50X50Data);
            }

            responseStream.Close();
        }

        public void FriendsUnlink(object sender, LinkUnlinkEventArgs e)
        {

        }

        public void ContactsLink(object sender, LinkUnlinkEventArgs e)
        {
        }

        public void ContactsUnlink(object sender, LinkUnlinkEventArgs e)
        {

        }

        public void UpdateDialogImages()
        {
            foreach (var dialog in DialogsList)
            {
                if (FriendsImages.ContainsKey(dialog.UID))
                {
                    dialog.Image = FriendsImages[dialog.UID];
                }
            }
        }

        public void DialogsLink(object sender, LinkUnlinkEventArgs e)
        {
            var item = e.ContentPresenter.Content as DialogItemViewModel;

            if (item == null)
                return;

            if (item.ChatId > 0)
            {
                var isCurrentUser = false;                
                for (var i = 0; i < item.ActiveChatIds.Count && i < 5; i++)
                {
                    if (SharedPreferences.UserId == item.ActiveChatIds[i])
                    {
                        isCurrentUser = true;
                        continue;
                    }

                    if (FriendsImages.ContainsKey(item.ActiveChatIds[i]))
                    {
                        switch (i)
                        {
                            case 0:
                                item.Image = !isCurrentUser ? FriendsImages[item.ActiveChatIds[i]] : FriendsImages[item.ActiveChatIds[i - 1]];
                                break;
                            case 1:
                                item.Image2 = !isCurrentUser ? FriendsImages[item.ActiveChatIds[i]] : FriendsImages[item.ActiveChatIds[i - 1]];
                                break;
                            case 2:
                                item.Image3 = !isCurrentUser ? FriendsImages[item.ActiveChatIds[i]] : FriendsImages[item.ActiveChatIds[i - 1]];
                                break;
                            case 3:
                                item.Image4 = !isCurrentUser ? FriendsImages[item.ActiveChatIds[i]] : FriendsImages[item.ActiveChatIds[i - 1]];
                                break;
                        }
                    }
                    else
                    {
                        if (item.ActiveChatImageUrls != null && item.ActiveChatImageUrls.Count > 0 && !string.IsNullOrEmpty(item.ActiveChatImageUrls[i]))
                        {
                            var request = (HttpWebRequest)WebRequest.Create(item.ActiveChatImageUrls[i]);
                            request.AllowReadStreamBuffering = false;
                            ImageDownloader.AddTask(new HttpRequestParams { Request = request, Param = item, IntParam = i, Callback = DownloadImageCompleted });
                        }
                    }
                }

                //if (!founded && item.ActiveChatImageUrls != null && item.ActiveChatImageUrls.Count > 0 && !string.IsNullOrEmpty(item.ActiveChatImageUrls[0]))
                //{
                //    var request = (HttpWebRequest)WebRequest.Create(item.ActiveChatImageUrls[0]);
                //    request.AllowReadStreamBuffering = false;
                //    ImageDownloader.AddTask(new HttpRequestParams { Request = request, Param = item, IntParam = 0, Callback = DownloadImageCompleted });
                //}

                //if (!founded2 && item.ActiveChatImageUrls != null && item.ActiveChatImageUrls.Count > 1 && !string.IsNullOrEmpty(item.ActiveChatImageUrls[1]))
                //{
                //    var request = (HttpWebRequest)WebRequest.Create(item.ActiveChatImageUrls[1]);
                //    request.AllowReadStreamBuffering = false;
                //    ImageDownloader.AddTask(new HttpRequestParams { Request = request, Param = item, IntParam = 1, Callback = DownloadImageCompleted });
                //}

                //if (!founded3 && item.ActiveChatImageUrls != null && item.ActiveChatImageUrls.Count > 2 && !string.IsNullOrEmpty(item.ActiveChatImageUrls[2]))
                //{
                //    var request = (HttpWebRequest)WebRequest.Create(item.ActiveChatImageUrls[2]);
                //    request.AllowReadStreamBuffering = false;
                //    ImageDownloader.AddTask(new HttpRequestParams { Request = request, Param = item, IntParam = 2, Callback = DownloadImageCompleted });
                //}

                //if (!founded4 && item.ActiveChatImageUrls != null && item.ActiveChatImageUrls.Count > 3 && !string.IsNullOrEmpty(item.ActiveChatImageUrls[3]))
                //{
                //    var request = (HttpWebRequest)WebRequest.Create(item.ActiveChatImageUrls[3]);
                //    request.AllowReadStreamBuffering = false;
                //    ImageDownloader.AddTask(new HttpRequestParams { Request = request, Param = item, IntParam = 3, Callback = DownloadImageCompleted });
                //}
            }
            else
            {
                if (FriendsImages.ContainsKey(item.UID))
                {
                    item.Image = FriendsImages[item.UID];
                    return;
                }

                if (!string.IsNullOrEmpty(item.Image50X50Url))
                {
                    var request = (HttpWebRequest)WebRequest.Create(item.Image50X50Url);
                    request.AllowReadStreamBuffering = false;
                    request.BeginGetResponse(DownloadImageCompleted, new HttpRequestParams { Request = request, Param = item });
                }
            }

            //if (item.UID == DialogsList.Last().UID)
            //{
            //    Status = MainStatus.ReceivingDialogs;
            //    VKApi.DialogsGet(DialogsList.Count, GetDialogsComplited);
            //}
        }

        public void DialogsUnink(object sender, LinkUnlinkEventArgs e)
        {

        }

        /// <summary>
        /// Creates and adds a few ItemViewModel objects into the Items collection.
        /// </summary>
        public void LoadData()
        {
            Status = MainStatus.ReceivingFriends;
            VKApi.GetUnreadCount(GetUnreadCountCompleted);

            DialogsList = DB.DB.GetDialogsList();

            DialogsSort = new CollectionViewSource();
            DialogsSort.SortDescriptions.Add(new SortDescription("Date", ListSortDirection.Descending));
            DialogsSort.Source = DialogsList;

            FriendsList = DB.DB.GetFriendsList();
            VKApi.FriendsAndDialogsGet(GetFriendsAndDialogsComplited);

            ContactList = DB.DB.GetContactsList();

            this.IsDataLoaded = true;
        }

        void Contacts_SearchCompleted(object sender, ContactsSearchEventArgs e)
        {
            try
            {
                var phonesListStr = e.Results.
                    Select(x => x.PhoneNumbers).
                    Select(y => y.Aggregate(string.Empty, (list1, phone1) => list1 + "," + phone1.PhoneNumber)).
                    Aggregate(string.Empty, (list2, phone2) => list2 + phone2);

                VKApi.GetByPhones(phonesListStr, e.Results.ToList(), GetByPhonesComplited);
            }
            catch (Exception ee)
            {
                //That's okay, no results
            }
        }

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            NotifyPropertyChanged("ContactList");
        }

        private void GetFriendsAndDialogsComplited(object sender, VKApiOperationCompletedEventArgs e)
        {
            if (e.Cancelled || !e.IsSuccess)
            {
                Status = MainStatus.NetProblem;
                //Thread.Sleep(5000);
                //VKApi.FriendsAndDialogsGet(GetFriendsAndDialogsComplited);
            }
            else
            {
                FriendsList = DB.DB.GetFriendsList();
                DialogsList = DB.DB.GetDialogsList();

                DialogsSort = new CollectionViewSource();
                DialogsSort.SortDescriptions.Add(new SortDescription("Date", ListSortDirection.Descending));
                DialogsSort.Source = DialogsList;

                Status = MainStatus.WaitingForInput;

                VKApi.GetFriendRequestsCount(GetFriendRequestsCountComplited);

                VKApi.RequestToLongPool(CrossPageParameters.LongPoolData, LongPoolRequestComplited);

                //VKApi.FriendsGet(20, GetFriendsComplited);
            }

            ImageDownloader.Start();
        }


        private void GetFriendRequestsCountComplited(object sender, VKApiOperationCompletedEventArgs e)
        {
            if (!e.Cancelled && e.IsSuccess)
            {
                CrossPageParameters.FriendRequestsCount = (int)e.Result;
            }
        }

        private void GetFriendsComplited(object sender, VKApiOperationCompletedEventArgs e)
        {
            if (e.Cancelled || !e.IsSuccess)
            {
                //Status = MainStatus.NetProblem;
                //VKApi.FriendsAndDialogsGet(GetFriendsComplited);
            }
            else
            {
                Status = MainStatus.WaitingForInput;
                FriendsList = DB.DB.GetFriendsList();
            }
        }

        public void OnDialogsStretching(object sender, StretchingEventArgs e)
        {
            if (!e.IsTop)
            {
                Status = MainStatus.ReceivingDialogs;
                VKApi.DialogsGet(DialogsList.Count, GetDialogsComplited);
            }
        }

        public void OnFriendsStretching(object sender, StretchingEventArgs e)
        {
            //var listControl = sender as FixedLongListSelector;

            //if (listControl == null)
            //    return;

            //if (!e.IsTop && Status == MainStatus.WaitingForInput)
            //{
            //    Status = MainStatus.ReceivingFriends;
            //    VKApi.FriendsGet(20, GetFriendsComplited);
            //}
        }

        private void GetByPhonesComplited(object sender, VKApiOperationCompletedEventArgs e)
        {
            if (e.Cancelled || !e.IsSuccess)
            {
                Status = MainStatus.WaitingForInput;
                //Status = MainStatus.NetProblem;
                //AppBarRefresh(null, new EventArgs());
            }
            else
            {
                ContactList = DB.DB.GetContactsList();

                Status = MainStatus.WaitingForInput;
                SharedPreferences.IsContactsSynced = true;
                NotifyPropertyChanged("ContactSyncIsEnabled");
                NotifyPropertyChanged("ContactsListVisibility");
                NotifyPropertyChanged("ContactsListVisibilityInv");
            }
        }

        private void GetDialogsComplited(object sender, VKApiOperationCompletedEventArgs e)
        {
            if (e.Cancelled || !e.IsSuccess)
            {
                Status = MainStatus.NetProblem;
                //VKApi.DialogsGet(0, GetDialogsComplited);
            }
            else
            {
                Status = MainStatus.WaitingForInput;
                DialogsList = DB.DB.GetDialogsList();

                DialogsSort = new CollectionViewSource();
                DialogsSort.SortDescriptions.Add(new SortDescription("Date", ListSortDirection.Descending));
                DialogsSort.Source = DialogsList;

                foreach (var dialogItemViewModel in DialogsList.Where(x => x.ChatId > 0 && (x.ActiveChatImageUrls == null || x.ActiveChatImageUrls.Count == 0)))
                {
                    foreach (var activeChatId in dialogItemViewModel.ActiveChatIds)
                    {
                        var exit = false;

                        foreach (var friendCategory in FriendsList)
                        {
                            foreach (var friend in friendCategory)
                            {
                                if (friend.UID == activeChatId)
                                {
                                    if (dialogItemViewModel.ActiveChatImageUrls == null)
                                        dialogItemViewModel.ActiveChatImageUrls = new List<string>();

                                    dialogItemViewModel.ActiveChatImageUrls.Add(friend.Image50X50Url);
                                    exit = true;
                                    break;
                                }
                            }

                            if (exit)
                            break;
                        }
                    }
                }
            }
        }


        public void AddDialog(DialogItemViewModel newVM)
        {
            if (DialogsList == null)
            {
                DialogsList = new ObservableCollection<DialogItemViewModel>();
                DialogsSort = new CollectionViewSource();
                DialogsSort.SortDescriptions.Add(new SortDescription("Date", ListSortDirection.Descending));
                DialogsSort.Source = DialogsList;
            }

            var user = DialogsList.FirstOrDefault(x => newVM.ChatId > 0 ? x.ChatId == newVM.ChatId : x.UID == newVM.UID);
                
            if (user == null)
            {
                DialogsList.Add(newVM);
                DialogsSort.View.Refresh();
                return;
            }
            
            user.Body = newVM.Body;
            user.Date = newVM.Date;
            user.IsUnread = newVM.IsUnread;
            DialogsSort.View.Refresh();
        }

        private long lastMsgChatId;
        public void LongPoolRequestComplited(object sender, VKApiOperationCompletedEventArgs e)
        {
            if (!e.Cancelled && e.IsSuccess)
            {
                var resp = e.Result as JsonLongPoolUpdatesResponce;
                if (resp != null)
                {
                    if (resp.Updates != null && resp.Updates.Count > 0)
                    {
                        foreach (var update in resp.Updates)
                        {
                            if (update.Count == 0)
                                continue;

                            switch ((int)update[0])
                            {
                                case (int)JsonUpdateType.MessageDeleted:
                                    break;
                                case (int)JsonUpdateType.NewMessageRecieved:
                                    VKApi.GetUnreadCount(GetUnreadCountCompleted);
                                    if (update.Count < 5)
                                        break;
                                    var flags = Convert.ToInt64(update[2]);
                                    var fromId = Convert.ToInt64(update[3]);

                                    var isChat = false;
                                    if (fromId > 2000000000)
                                    {
                                        fromId -= 2000000000;
                                        isChat = true;
                                    }

                                    var msg = new DBMessage
                                                  {
                                                      MID = Convert.ToInt64(update[1]),
                                                      Body = Convert.ToString(update[6]).Replace("<br>", "\n"),
                                                      Date = Convert.ToInt64(update[4]),
                                                      Out = (int) (flags & (long) JsonMessageFlagsType.Outbox),
                                                      ReadState = (int)(flags & (long)JsonMessageFlagsType.Unread) == 0 ? 1 : 0
                                                  };
                                    msg.FromId = msg.Out != 0 ? 0: fromId;                                    
                                    msg.ToId = isChat ? (fromId + 2000000000) : (msg.Out != 0 ? fromId : 0);
                                    msg.ChatId = isChat ? fromId : 0;
                                    DB.DB.InsertMessage(msg, new JsonMessageSendResponce {MID = msg.MID});
                                    if (SharedPreferences.IsSoundOn && msg.Out == 0)
                                        newMessageSoundEffect.Play();
                                    if (SharedPreferences.IsVibrateOn && msg.Out == 0)
                                    {
                                        VibrateController.Default.Start(TimeSpan.FromMilliseconds(300));
                                        VibrateController.Default.Start(TimeSpan.FromMilliseconds(300));
                                        VibrateController.Default.Start(TimeSpan.FromMilliseconds(300));
                                    }

                                    if (!isChat)
                                    {
                                        var vm = App.GetConversationViewModelByUID(fromId);
                                        if (vm != null && vm.Messages != null)
                                        {
                                            vm.AddMessage(new MessageItemViewModel
                                                              {
                                                                  Body = msg.Body,
                                                                  Checked = false,
                                                                  Date =
                                                                      (new DateTime(1970, 1, 1, 0, 0, 0, 0,
                                                                                    DateTimeKind.Utc) +
                                                                       TimeSpan.FromSeconds(msg.Date)).ToLocalTime(),
                                                                  FromId = msg.Out != 0 ? 0 : msg.FromId,
                                                                  MID = msg.MID,
                                                                  Out = msg.Out != 0,
                                                                  ReadState = msg.ReadState != 0
                                                              });
                                        }

                                        if (resp.HasAttachments)
                                            VKApi.GetMessageById(msg.MID, GetMessageByIdCompleted);
                                    }
                                    else
                                    {
                                        var chatVM = App.GetChatViewModelByUID(fromId);
                                        if (chatVM != null && chatVM.Messages != null)
                                        {
                                            chatVM.AddMessage(new MessageItemViewModel
                                            {
                                                Body = msg.Body,
                                                Checked = false,
                                                Date =
                                                    (new DateTime(1970, 1, 1, 0, 0, 0, 0,
                                                                  DateTimeKind.Utc) +
                                                     TimeSpan.FromSeconds(msg.Date)).ToLocalTime(),
                                                FromId = msg.Out != 0 ? 0 : msg.FromId,
                                                MID = msg.MID,
                                                Out = msg.Out != 0,
                                                ReadState = msg.ReadState != 0
                                            });
                                        }
                                    }
                                    AddDialog(new DialogItemViewModel
                                                  {
                                                      Body = msg.Body.Length > 40 ? msg.Body.Substring(0, 40) : msg.Body,
                                                      Date = (new DateTime(1970, 1, 1, 0, 0, 0, 0,
                                                                                 DateTimeKind.Utc) +
                                                                    TimeSpan.FromSeconds(msg.Date)).ToLocalTime(),
                                                      UID = isChat ? 0 : (msg.Out != 0 ? msg.ToId : msg.FromId),
                                                      ChatId = isChat ? msg.ChatId : 0,
                                                      IsUnread = msg.Out == 0
                                                  });
                                    if (resp.HasAttachments)
                                    {
                                        lastMsgChatId = msg.ChatId;
                                        VKApi.GetChatMessageById(msg.MID, GetChatMessageByIdCompleted);
                                    }
                                    break;
                                case (int)JsonUpdateType.FriendOnline:
                                    break;
                                case (int)JsonUpdateType.FriendOffline:
                                    break;
                            }
                        }
                    }

                    CrossPageParameters.LongPoolData.Ts = resp.Ts;
                    VKApi.RequestToLongPool(CrossPageParameters.LongPoolData, LongPoolRequestComplited);
                }
            }
        }


        private void GetChatMessageByIdCompleted(object sender, VKApiOperationCompletedEventArgs e)
        {
            if (!e.Cancelled && e.IsSuccess)
            {
                var resp = e.Result as List<JsonMessage>;
                if (resp == null || resp.Count < 1)
                    return;

                var msg = resp[0];

                DB.DB.UpdateMessage(msg, null);

                var vm = App.GetChatViewModelByUID(lastMsgChatId);
                if (vm != null && vm.Messages != null)
                {
                    var existMsg = vm.Messages.FirstOrDefault(x => x.MID == msg.MID);
                    if (existMsg == null)
                        return;

                    existMsg.Latitude = msg.Latitude;
                    existMsg.Longitude = msg.Longitude;
                    if (msg.Attachments != null && msg.Attachments.Count > 0)
                    {
                        existMsg.Attachments = new ObservableCollection<Attachment>(
                            msg.Attachments.Select(x =>
                                                   new Attachment
                                                       {
                                                           Url =
                                                               x.Photo != null
                                                                   ? x.Photo.Url
                                                                   : (x.Video != null
                                                                          ? x.Video.Url
                                                                          : (x.Audio != null
                                                                                 ? x.Audio.Url
                                                                                 : (x.Document != null
                                                                                        ? x.Document.Url
                                                                                        : string.Empty))),
                                                           UrlBig =
                                                               x.Photo != null
                                                                   ? x.Photo.UrlBig
                                                                   : (x.Video != null ? x.Video.UrlBig : string.Empty),
                                                           AudioTitle = x.Audio != null ? x.Audio.Title : string.Empty,
                                                           DocTitle =
                                                               x.Document != null ? x.Document.Title : string.Empty,
                                                           AudioArtist =
                                                               x.Audio != null ? x.Audio.Performer : string.Empty,
                                                           VideoDuration = x.Video != null ? x.Video.Duration : 0,
                                                           AudioDuration = x.Audio != null ? x.Audio.Duration : 0,
                                                           Type = Attachment.String2AttachmentType(x.Type)
                                                       }));
                    }

                    vm.RefreshLocationImage(existMsg);
                    vm.RefreshAttachmentsImages(existMsg);
                }
            }
        }

        private void GetMessageByIdCompleted(object sender, VKApiOperationCompletedEventArgs e)
        {
            if (!e.Cancelled && e.IsSuccess)
            {
                var resp = e.Result as List<JsonMessage>;
                if (resp == null || resp.Count < 1)
                    return;

                var msg = resp[0];

                DB.DB.UpdateMessage(msg, null);

                var vm = App.GetConversationViewModelByUID(msg.UID);
                if (vm != null && vm.Messages != null)
                {
                    var existMsg = vm.Messages.FirstOrDefault(x => x.MID == msg.MID);
                    if (existMsg == null)
                        return;

                    existMsg.Latitude = msg.Latitude;
                    existMsg.Longitude = msg.Longitude;
                    if (msg.Attachments != null && msg.Attachments.Count > 0)
                    {
                        existMsg.Attachments = new ObservableCollection<Attachment>(
                            msg.Attachments.Select(x =>
                                                   new Attachment
                                                       {
                                                           Url =
                                                               x.Photo != null
                                                                   ? x.Photo.Url
                                                                   : (x.Video != null
                                                                          ? x.Video.Url
                                                                          : (x.Audio != null
                                                                                 ? x.Audio.Url
                                                                                 : (x.Document != null
                                                                                        ? x.Document.Url
                                                                                        : string.Empty))),
                                                           UrlBig =
                                                               x.Photo != null
                                                                   ? x.Photo.UrlBig
                                                                   : (x.Video != null ? x.Video.UrlBig : string.Empty),
                                                           AudioTitle = x.Audio != null ? x.Audio.Title : string.Empty,
                                                           DocTitle =
                                                               x.Document != null ? x.Document.Title : string.Empty,
                                                           AudioArtist =
                                                               x.Audio != null ? x.Audio.Performer : string.Empty,
                                                           VideoDuration = x.Video != null ? x.Video.Duration : 0,
                                                           AudioDuration = x.Audio != null ? x.Audio.Duration : 0,
                                                           Type = Attachment.String2AttachmentType(x.Type)
                                                       }));
                    }

                    vm.RefreshLocationImage(existMsg);
                    vm.RefreshAttachmentsImages(existMsg);
                }
            }
        }

        public void ExecuteSyncContacts(object o)
        {
            if (SharedPreferences.IsContactsSynced)
                return;

            Status = MainStatus.SyncingContacts;

            var contacts = new Contacts();

            contacts.SearchCompleted += Contacts_SearchCompleted;
            contacts.SearchAsync(string.Empty, FilterKind.None, string.Empty);
        }

        public void FriendClick(object sender, MouseButtonEventArgs e)
        {
            if (SelectedFriend != null)
            {
                CrossPageParameters.FriendUId = SelectedFriend.UID;
                CrossPageParameters.FriendVKName = SelectedFriend.VKName;
                CrossPageParameters.FriendIsOnline = SelectedFriend.IsOnline;
                Navigate("ConversationPage");
            }
        }

        public void ContactClick(object sender, MouseButtonEventArgs e)
        {
            if (SelectedContact != null)
            {
                CrossPageParameters.CurrentContactVM = SelectedContact;
                Navigate("ContactPage");
            }
        }

        public void DialogClick(object sender, MouseButtonEventArgs e)
        {
            if (SelectedDialog != null)
            {
                SelectedDialog.IsUnread = false;

                if (SelectedDialog.ChatId > 0)
                {
                    CrossPageParameters.ChatId = SelectedDialog.ChatId;
                    CrossPageParameters.ChatName = SelectedDialog.Title;
                    CrossPageParameters.CurrentTitle = SelectedDialog.Title;
                    CrossPageParameters.CurrentChatUIDs = new List<long>(SelectedDialog.ActiveChatIds);
                    CrossPageParameters.CurrentDialog = SelectedDialog;
                    VKApi.GetUnreadCount(GetUnreadCountCompleted);
                    Navigate("ChatPage");
                }
                else
                {
                    CrossPageParameters.FriendUId = SelectedDialog.UID;
                    CrossPageParameters.FriendVKName = SelectedDialog.VKName;
                    CrossPageParameters.CurrentTitle = SelectedDialog.VKName;
                    CrossPageParameters.CurrentDialog = SelectedDialog;
                    CrossPageParameters.FriendIsOnline = SelectedDialog.IsOnline;
                    VKApi.GetUnreadCount(GetUnreadCountCompleted);
                    Navigate("ConversationPage");
                }
            }
        }

        private void GetUnreadCountCompleted(object sender, VKApiOperationCompletedEventArgs e)
        {
            if (!e.Cancelled && e.IsSuccess)
            {
                UnreadMessagesCount = (int)e.Result;
            }
        }

        public void AppBarSettingsShow(object sender, EventArgs e)
        {
            Navigate("SettingsPage");
        }

        public void AppBarRefresh(object sender, EventArgs e)
        {
            if (SelectedPivotItem.Name == "ContactsPivot" && Status == MainStatus.WaitingForInput)
            {
                SharedPreferences.IsContactsSynced = false;
                ExecuteSyncContacts(null);
            }
            else
            {
                Status = MainStatus.ReceivingDialogs;
                VKApi.FriendsAndDialogsGet(GetFriendsAndDialogsComplited);
            }
        }

        public void AppBarSearch(object sender, EventArgs e)
        {
            Navigate("SearchPage");
        }

        public void AppBarAdd(object sender, EventArgs e)
        {
            CrossPageParameters.CurrentChatVM = null;
            CrossPageParameters.CurrentChatUIDs = null;
            Navigate("CreateChatPage");
        }
    }
}