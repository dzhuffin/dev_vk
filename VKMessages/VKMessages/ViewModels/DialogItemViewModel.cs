﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace VKMessages.ViewModels
{
    public class DialogItemViewModel : BaseViewModel
    {
        public DialogItemViewModel()
        {
            openConversation = new Commands.DelegateCommand(ExecuteOpenConversation);
        }

        private readonly ICommand openConversation;
        public ICommand OpenConversation
        {
            get { return openConversation; }
        }

        public Brush UnreadColor
        {
            get { return IsUnread ? (Brush)Application.Current.Resources["PhoneAccentBrush"] : (Brush)Application.Current.Resources["PhoneForegroundBrush"]; }
        }

        private bool isUnread;
        public bool IsUnread
        {
            get
            {
                return isUnread;
            }
            set
            {
                if (value != isUnread)
                {
                    isUnread = value;
                    NotifyPropertyChanged("IsUnread");
                    NotifyPropertyChanged("UnreadColor");
                }
            }
        }

        private DateTime date;
        public DateTime Date
        {
            get
            {
                return date;
            }
            set
            {
                if (value != date)
                {
                    date = value;
                    NotifyPropertyChanged("Date");
                    NotifyPropertyChanged("DateString");
                }
            }
        }

        public string Image50X50Url { get; set; }

        private byte[] image50X50Data;
        public byte[] Image50X50Data
        {
            get { return image50X50Data; }
            set
            {
                image50X50Data = value;

                if (value == null)
                    return;

                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    lock (image50X50Data)
                    {
                        try
                        {
                            var stream = new MemoryStream(image50X50Data);
                            Image = Microsoft.Phone.PictureDecoder.DecodeJpeg(stream);

                            if (MainViewModel.FriendsImages.ContainsKey(UID))
                                MainViewModel.FriendsImages[UID] = Image;
                            else
                                MainViewModel.FriendsImages.Add(UID, Image);

                            stream.Close();
                        }
                        catch (Exception)
                        {
                        }
                    }
                });
            }
        }

        private WriteableBitmap image;
        public WriteableBitmap Image
        {
            get
            {
                return image;
            }
            set
            {
                if (value != image)
                {
                    image = value;
                    NotifyPropertyChanged("Image");
                }
            }
        }

        private byte[] image50X50Data2;
        public byte[] Image50X50Data2
        {
            get { return image50X50Data2; }
            set
            {
                image50X50Data2 = value;

                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    lock (image50X50Data2)
                    {
                        try
                        {
                            var stream = new MemoryStream(image50X50Data2);
                            Image2 = Microsoft.Phone.PictureDecoder.DecodeJpeg(stream);

                            if (MainViewModel.FriendsImages.ContainsKey(UID))
                                MainViewModel.FriendsImages[UID] = Image2;
                            else
                                MainViewModel.FriendsImages.Add(UID, Image2);

                            stream.Close();
                        }
                        catch (Exception)
                        {
                        }
                    }
                });
            }
        }

        private WriteableBitmap image2;
        public WriteableBitmap Image2
        {
            get
            {
                return image2;
            }
            set
            {
                if (value != image2)
                {
                    image2 = value;
                    NotifyPropertyChanged("Image2");
                }
            }
        }

        private byte[] image50X50Data3;
        public byte[] Image50X50Data3
        {
            get { return image50X50Data3; }
            set
            {
                image50X50Data3 = value;

                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    lock (image50X50Data3)
                    {
                        try
                        {
                            var stream = new MemoryStream(image50X50Data3);
                            Image3 = Microsoft.Phone.PictureDecoder.DecodeJpeg(stream);

                            if (MainViewModel.FriendsImages.ContainsKey(UID))
                                MainViewModel.FriendsImages[UID] = Image3;
                            else
                                MainViewModel.FriendsImages.Add(UID, Image3);

                            stream.Close();
                        }
                        catch (Exception)
                        {
                        }
                    }
                });
            }
        }

        private WriteableBitmap image3;
        public WriteableBitmap Image3
        {
            get
            {
                return image3;
            }
            set
            {
                if (value != image3)
                {
                    image3 = value;
                    NotifyPropertyChanged("Image3");
                }
            }
        }

        private byte[] image50X50Data4;
        public byte[] Image50X50Data4
        {
            get { return image50X50Data4; }
            set
            {
                image50X50Data4 = value;

                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    lock (image50X50Data4)
                    {
                        try
                        {
                            var stream = new MemoryStream(image50X50Data4);
                            Image4 = Microsoft.Phone.PictureDecoder.DecodeJpeg(stream);

                            if (MainViewModel.FriendsImages.ContainsKey(UID))
                                MainViewModel.FriendsImages[UID] = Image4;
                            else
                                MainViewModel.FriendsImages.Add(UID, Image4);

                            stream.Close();
                        }
                        catch (Exception)
                        {
                        }
                    }
                });
            }
        }

        private WriteableBitmap image4;
        public WriteableBitmap Image4
        {
            get
            {
                return image4;
            }
            set
            {
                if (value != image4)
                {
                    image4 = value;
                    NotifyPropertyChanged("Image4");
                }
            }
        }

        private string title;
        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                if (value != title)
                {
                    title = value;
                    NotifyPropertyChanged("Title");
                    NotifyPropertyChanged("VKName");
                }
            }
        }

        private string body;
        public string Body
        {
            get
            {
                if (string.IsNullOrEmpty(body))
                    return Resource.ATTACHMENT;

                return body;
            }
            set
            {
                if (value != body)
                {
                    body = value;
                    NotifyPropertyChanged("Body");
                }
            }
        }

        public string VKName
        {
            get
            {
                return ChatId > 0 ? 
                            Title : 
                            string.Format("{0} {1}", FirstName, LastName);
            }
            set
            {
                NotifyPropertyChanged("VKName");
            }
        }

        private string firstName;
        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                if (value != firstName)
                {
                    firstName = value;
                    NotifyPropertyChanged("FirstName");
                    NotifyPropertyChanged("VKName");
                }
            }
        }

        private string lastName;
        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                if (value != lastName)
                {
                    lastName = value;
                    NotifyPropertyChanged("LastName");
                    NotifyPropertyChanged("VKName");
                }
            }
        }

        private bool isOnline;
        public bool IsOnline
        {
            get
            {
                return isOnline;
            }
            set
            {
                if (value != isOnline)
                {
                    isOnline = value;
                    NotifyPropertyChanged("IsOnline");
                }
            }
        }

        private long uid;
        public long UID
        {
            get
            {
                return uid;
            }
            set
            {
                if (value != uid)
                {
                    uid = value;
                    NotifyPropertyChanged("UID");
                }
            }
        }

        private long chatId;
        public long ChatId
        {
            get
            {
                return chatId;
            }
            set
            {
                if (value != chatId)
                {
                    chatId = value;
                    NotifyPropertyChanged("ChatId");
                    NotifyPropertyChanged("FourImagesVisibility");
                    NotifyPropertyChanged("FourImagesVisibilityInv");
                }
            }
        }

        public List<long> ActiveChatIds { get; set; }
        public List<string> ActiveChatImageUrls { get; set; }

        public Visibility FourImagesVisibility
        {
            get
            {
                return ChatId > 0 ? Visibility.Visible : Visibility.Collapsed;
            }
            set
            {
                NotifyPropertyChanged("FourImagesVisibility");
            }
        }

        public Visibility FourImagesVisibilityInv
        {
            get
            {
                return ChatId > 0 ? Visibility.Collapsed : Visibility.Visible;
            }
            set
            {
                NotifyPropertyChanged("FourImagesVisibilityInv");
            }
        }

        public void ExecuteOpenConversation(object o)
        {

        }
    }
}
