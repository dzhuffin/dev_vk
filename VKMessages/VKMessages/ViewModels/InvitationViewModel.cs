﻿using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace VKMessages.ViewModels
{
    public class InvitationViewModel : BaseViewModel
    {
        public InvitationViewModel()
        {
            sendInvitation = new Commands.DelegateCommand(StartSendInvitation);
        }

        private string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (value != name)
                {
                    name = value;
                    NotifyPropertyChanged("Name");
                }
            }
        }

        private BitmapImage accountPhoto = new BitmapImage();
        public BitmapImage AccountPhoto
        {
            get
            {
                return accountPhoto;
            }
            set
            {
                if (value != accountPhoto)
                {
                    accountPhoto = value;
                    NotifyPropertyChanged("AccountPhoto");
                }
            }
        }

        private readonly ICommand sendInvitation;
        public ICommand SendInvitation
        {
            get { return sendInvitation; }
        }

        public void StartSendInvitation(object o)
        {
            Navigate("LoginPage");
        }
    }
}
