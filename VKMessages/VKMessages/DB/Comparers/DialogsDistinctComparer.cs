﻿using System.Collections.Generic;
using VKMessages.DB.Entities;

namespace VKMessages.DB.Comparers
{
    public class DialogsDistinctComparer : IEqualityComparer<DBDialog>
    {
        public bool Equals(DBDialog x, DBDialog y)
        {
            return ReferenceEquals(x, y) || x.UID.Equals(y.UID);
        }

        public int GetHashCode(DBDialog item)
        {
            return ReferenceEquals(item, null) ?
                0 :
                item.UID.GetHashCode();
        }
    }
}
