﻿using System.Collections.Generic;
using VKMessages.ViewModels;

namespace VKMessages.DB.Comparers
{
    public class MessageItemDistinctComparer : IEqualityComparer<MessageItemViewModel>
    {
        public bool Equals(MessageItemViewModel x, MessageItemViewModel y)
        {
            return ReferenceEquals(x, y) || x.MID.Equals(y.MID);
        }

        public int GetHashCode(MessageItemViewModel item)
        {
            return ReferenceEquals(item, null) ?
                0 :
                item.MID.GetHashCode();
        }
    }
}
