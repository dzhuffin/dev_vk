﻿using System.Collections.Generic;
using VKMessages.DB.Entities;

namespace VKMessages.DB.Comparers
{
    public class FriendsDistinctComparer : IEqualityComparer<DBFriend>
    {
        public bool Equals(DBFriend x, DBFriend y)
        {
            return ReferenceEquals(x, y) || x.UID.Equals(y.UID);
        }

        public int GetHashCode(DBFriend item)
        {
            return ReferenceEquals(item, null) ? 
                0 : 
                item.UID.GetHashCode();
        }
    }
}
