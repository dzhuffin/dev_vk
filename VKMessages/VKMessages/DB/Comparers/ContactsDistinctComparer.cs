﻿using System.Collections.Generic;
using VKMessages.DB.Entities;

namespace VKMessages.DB.Comparers
{
    public class ContactsDistinctComparer : IEqualityComparer<DBContact>
    {
        public bool Equals(DBContact x, DBContact y)
        {
            return ReferenceEquals(x, y) || x.DisplayName.Equals(y.DisplayName);
        }

        public int GetHashCode(DBContact item)
        {
            return ReferenceEquals(item, null) ?
                0 :
                item.DisplayName.GetHashCode();
        }
    }
}
