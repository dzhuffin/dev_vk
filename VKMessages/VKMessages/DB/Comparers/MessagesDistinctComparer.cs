﻿using System.Collections.Generic;
using VKMessages.DB.Entities;

namespace VKMessages.DB.Comparers
{
    public class MessagesDistinctComparer : IEqualityComparer<DBMessage>
    {
        public bool Equals(DBMessage x, DBMessage y)
        {
            return ReferenceEquals(x, y) || x.MID.Equals(y.MID);
        }

        public int GetHashCode(DBMessage item)
        {
            return ReferenceEquals(item, null) ?
                0 :
                item.MID.GetHashCode();
        }
    }
}
