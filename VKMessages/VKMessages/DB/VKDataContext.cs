﻿using System.Data.Linq;
using VKMessages.DB.Entities;

namespace VKMessages.DB
{
    public class VKDataContext : DataContext
    {
        public VKDataContext(string connectionString)
            : base(connectionString)
        {
        }

        public Table<DBFriend> Friends
        {
            get { return this.GetTable<DBFriend>(); }
        }

        public Table<DBDialog> Dialogs
        {
            get { return this.GetTable<DBDialog>(); }
        }

        public Table<DBMessage> Messages
        {
            get { return this.GetTable<DBMessage>(); }
        }

        public Table<DBContact> Contacts
        {
            get { return this.GetTable<DBContact>(); }
        }
    }
}
