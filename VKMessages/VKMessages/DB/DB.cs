﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Linq;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Media.Imaging;
using Microsoft.Phone.UserData;
using VKMessages.API;
using VKMessages.API.JSON;
using VKMessages.DB.Comparers;
using VKMessages.DB.Entities;
using VKMessages.Tools;
using VKMessages.ViewModels;

namespace VKMessages.DB
{
    public static class DB
    {
        public const string ConnectionString = @"isostore:/vk.sdf";

        public static bool IsDbAvaliable = false;

        static DB()
        {
            using (var context = new VKDataContext(ConnectionString))
            {
                if (!context.DatabaseExists())
                {
                    context.CreateDatabase();
                }
            }
        }

        public static void UpdateFriends(JsonSimpleFriendsGetResponce responce, VKApi.VKApiOperationCompletedEventHandler friendsGetCompletedEventHandler)
        {
            var success = false;

            using (var context = new VKDataContext(ConnectionString))
            {
                try
                {
                    var dbFriends = context.Friends;
                    var jsonFriend = responce.Friends.Select(
                        x => new DBFriend
                        {
                            UID = x.UID,
                            FirstName = x.FirstName,
                            UserRate = x.Rate,
                            IsOnline = x.IsOnline,
                            Phone = x.Phone,
                            MobilePhone = x.MobilePhone,
                            HomePhone = x.HomePhone,
                            LastName = x.LastName,
                            Photo50X50Url = x.Photo50X50Sq.ToString(),
                            Photo100X100Url = x.Photo100X100Sq.ToString()
                        }).ToList();

                    var toInsertList = jsonFriend.Except(dbFriends, new FriendsDistinctComparer()).ToList();
                    var toUpdateList = jsonFriend.Except(toInsertList, new FriendsDistinctComparer());

                    foreach (var updateFriend in toUpdateList)
                    {
                        var srcFriend = dbFriends.FirstOrDefault(x => x.UID == updateFriend.UID);
                        if (srcFriend != null)
                        {
                            srcFriend.FirstName = updateFriend.FirstName;
                            srcFriend.LastName = updateFriend.LastName;
                            srcFriend.HomePhone = updateFriend.HomePhone;
                            srcFriend.IsOnline = updateFriend.IsOnline;
                            srcFriend.MobilePhone = updateFriend.MobilePhone;
                            srcFriend.Photo100X100Url = updateFriend.Photo100X100Url;
                            srcFriend.Photo50X50Url = updateFriend.Photo50X50Url;
                            srcFriend.UserRate = updateFriend.UserRate;
                        }
                    }

                    dbFriends.InsertAllOnSubmit(toInsertList);

                    context.SubmitChanges(ConflictMode.ContinueOnConflict);

                    success = true;
                }
                catch (Exception)
                {
                    success = false;
                }

                Deployment.Current.Dispatcher.BeginInvoke(() =>
                    friendsGetCompletedEventHandler.Invoke(null, new VKApiOperationCompletedEventArgs
                    {
                        IsSuccess = success,
                        ErrorCode = 0
                    }));

            }
        }

        public static void UpdateFriends(JsonAllFriendsGetResponce responce, VKApi.VKApiOperationCompletedEventHandler friendsGetCompletedEventHandler)
        {
            var success = false;

            using (var context = new VKDataContext(ConnectionString))
            {
                try
                {
                    var dbFriends = context.Friends;
                    var jsonFriend = responce.AllFriends.Friends.Select(
                        x => new DBFriend
                        {
                            UID = x.UID,
                            FirstName = x.FirstName,
                            UserRate = x.Rate,
                            IsOnline = x.IsOnline,
                            Phone = x.Phone,
                            MobilePhone = x.MobilePhone,
                            HomePhone = x.HomePhone,
                            LastName = x.LastName,
                            Photo50X50Url = x.Photo50X50Sq.ToString(),
                            Photo100X100Url = x.Photo100X100Sq.ToString()
                        }).ToList();

                    var toInsertList = jsonFriend.Except(dbFriends, new FriendsDistinctComparer()).ToList();
                    var toUpdateList = jsonFriend.Except(toInsertList, new FriendsDistinctComparer());

                    foreach (var updateFriend in toUpdateList)
                    {
                        var srcFriend = dbFriends.FirstOrDefault(x => x.UID == updateFriend.UID);
                        if (srcFriend != null)
                        {
                            srcFriend.FirstName = updateFriend.FirstName;
                            srcFriend.LastName = updateFriend.LastName;
                            srcFriend.HomePhone = updateFriend.HomePhone;
                            srcFriend.IsOnline = updateFriend.IsOnline;
                            srcFriend.MobilePhone = updateFriend.MobilePhone;
                            srcFriend.Photo100X100Url = updateFriend.Photo100X100Url;
                            srcFriend.Photo50X50Url = updateFriend.Photo50X50Url;
                            srcFriend.UserRate = updateFriend.UserRate;
                        }
                    }
                    
                    dbFriends.InsertAllOnSubmit(toInsertList);

                    context.SubmitChanges(ConflictMode.ContinueOnConflict);

                    success = true;
                }
                catch (Exception)
                {
                    success = false;
                }

                if (friendsGetCompletedEventHandler != null)
                    Deployment.Current.Dispatcher.BeginInvoke(() => 
                        friendsGetCompletedEventHandler.Invoke(null, new VKApiOperationCompletedEventArgs
                                                                 {
                                                                    IsSuccess = success,
                                                                    ErrorCode = 0
                                                                 }));
                
            }
        }

        private static bool IsPhonesEqual(string phone1, string phone2)
        {
            if (string.IsNullOrEmpty(phone1) || string.IsNullOrEmpty(phone2))
                return false;

            var c1 = phone1.Where(t => t >= '0' && t <= '9').Aggregate(string.Empty, (current, t) => current + t);
            var c2 = phone2.Where(t => t >= '0' && t <= '9').Aggregate(string.Empty, (current, t) => current + t);

            if (c1.Length == c2.Length)
                return c1 == c2;

            if (c1.Length < c2.Length)
            {
                if (c1.Length < 7)
                    return false;

                return c1 == c2.Substring(c2.Length - 7, 7);
            }

            if (c2.Length < 7)
                return false;

            return c2 == c1.Substring(c1.Length - 7, 7);
        }

        public static void UpdateContacts(JsonSimpleFriendsGetResponce responce, List<Contact> contacts, VKApi.VKApiOperationCompletedEventHandler contactsCompletedEventHandler)
        {
            var success = false;

            using (var context = new VKDataContext(ConnectionString))
            {
                try
                {
                    var dbContacts = context.Contacts;
                    JsonFriend friend = null;
                    var jsonContacts = new List<DBContact>();
                    foreach (var contact in contacts)
                    {
                        foreach (var contactPhone in contact.PhoneNumbers)
                        {
                            friend = responce.Friends.FirstOrDefault(x => IsPhonesEqual(x.MobilePhone, contactPhone.PhoneNumber));
                            if (friend == null)
                            {
                                friend = responce.Friends.FirstOrDefault(x => IsPhonesEqual(x.Phone, contactPhone.PhoneNumber));
                                if (friend == null)
                                {
                                    friend = responce.Friends.FirstOrDefault(x => IsPhonesEqual(x.HomePhone, contactPhone.PhoneNumber));
                                    if (friend != null)
                                    {
                                        break;
                                    }
                                }
                            }
                        }

                        var phones = contact.PhoneNumbers.Aggregate(string.Empty, 
                            (current, contactPhone) => current + "," + contactPhone.PhoneNumber);

                        var stream = contact.GetPicture();
                        byte[] image = null;
                        if (stream != null)
                        {
                            image = new byte[stream.Length];
                            stream.Read(image, 0, image.Length);
                        }

                        jsonContacts.Add(new DBContact
                                             {
                                                 DisplayName = contact.DisplayName,
                                                 VKName =
                                                     friend != null
                                                         ? friend.FirstName + " " + friend.LastName
                                                         : string.Empty,
                                                 UID = friend != null ? friend.UID : 0,
                                                 Phone = phones,
                                                 Image = image
                                             });
                    }

                    var toInsertList = jsonContacts.Except(dbContacts, new ContactsDistinctComparer()).ToList();
                    var toUpdateList = jsonContacts.Except(toInsertList, new ContactsDistinctComparer());

                    foreach (var updateContact in toUpdateList)
                    {
                        var srcFriend = dbContacts.FirstOrDefault(x => x.DisplayName == updateContact.DisplayName);
                        if (srcFriend != null)
                        {
                            srcFriend.Image = updateContact.Image;
                            srcFriend.Phone = updateContact.Phone;
                            srcFriend.UID = updateContact.UID;
                            srcFriend.VKName = updateContact.VKName;
                        }
                    }

                    dbContacts.InsertAllOnSubmit(toInsertList);

                    context.SubmitChanges(ConflictMode.ContinueOnConflict);

                    success = true;
                }
                catch (Exception ee)
                {
                    success = false;
                }

                Deployment.Current.Dispatcher.BeginInvoke(() =>
                    contactsCompletedEventHandler.Invoke(null, new VKApiOperationCompletedEventArgs
                    {
                        IsSuccess = success,
                        ErrorCode = 0
                    }));

            }
        }

        public static void UpdateDialogs(JsonDialogsGetResponce responce, VKApi.VKApiOperationCompletedEventHandler dialogsGetCompletedEventHandler)
        {
            var success = false;

            using (var context = new VKDataContext(ConnectionString))
            {
                try
                {
                    var dbDialogs = context.Dialogs;
                    var jsonDialog = new List<DBDialog>();
                    for (var i = 0; i < responce.Dialogs.Bodies.Count; i++)
                    {

                        var newDB = new DBDialog
                        {
                            Body = responce.Dialogs.Bodies[i].Replace("<br>", "\n"),
                            Title = responce.Dialogs.Titles[i],
                            ReadState = responce.Dialogs.ReadState[i],
                            Out = responce.Dialogs.Out[i],
                            ChatId = responce.Dialogs.ChatIds[i],
                            ChatActiveIds = responce.Dialogs.ActiveChatUIds[i],
                            Date = responce.Dialogs.Dates[i],
                            UID = responce.Dialogs.DialogsUIDs[i]
                        };

                        var j = responce.Dialogs.ProfilesUIDs.IndexOf(newDB.UID);

                        newDB.Image50X50Url = responce.Dialogs.Image50X50Urls[j];
                        newDB.FirstName = responce.Dialogs.FirstNames[j];
                        newDB.LastName = responce.Dialogs.LastNames[j];

                        if (newDB.ChatId > 0)
                            newDB.UID = newDB.ChatId + 2000000000;

                        jsonDialog.Add(newDB);
                    }

                    var toInsertList = jsonDialog.Except(dbDialogs, new DialogsDistinctComparer()).ToList();
                    var toUpdateList = jsonDialog.Except(toInsertList, new DialogsDistinctComparer());

                    foreach (var upadteDialog in toUpdateList)
                    {
                        var srcDialog = dbDialogs.FirstOrDefault(x => x.UID == upadteDialog.UID);
                        if (srcDialog != null)
                        {
                            srcDialog.Body = upadteDialog.Body;
                            srcDialog.Date = upadteDialog.Date;
                            srcDialog.FirstName = upadteDialog.FirstName;
                            srcDialog.LastName = upadteDialog.LastName;
                            srcDialog.ReadState = upadteDialog.ReadState;
                            srcDialog.Out = upadteDialog.Out;
                            srcDialog.Title = upadteDialog.Title;
                        }
                    }

                    dbDialogs.InsertAllOnSubmit(toInsertList);

                    context.SubmitChanges(ConflictMode.ContinueOnConflict);

                    success = true;
                }
                catch (Exception e)
                {
                    success = false;
                }

                if (dialogsGetCompletedEventHandler != null)
                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                        dialogsGetCompletedEventHandler.Invoke(null, new VKApiOperationCompletedEventArgs
                        {
                            IsSuccess = success,
                            ErrorCode = 0
                        }));
            }
        }

        public static void UpdateDialogs(JsonAllFriendsGetResponce responce, VKApi.VKApiOperationCompletedEventHandler dialogsGetCompletedEventHandler)
        {
            var success = false;

            using (var context = new VKDataContext(ConnectionString))
            {
                try
                {
                    var dbDialogs = context.Dialogs;
                    var jsonDialog = new List<DBDialog>();
                    for (var i = 0; i < responce.AllFriends.Bodies.Count; i++)
                    {

                        var newDB = new DBDialog
                                       {
                                           Body = responce.AllFriends.Bodies[i].Replace("<br>", "\n"),
                                           Title = responce.AllFriends.Titles[i],
                                           ReadState = responce.AllFriends.ReadState[i],
                                           Out = responce.AllFriends.Out[i],
                                           ChatId = responce.AllFriends.ChatIds[i],
                                           ChatActiveIds = responce.AllFriends.ActiveChatUIds[i],
                                           Date = responce.AllFriends.Dates[i],
                                           UID = responce.AllFriends.DialogsUIDs[i]
                                       };

                        var j = responce.AllFriends.ProfilesUIDs.IndexOf(newDB.UID);

                        newDB.Image50X50Url = responce.AllFriends.Image50X50Urls[j];
                        newDB.FirstName = responce.AllFriends.FirstNames[j];
                        newDB.LastName = responce.AllFriends.LastNames[j];

                        if (newDB.ChatId > 0)
                            newDB.UID = newDB.ChatId + 2000000000;

                        jsonDialog.Add(newDB);
                    }

                    var toInsertList = jsonDialog.Except(dbDialogs, new DialogsDistinctComparer()).ToList();
                    var toUpdateList = jsonDialog.Except(toInsertList, new DialogsDistinctComparer());

                    foreach (var upadteDialog in toUpdateList)
                    {
                        var srcDialog = dbDialogs.FirstOrDefault(x => x.UID == upadteDialog.UID);
                        if (srcDialog != null)
                        {
                            srcDialog.Body = upadteDialog.Body;
                            srcDialog.Date = upadteDialog.Date;
                            srcDialog.FirstName = upadteDialog.FirstName;
                            srcDialog.LastName = upadteDialog.LastName;
                            srcDialog.ReadState = upadteDialog.ReadState;
                            srcDialog.Out = upadteDialog.Out;
                            srcDialog.Title = upadteDialog.Title;
                        }
                    }

                    dbDialogs.InsertAllOnSubmit(toInsertList);

                    context.SubmitChanges(ConflictMode.ContinueOnConflict);

                    success = true;
                }
                catch (Exception e)
                {
                    success = false;
                }

                if (dialogsGetCompletedEventHandler != null)
                    Deployment.Current.Dispatcher.BeginInvoke(() => 
                        dialogsGetCompletedEventHandler.Invoke(null, new VKApiOperationCompletedEventArgs
                        {
                            IsSuccess = success,
                            ErrorCode = 0
                        }));
            }
        }

        public static void UpdateFriendImage(long uid, byte[] image)
        {
            var success = false;

            using (var context = new VKDataContext(ConnectionString))
            {
                try
                {
                    var dbFriends = (from x in context.Friends where x.UID == uid select x);

                    var uDlg = dbFriends.FirstOrDefault();

                    if (uDlg == null)
                    {
                        success = false;
                    }
                    else
                    {
                        if (image != null && image.Length > 0)
                            uDlg.Image50X50 = image;

                        context.SubmitChanges(ConflictMode.ContinueOnConflict);

                        success = true;
                    }
                }
                catch (Exception ee)
                {
                    success = false;
                }
            }
        }

        public static void UpdateDialog(long uid, List<long> uids, string title, byte[] image)
        {
            var success = false;

            using (var context = new VKDataContext(ConnectionString))
            {
                try
                {
                    var dbDialogs = (from x in context.Dialogs where x.UID == uid select x);

                    var uDlg = dbDialogs.FirstOrDefault();

                    if (uDlg == null)
                    {
                        success = false;
                    }
                    else
                    {
                        if (uids != null && uids.Count > 0)
                        {
                            uDlg.ChatActiveIds = uids.Aggregate(string.Empty, (list, uid1) => list + "," + uid1);
                            if (!string.IsNullOrEmpty(uDlg.ChatActiveIds) && uDlg.ChatActiveIds[0] == ',' &&
                                uDlg.ChatActiveIds.Length > 1)
                                uDlg.ChatActiveIds = uDlg.ChatActiveIds.Substring(1, uDlg.ChatActiveIds.Length - 1);
                        }

                        if (!string.IsNullOrEmpty(title))
                            uDlg.Title = title;

                        if (image != null && image.Length > 0)
                            uDlg.Image50X50 = image;

                        context.SubmitChanges(ConflictMode.ContinueOnConflict);

                        success = true;
                    }
                }
                catch (Exception ee)
                {
                    success = false;
                }
            }
        }

        public static void UpdateMessage(JsonMessage msg, VKApi.VKApiOperationCompletedEventHandler friendsGetCompletedEventHandler)
        {
            var success = false;

            using (var context = new VKDataContext(ConnectionString))
            {
                try
                {
                    var dbMessages = (from x in context.Messages where x.MID == msg.MID select x);

                    var uMsg = dbMessages.FirstOrDefault();

                    if (uMsg == null)
                    {
                        success = false;
                    }
                    else
                    {
                        uMsg.Body = msg.Body;
                        uMsg.Latitude = msg.Latitude;
                        uMsg.Longitude = msg.Longitude;
                        uMsg.ReadState = msg.ReadState;
                        uMsg.PhotoUrls = msg.Attachments == null || msg.Attachments.Count < 1
                                             ? string.Empty
                                             : msg.Attachments.Where(
                                                 att =>
                                                 att.Photo != null && !string.IsNullOrEmpty(att.Photo.Url) &&
                                                 !string.IsNullOrEmpty(att.Photo.UrlBig)).
                                                   Aggregate(string.Empty,
                                                             (current, t) =>
                                                             current + t.Photo.Url + " " + t.Photo.UrlBig + " ").
                                                   TrimEnd();
                        uMsg.VideoUrls = msg.Attachments == null || msg.Attachments.Count < 1
                                             ? string.Empty
                                             : msg.Attachments.Where(
                                                 att =>
                                                 att.Video != null && !string.IsNullOrEmpty(att.Video.Url) &&
                                                 !string.IsNullOrEmpty(att.Video.UrlBig)).
                                                   Aggregate(string.Empty,
                                                             (current, t) =>
                                                             current + t.Video.Url + " " + t.Video.UrlBig + " ").
                                                   TrimEnd();
                        uMsg.VideoDurations = msg.Attachments == null || msg.Attachments.Count < 1
                                             ? string.Empty
                                             : msg.Attachments.Where(
                                                 att =>
                                                 att.Video != null).
                                                   Aggregate(string.Empty,
                                                             (current, t) =>
                                                             current + t.Video.Duration + " ").
                                                   TrimEnd();

                        uMsg.AudioUrls = msg.Attachments == null || msg.Attachments.Count < 1
                                             ? string.Empty
                                             : msg.Attachments.Where(
                                                 att =>
                                                 att.Audio != null && !string.IsNullOrEmpty(att.Audio.Url)).
                                                   Aggregate(string.Empty,
                                                             (current, t) =>
                                                             current + t.Audio.Url + " ").
                                                   TrimEnd();
                        uMsg.DocUrls = msg.Attachments == null || msg.Attachments.Count < 1
                                             ? string.Empty
                                             : msg.Attachments.Where(
                                                 att =>
                                                 att.Document != null && !string.IsNullOrEmpty(att.Document.Url)).
                                                   Aggregate(string.Empty,
                                                             (current, t) =>
                                                             current + t.Document.Url + " ").
                                                   TrimEnd();
                        uMsg.AudioDurations = msg.Attachments == null || msg.Attachments.Count < 1
                                             ? string.Empty
                                             : msg.Attachments.Where(
                                                 att =>
                                                 att.Audio != null).
                                                   Aggregate(string.Empty,
                                                             (current, t) =>
                                                             current + t.Audio.Duration + " ").
                                                   TrimEnd();
                        uMsg.AudioTitles = msg.Attachments == null || msg.Attachments.Count < 1
                                             ? string.Empty
                                             : msg.Attachments.Where(
                                                 att =>
                                                 att.Audio != null).
                                                   Aggregate(string.Empty,
                                                             (current, t) =>
                                                             current + t.Audio.Title + "%%%").
                                                   TrimEnd();
                        uMsg.DocTitles = msg.Attachments == null || msg.Attachments.Count < 1
                                             ? string.Empty
                                             : msg.Attachments.Where(
                                                 att =>
                                                 att.Document != null).
                                                   Aggregate(string.Empty,
                                                             (current, t) =>
                                                             current + t.Document.Title + "%%%").
                                                   TrimEnd();
                        uMsg.AudioArtists = msg.Attachments == null || msg.Attachments.Count < 1
                                             ? string.Empty
                                             : msg.Attachments.Where(
                                                 att =>
                                                 att.Audio != null).
                                                   Aggregate(string.Empty,
                                                             (current, t) =>
                                                             current + t.Audio.Performer + "%%%").
                                                   TrimEnd();

                        context.SubmitChanges(ConflictMode.ContinueOnConflict);

                        success = true;
                    }
                }
                catch (Exception ee)
                {
                    success = false;
                }

                if (friendsGetCompletedEventHandler != null)
                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                        friendsGetCompletedEventHandler.Invoke(null, new VKApiOperationCompletedEventArgs
                        {
                            IsSuccess = success,
                            ErrorCode = 0
                        }));

            }
        }

        public static void UpdateMessages(long uid, JsonMessagesGetResponce responce, VKApi.VKApiOperationCompletedEventHandler friendsGetCompletedEventHandler)
        {
            var success = false;

            using (var context = new VKDataContext(ConnectionString))
            {
                try
                {
                    var dbFriends = context.Messages;
                    var jsonFriend = responce.Messages.Select(
                        x => new DBMessage
                        {
                            MID = x.MID,
                            Body = x.Body.Replace("<br>", "\n"),
                            Date = x.Date,
                            ChatId = uid > 2000000000 ? uid - 2000000000 : 0,
                            FromId = x.FromId,
                            ToId = uid > 2000000000 ? uid : (x.Out != 0 ? uid : 0),
                            Out = x.Out,
                            ReadState = x.ReadState,
                            Latitude = x.Latitude,
                            Longitude = x.Longitude,
                            PhotoUrls = x.Attachments == null || x.Attachments.Count < 1 ? string.Empty :
                                        x.Attachments.Where(att => att.Photo != null && !string.IsNullOrEmpty(att.Photo.Url) && !string.IsNullOrEmpty(att.Photo.UrlBig)).
                                                      Aggregate(string.Empty, (current, t) => current + t.Photo.Url + " " + t.Photo.UrlBig + " ").TrimEnd(),
                            VideoUrls = x.Attachments == null || x.Attachments.Count < 1 ? string.Empty :
                                        x.Attachments.Where(att => att.Video != null && !string.IsNullOrEmpty(att.Video.Url) && !string.IsNullOrEmpty(att.Video.UrlBig)).
                                                      Aggregate(string.Empty, (current, t) => current + t.Video.Url + " " + t.Video.UrlBig + " ").TrimEnd(),
                            VideoDurations = x.Attachments == null || x.Attachments.Count < 1 ? string.Empty :
                                             x.Attachments.Where(att => att.Video != null).
                                                      Aggregate(string.Empty, (current, t) => current + t.Video.Duration + " ").TrimEnd(),
                            AudioUrls = x.Attachments == null || x.Attachments.Count < 1 ? string.Empty :
                                        x.Attachments.Where(att => att.Audio != null && !string.IsNullOrEmpty(att.Audio.Url)).
                                                      Aggregate(string.Empty, (current, t) => current + t.Audio.Url + " ").TrimEnd(),
                            AudioDurations = x.Attachments == null || x.Attachments.Count < 1 ? string.Empty :
                                             x.Attachments.Where(att => att.Audio != null).
                                                      Aggregate(string.Empty, (current, t) => current + t.Audio.Duration + " ").TrimEnd(),
                            AudioArtists = x.Attachments == null || x.Attachments.Count < 1 ? string.Empty :
                                             x.Attachments.Where(att => att.Audio != null).
                                                      Aggregate(string.Empty, (current, t) => current + t.Audio.Performer + "%%%").TrimEnd(),
                            AudioTitles = x.Attachments == null || x.Attachments.Count < 1 ? string.Empty :
                                          x.Attachments.Where(att => att.Audio != null).
                                                      Aggregate(string.Empty, (current, t) => current + t.Audio.Title + "%%%").TrimEnd(),
                            DocUrls = x.Attachments == null || x.Attachments.Count < 1 ? string.Empty :
                                        x.Attachments.Where(att => att.Document != null && !string.IsNullOrEmpty(att.Document.Url)).
                                                      Aggregate(string.Empty, (current, t) => current + t.Document.Url + " ").TrimEnd(),
                            DocTitles = x.Attachments == null || x.Attachments.Count < 1 ? string.Empty :
                                        x.Attachments.Where(att => att.Document != null).
                                                    Aggregate(string.Empty, (current, t) => current + t.Document    .Title + "%%%").TrimEnd()
                                                      
                        }).ToList();

                    var toInsertList = jsonFriend.Except(dbFriends, new MessagesDistinctComparer());
                    var toUpdateList = dbFriends.Intersect(jsonFriend, new MessagesDistinctComparer());

                    dbFriends.InsertAllOnSubmit(toInsertList);

                    context.SubmitChanges(ConflictMode.ContinueOnConflict);

                    success = true;
                }
                catch (Exception ee)
                {
                    success = false;
                }

                Deployment.Current.Dispatcher.BeginInvoke(() =>
                    friendsGetCompletedEventHandler.Invoke(null, new VKApiOperationCompletedEventArgs
                    {
                        IsSuccess = success,
                        ErrorCode = 0
                    }));

            }
        }

        public static void InsertMessage(DBMessage msg, JsonMessageSendResponce responce)
        {
            var success = false;

            using (var context = new VKDataContext(ConnectionString))
            {
                try
                {
                    var dbMessages = context.Messages;

                    dbMessages.InsertOnSubmit(msg);

                    context.SubmitChanges(ConflictMode.ContinueOnConflict);

                    success = true;
                }
                catch (Exception ee)
                {
                    success = false;
                }
            }
        }

        public static void InsertDialog(DBDialog msg)
        {
            var success = false;

            using (var context = new VKDataContext(ConnectionString))
            {
                try
                {
                    var dbMessages = context.Dialogs;

                    dbMessages.InsertOnSubmit(msg);

                    context.SubmitChanges(ConflictMode.ContinueOnConflict);

                    success = true;
                }
                catch (Exception ee)
                {
                    success = false;
                }
            }
        }

        public static ObservableCollection<FriendCategory> GetFriendsList()
        {
            using (var context = new VKDataContext(ConnectionString))
            {
                var result = new ObservableCollection<FriendCategory>();
                var dbFriends = context.Friends.ToList();

                var query = dbFriends.GroupBy(friend => 
                    CrossPageParameters.FirstFiveUids.Contains(friend.UID) ? "" : friend.FirstName[0].ToString().ToLower(), 
                    friend => friend).OrderBy(y => y.Key);

                foreach (var contactGroup in query)
                {
                    result.Add(new FriendCategory
                    (
                        new ObservableCollection<FriendItemViewModel>(
                            contactGroup.
                                Select(x => new FriendItemViewModel
                                                {
                                                    VKName = x.FirstName + " " + x.LastName,
                                                    IsOnline = x.IsOnline,
                                                    UID = x.UID,
                                                    Phone = x.Phone,
                                                    MobilePhone = x.MobilePhone,
                                                    HomePhone = x.HomePhone,
                                                    Rate = x.UserRate,
                                                    Image50X50Url = x.Photo50X50Url,
                                                    Image100X100Url = x.Photo100X100Url,
                                                    Image50X50Data = x.Image50X50
                                                }))
                    ) {Key = contactGroup.Key});
                }

                if (result.Count > 0)
                    result[0] = new FriendCategory(result[0].OrderByDescending(r => r.Rate));

                return result;
            }
        }

        public static WriteableBitmap ByteArrayToImage(byte[] byteArrayIn)
        {
            using (var ms = new MemoryStream(byteArrayIn))
            {
                return Microsoft.Phone.PictureDecoder.DecodeJpeg(ms);
            }
        }

        public static ObservableCollection<ContactCategory> GetContactsList()
        {
            using (var context = new VKDataContext(ConnectionString))
            {
                var result = new ObservableCollection<ContactCategory>();
                var dbFriends = context.Contacts.ToList();

                var query = dbFriends.GroupBy(contact => contact.DisplayName[0].ToString().ToLower(), friend => friend);

                var defaultImage = Microsoft.Phone.PictureDecoder.DecodeJpeg(
                    Application.GetResourceStream(new Uri("Images/Components/Photo_Default.jpg", UriKind.Relative)).Stream);

                foreach (var contactGroup in query)
                {
                    result.Add(new ContactCategory
                    (
                        new ObservableCollection<ContactItemViewModel>(
                            contactGroup.
                                Select(x => new ContactItemViewModel
                                {
                                    VKName = x.VKName,
                                    UID = x.UID,
                                    ContactName = x.DisplayName,
                                    Phones = new List<string>(x.Phone.Split(new [] {','}, StringSplitOptions.RemoveEmptyEntries)),
                                    Image = x.Image != null ? ByteArrayToImage(x.Image) : defaultImage
                                }))
                    ) { Key = contactGroup.Key });
                }

                return result;
            }
        }

        public static ObservableCollection<DialogItemViewModel> GetDialogsList()
        {
            using (var context = new VKDataContext(ConnectionString))
            {
                var list = new ObservableCollection<DialogItemViewModel>();
                var dbDialogs = context.Dialogs.ToList();

                foreach (var dialog in dbDialogs)
                {
                    list.Add(new DialogItemViewModel
                                {                                   
                                    IsUnread = dialog.ReadState == 0 && dialog.Out == 0,
                                    Body = dialog.Body,
                                    Title = dialog.Title,
                                    Image50X50Url = dialog.Image50X50Url,
                                    Image50X50Data = dialog.Image50X50,
                                    Date = (new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc) +
                                            TimeSpan.FromSeconds(dialog.Date)).ToLocalTime(),
                                    FirstName = dialog.FirstName,
                                    LastName = dialog.LastName,
                                    IsOnline = dialog.IsOnline != 0,
                                    UID = dialog.UID,
                                    ChatId = dialog.ChatId,
                                    ActiveChatIds = !string.IsNullOrEmpty(dialog.ChatActiveIds) ?
                                                            dialog.ChatActiveIds.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries).
                                                            Select(x => !string.IsNullOrEmpty(x) ? Convert.ToInt64(x) : 0).
                                                            ToList()
                                                            :
                                                            new List<long>()
                                });
                }

                return list;
            }
        }

        public static ObservableCollection<MessageItemViewModel> GetMessagesList(long uid, bool isChat)
        {
            using (var context = new VKDataContext(ConnectionString))
            {
                var list = new ObservableCollection<MessageItemViewModel>();
                var dbMessages = (from x in context.Messages where isChat ? x.ChatId == uid : (x.ChatId < 1 && (x.FromId == uid || x.ToId == uid)) select x).ToList();

                for (var i = dbMessages.Count - 1; i >= 0; i--)
                {
                    var newMI = new MessageItemViewModel
                                    {
                                        MID = dbMessages[i].MID,
                                        Body = dbMessages[i].Body,
                                        Date = (new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc) +
                                                TimeSpan.FromSeconds(dbMessages[i].Date)).ToLocalTime(),
                                        ReadState = dbMessages[i].ReadState != 0,
                                        FromId = dbMessages[i].FromId,
                                        Out = dbMessages[i].Out != 0,
                                        Latitude = dbMessages[i].Latitude,
                                        Longitude = dbMessages[i].Longitude
                                    };

                    newMI.Attachments = new ObservableCollection<Attachment>();

                    if (!string.IsNullOrEmpty(dbMessages[i].PhotoUrls))
                    {
                        var photoArr = dbMessages[i].PhotoUrls.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);                        
                        if (photoArr.Length%2 == 0)
                        {
                            for (var j = 0; j < photoArr.Length; j += 2)
                                newMI.Attachments.Add(new Attachment
                                                          {
                                                              Type = AttachmentType.Photo,
                                                              Url = photoArr[j],
                                                              UrlBig = photoArr[j + 1]
                                                          });
                        }
                    }

                    if (!string.IsNullOrEmpty(dbMessages[i].VideoUrls))
                    {
                        var videoArr = dbMessages[i].VideoUrls.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        var dArr = dbMessages[i].VideoDurations.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        if (videoArr.Length % 2 == 0)
                        {
                            for (int j = 0, k = 0; j < videoArr.Length; j += 2, k++)
                                newMI.Attachments.Add(new Attachment
                                {
                                    Type = AttachmentType.Video,
                                    Url = videoArr[j],
                                    UrlBig = videoArr[j + 1],
                                    VideoDuration = Convert.ToInt64(dArr[k])
                                });
                        }
                    }

                    if (!string.IsNullOrEmpty(dbMessages[i].AudioUrls))
                    {
                        var audioArr = dbMessages[i].AudioUrls.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        var dArr = dbMessages[i].AudioDurations.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        var arArr = dbMessages[i].AudioArtists.Split(new[] { "%%%" }, StringSplitOptions.RemoveEmptyEntries);
                        var tArr = dbMessages[i].AudioTitles.Split(new[] { "%%%" }, StringSplitOptions.RemoveEmptyEntries);
                        for (var k = 0; k < audioArr.Length; k++)
                            newMI.Attachments.Add(new Attachment
                            {
                                Type = AttachmentType.Audio,
                                Url = audioArr[k],
                                AudioDuration = Convert.ToInt64(dArr[k]),
                                AudioArtist = arArr[k],
                                AudioTitle = tArr[k]
                            });
                    }

                    if (!string.IsNullOrEmpty(dbMessages[i].DocUrls))
                    {
                        var docUrls = dbMessages[i].DocUrls.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        var docTitles = dbMessages[i].DocTitles.Split(new[] { "%%%" }, StringSplitOptions.RemoveEmptyEntries);
                        for (var k = 0; k < docUrls.Length; k++)
                            newMI.Attachments.Add(new Attachment
                            {
                                Type = AttachmentType.Document,
                                Url = docUrls[k],
                                DocTitle = docTitles[k]
                            });
                    }

                    list.Add(newMI);
                }

                return list;
            }
        }
    }
}
