﻿using System.Data.Linq.Mapping;
using System.Windows.Media.Imaging;

namespace VKMessages.DB.Entities
{
    [Table(Name = "Friends")]
    public class DBFriend : DBEntityBase
    {
        private long uid;
        [Column(IsPrimaryKey = true, IsDbGenerated = false, DbType = "BIGINT", 
                CanBeNull = false, AutoSync = AutoSync.OnInsert)]         
        public long UID
        {
            get { return uid; }
            set
            {
                if (uid != value)
                {
                    NotifyPropertyChanging("UID");
                    uid = value;
                    NotifyPropertyChanged("UID");
                }
            }
        }

        private long userRate;
        [Column(DbType = "BIGINT")]
        public long UserRate
        {
            get { return userRate; }
            set
            {
                if (userRate != value)
                {
                    NotifyPropertyChanging("UserRate");
                    userRate = value;
                    NotifyPropertyChanged("UserRate");
                }
            }
        }

        private string firstName;
        [Column(DbType = "NVARCHAR(100)")]
        public string FirstName
        {
            get { return firstName; }
            set
            {
                if (firstName != value)
                {
                    NotifyPropertyChanging("FirstName");
                    firstName = value;
                    NotifyPropertyChanged("FirstName");
                }
            }
        }

        private string lastName;
        [Column(DbType = "NVARCHAR(100)")]
        public string LastName
        {
            get { return lastName; }
            set
            {
                if (lastName != value)
                {
                    NotifyPropertyChanging("LastName");
                    lastName = value;
                    NotifyPropertyChanged("LastName");
                }
            }
        }

        private string phone;
        [Column(DbType = "NVARCHAR(100)")]
        public string Phone
        {
            get { return phone; }
            set
            {
                if (phone != value)
                {
                    NotifyPropertyChanging("Phone");
                    phone = value;
                    NotifyPropertyChanged("Phone");
                }
            }
        }

        private string mobilePhone;
        [Column(DbType = "NVARCHAR(100)")]
        public string MobilePhone
        {
            get { return mobilePhone; }
            set
            {
                if (mobilePhone != value)
                {
                    NotifyPropertyChanging("MobilePhone");
                    mobilePhone = value;
                    NotifyPropertyChanged("MobilePhone");
                }
            }
        }

        private string homePhone;
        [Column(DbType = "NVARCHAR(100)")]
        public string HomePhone
        {
            get { return homePhone; }
            set
            {
                if (homePhone != value)
                {
                    NotifyPropertyChanging("HomePhone");
                    homePhone = value;
                    NotifyPropertyChanged("HomePhone");
                }
            }
        }

        private bool isOnline;
        [Column(DbType = "INT", CanBeNull = false)]
        public bool IsOnline
        {
            get { return isOnline; }
            set
            {
                if (isOnline != value)
                {
                    NotifyPropertyChanging("IsOnline");
                    isOnline = value;
                    NotifyPropertyChanged("IsOnline");
                }
            }
        }

        private string photo50X50Url;
        [Column(DbType = "NVARCHAR(100)")]
        public string Photo50X50Url
        {
            get { return photo50X50Url; }
            set
            {
                if (photo50X50Url != value)
                {
                    NotifyPropertyChanging("Photo50X50Url");
                    photo50X50Url = value;
                    NotifyPropertyChanged("Photo50X50Url");
                }
            }
        }

        private byte[] image50X50;
        [Column(DbType = "IMAGE", UpdateCheck = UpdateCheck.Never)]
        public byte[] Image50X50
        {
            get { return image50X50; }
            set
            {
                if (image50X50 != value)
                {
                    NotifyPropertyChanging("Image50X50");
                    image50X50 = value;
                    NotifyPropertyChanged("Image50X50");
                }
            }
        }

        public BitmapImage SmallPhoto
        {
            get { return ByteArrayToImage(image50X50); }
        }

        private string photo100X100Url;
        [Column(DbType = "NVARCHAR(100)")]
        public string Photo100X100Url
        {
            get { return photo100X100Url; }
            set
            {
                if (photo100X100Url != value)
                {
                    NotifyPropertyChanging("Photo100X100Url");
                    photo100X100Url = value;
                    NotifyPropertyChanged("Photo100X100Url");
                }
            }
        }

        private byte[] photo100X100;
        [Column(DbType = "VARBINARY(1024)")]
        public byte[] Photo100X100
        {
            get { return photo100X100; }
            set
            {
                if (photo100X100 != value)
                {
                    NotifyPropertyChanging("Photo100X100");
                    NotifyPropertyChanging("MediumPhoto");
                    photo100X100 = value;
                    NotifyPropertyChanged("Photo100X100");
                    NotifyPropertyChanged("MediumPhoto");
                }
            }
        }

        public BitmapImage MediumPhoto
        {
            get { return ByteArrayToImage(photo100X100); }
        }
    }
}
