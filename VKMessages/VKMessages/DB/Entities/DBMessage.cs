﻿using System.Data.Linq.Mapping;

namespace VKMessages.DB.Entities
{
    [Table(Name = "Messages")]
    public class DBMessage : DBEntityBase
    {
        private long mid;
        [Column(IsPrimaryKey = true, IsDbGenerated = false, DbType = "BIGINT",
                CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public long MID
        {
            get { return mid; }
            set
            {
                if (mid != value)
                {
                    NotifyPropertyChanging("MID");
                    mid = value;
                    NotifyPropertyChanged("MID");
                }
            }
        }

        private long fromId;
        [Column(DbType = "BIGINT")]
        public long FromId
        {
            get { return fromId; }
            set
            {
                if (fromId != value)
                {
                    NotifyPropertyChanging("FromId");
                    fromId = value;
                    NotifyPropertyChanged("FromId");
                }
            }
        }

        private long chatId;
        [Column(DbType = "BIGINT")]
        public long ChatId
        {
            get { return chatId; }
            set
            {
                if (chatId != value)
                {
                    NotifyPropertyChanging("ChatId");
                    chatId = value;
                    NotifyPropertyChanged("ChatId");
                }
            }
        }

        private long toId;
        [Column(DbType = "BIGINT")]
        public long ToId
        {
            get { return toId; }
            set
            {
                if (toId != value)
                {
                    NotifyPropertyChanging("ToId");
                    toId = value;
                    NotifyPropertyChanged("ToId");
                }
            }
        }

        private long date;
        [Column(DbType = "BIGINT")]
        public long Date
        {
            get { return date; }
            set
            {
                if (date != value)
                {
                    NotifyPropertyChanging("Date");
                    date = value;
                    NotifyPropertyChanged("Date");
                }
            }
        }

        private string body;
        [Column(DbType = "NVARCHAR(1024)")]
        public string Body
        {
            get { return body; }
            set
            {
                if (body != value)
                {
                    NotifyPropertyChanging("Body");
                    body = value;
                    NotifyPropertyChanged("Body");
                }
            }
        }

        private int readState;
        [Column(DbType = "INT")]
        public int ReadState
        {
            get { return readState; }
            set
            {
                if (readState != value)
                {
                    NotifyPropertyChanging("ReadState");
                    readState = value;
                    NotifyPropertyChanged("ReadState");
                }
            }
        }

        private int _out;
        [Column(DbType = "INT")]
        public int Out
        {
            get { return _out; }
            set
            {
                if (_out != value)
                {
                    NotifyPropertyChanging("Out");
                    _out = value;
                    NotifyPropertyChanged("Out");
                }
            }
        }

        private string latitude;
        [Column(DbType = "NVARCHAR(20)")]
        public string Latitude
        {
            get { return latitude; }
            set
            {
                if (latitude != value)
                {
                    NotifyPropertyChanging("Latitude");
                    latitude = value;
                    NotifyPropertyChanged("Latitude");
                }
            }
        }

        private string longitude;
        [Column(DbType = "NVARCHAR(20)")]
        public string Longitude
        {
            get { return longitude; }
            set
            {
                if (longitude != value)
                {
                    NotifyPropertyChanging("Longitude");
                    longitude = value;
                    NotifyPropertyChanged("Longitude");
                }
            }
        }

        private string photoUrls;
        [Column(DbType = "NVARCHAR(1024)")]
        public string PhotoUrls
        {
            get { return photoUrls; }
            set
            {
                if (photoUrls != value)
                {
                    NotifyPropertyChanging("PhotoUrls");
                    photoUrls = value;
                    NotifyPropertyChanged("PhotoUrls");
                }
            }
        }

        private string videoUrls;
        [Column(DbType = "NVARCHAR(1024)")]
        public string VideoUrls
        {
            get { return videoUrls; }
            set
            {
                if (videoUrls != value)
                {
                    NotifyPropertyChanging("VideoUrls");
                    videoUrls = value;
                    NotifyPropertyChanged("VideoUrls");
                }
            }
        }

        private string audioUrls;
        [Column(DbType = "NVARCHAR(1024)")]
        public string AudioUrls
        {
            get { return audioUrls; }
            set
            {
                if (audioUrls != value)
                {
                    NotifyPropertyChanging("AudioUrls");
                    audioUrls = value;
                    NotifyPropertyChanged("AudioUrls");
                }
            }
        }

        private string docUrls;
        [Column(DbType = "NVARCHAR(1024)")]
        public string DocUrls
        {
            get { return docUrls; }
            set
            {
                if (docUrls != value)
                {
                    NotifyPropertyChanging("DocUrls");
                    docUrls = value;
                    NotifyPropertyChanged("DocUrls");
                }
            }
        }

        private string videoDurations;
        [Column(DbType = "NVARCHAR(256)")]
        public string VideoDurations
        {
            get { return videoDurations; }
            set
            {
                if (videoDurations != value)
                {
                    NotifyPropertyChanging("VideoDurations");
                    videoDurations = value;
                    NotifyPropertyChanged("VideoDurations");
                }
            }
        }

        private string audioDurations;
        [Column(DbType = "NVARCHAR(256)")]
        public string AudioDurations
        {
            get { return audioDurations; }
            set
            {
                if (audioDurations != value)
                {
                    NotifyPropertyChanging("AudioDurations");
                    audioDurations = value;
                    NotifyPropertyChanged("AudioDurations");
                }
            }
        }

        private string audioArtists;
        [Column(DbType = "NVARCHAR(1024)")]
        public string AudioArtists
        {
            get { return audioArtists; }
            set
            {
                if (audioArtists != value)
                {
                    NotifyPropertyChanging("AudioArtists");
                    audioArtists = value;
                    NotifyPropertyChanged("AudioArtists");
                }
            }
        }

        private string audioTitles;
        [Column(DbType = "NVARCHAR(1024)")]
        public string AudioTitles
        {
            get { return audioTitles; }
            set
            {
                if (audioTitles != value)
                {
                    NotifyPropertyChanging("AudioTitles");
                    audioTitles = value;
                    NotifyPropertyChanged("AudioTitles");
                }
            }
        }

        private string docTitles;
        [Column(DbType = "NVARCHAR(1024)")]
        public string DocTitles
        {
            get { return docTitles; }
            set
            {
                if (docTitles != value)
                {
                    NotifyPropertyChanging("DocTitles");
                    docTitles = value;
                    NotifyPropertyChanged("DocTitles");
                }
            }
        }
    }
}
