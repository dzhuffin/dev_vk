﻿using System.Data.Linq.Mapping;

namespace VKMessages.DB.Entities
{
    [Table(Name = "Dialogs")]
    public class DBDialog : DBEntityBase
    {
        private long uid;

        [Column(IsPrimaryKey = true, IsDbGenerated = false, DbType = "BIGINT",
            CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public long UID
        {
            get { return uid; }
            set
            {
                if (uid != value)
                {
                    NotifyPropertyChanging("UID");
                    uid = value;
                    NotifyPropertyChanged("UID");
                }
            }
        }

        private long date;

        [Column(DbType = "BIGINT")]
        public long Date
        {
            get { return date; }
            set
            {
                if (date != value)
                {
                    NotifyPropertyChanging("Date");
                    date = value;
                    NotifyPropertyChanged("Date");
                }
            }
        }

        private int readState;

        [Column(DbType = "INT")]
        public int ReadState
        {
            get { return readState; }
            set
            {
                if (readState != value)
                {
                    NotifyPropertyChanging("ReadState");
                    readState = value;
                    NotifyPropertyChanged("ReadState");
                }
            }
        }

        private int _out;

        [Column(DbType = "INT")]
        public int Out
        {
            get { return _out; }
            set
            {
                if (_out != value)
                {
                    NotifyPropertyChanging("Out");
                    _out = value;
                    NotifyPropertyChanged("Out");
                }
            }
        }

        private int isOnline;

        [Column(DbType = "INT")]
        public int IsOnline
        {
            get { return isOnline; }
            set
            {
                if (isOnline != value)
                {
                    NotifyPropertyChanging("IsOnline");
                    isOnline = value;
                    NotifyPropertyChanged("IsOnline");
                }
            }
        }

        private string body;

        [Column(DbType = "NVARCHAR(100)")]
        public string Body
        {
            get { return body; }
            set
            {
                if (body != value)
                {
                    NotifyPropertyChanging("Body");
                    body = value;
                    NotifyPropertyChanged("Body");
                }
            }
        }

        private string title;

        [Column(DbType = "NVARCHAR(100)")]
        public string Title
        {
            get { return title; }
            set
            {
                if (title != value)
                {
                    NotifyPropertyChanging("Title");
                    title = value;
                    NotifyPropertyChanged("Title");
                }
            }
        }

        private string image50X50Url;

        [Column(DbType = "NVARCHAR(100)")]
        public string Image50X50Url
        {
            get { return image50X50Url; }
            set
            {
                if (image50X50Url != value)
                {
                    NotifyPropertyChanging("Image50X50Url");
                    image50X50Url = value;
                    NotifyPropertyChanged("Image50X50Url");
                }
            }
        }

        private byte[] image50X50;
        [Column(DbType = "IMAGE", UpdateCheck = UpdateCheck.Never)]
        public byte[] Image50X50
        {
            get { return image50X50; }
            set
            {
                if (image50X50 != value)
                {
                    NotifyPropertyChanging("Image50X50");
                    image50X50 = value;
                    NotifyPropertyChanged("Image50X50");
                }
            }
        }

        private string firstName;

        [Column(DbType = "NVARCHAR(100)")]
        public string FirstName
        {
            get { return firstName; }
            set
            {
                if (firstName != value)
                {
                    NotifyPropertyChanging("FirstName");
                    firstName = value;
                    NotifyPropertyChanged("FirstName");
                }
            }
        }


        private string lastName;

        [Column(DbType = "NVARCHAR(100)")]
        public string LastName
        {
            get { return lastName; }
            set
            {
                if (lastName != value)
                {
                    NotifyPropertyChanging("LastName");
                    lastName = value;
                    NotifyPropertyChanged("LastName");
                }
            }
        }

        private long chatId;

        [Column(DbType = "BIGINT")]
        public long ChatId
        {
            get { return chatId; }
            set
            {
                if (chatId != value)
                {
                    NotifyPropertyChanging("ChatId");
                    chatId = value;
                    NotifyPropertyChanged("ChatId");
                }
            }
        }

        private string chatActiveIds;
        [Column(DbType = "NVARCHAR(1024)")]
        public string ChatActiveIds
        {
            get { return chatActiveIds; }
            set
            {
                if (chatActiveIds != value)
                {
                    NotifyPropertyChanging("ChatActiveIds");
                    chatActiveIds = value;
                    NotifyPropertyChanged("ChatActiveIds");
                }
            }
        }
    }
}
