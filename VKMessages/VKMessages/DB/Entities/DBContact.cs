﻿using System.Data.Linq.Mapping;

namespace VKMessages.DB.Entities
{
    [Table(Name = "Contacts")]
    public class DBContact : DBEntityBase
    {
        private long id;
        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "BIGINT IDENTITY",
                CanBeNull = false)]
        public long ID
        {
            get { return id; }
            set
            {
                if (id != value)
                {
                    NotifyPropertyChanging("ID");
                    id = value;
                    NotifyPropertyChanged("ID");
                }
            }
        }

        private long uid;
        [Column(DbType = "BIGINT")]
        public long UID
        {
            get { return uid; }
            set
            {
                if (uid != value)
                {
                    NotifyPropertyChanging("UID");
                    uid = value;
                    NotifyPropertyChanged("UID");
                }
            }
        }

        private string displayName;
        [Column(DbType = "NVARCHAR(100)")]
        public string DisplayName
        {
            get { return displayName; }
            set
            {
                if (displayName != value)
                {
                    NotifyPropertyChanging("DisplayName");
                    displayName = value;
                    NotifyPropertyChanged("DisplayName");
                }
            }
        }

        private string vkName;
        [Column(DbType = "NVARCHAR(100)")]
        public string VKName
        {
            get { return vkName; }
            set
            {
                if (vkName != value)
                {
                    NotifyPropertyChanging("VKName");
                    vkName = value;
                    NotifyPropertyChanged("VKName");
                }
            }
        }

        private byte[] image;
        [Column(DbType = "IMAGE", UpdateCheck = UpdateCheck.Never)]
        public byte[] Image
        {
            get { return image; }
            set
            {
                if (image != value)
                {
                    NotifyPropertyChanging("Image");
                    image = value;
                    NotifyPropertyChanged("Image");
                }
            }
        }

        private string phone;
        [Column(DbType = "NVARCHAR(100)")]
        public string Phone
        {
            get { return phone; }
            set
            {
                if (phone != value)
                {
                    NotifyPropertyChanging("Phone");
                    phone = value;
                    NotifyPropertyChanged("Phone");
                }
            }
        }
    }
}
