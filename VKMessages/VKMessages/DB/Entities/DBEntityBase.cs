﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows.Media.Imaging;

namespace VKMessages.DB.Entities
{
    public class DBEntityBase : INotifyPropertyChanging, INotifyPropertyChanged
    {
        public event PropertyChangingEventHandler PropertyChanging;

        protected void NotifyPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private byte[] ImageToByteArray(WriteableBitmap imageIn)
        {
            using (var ms = new MemoryStream())
            {
                imageIn.SaveJpeg(ms, imageIn.PixelWidth, imageIn.PixelHeight, 0, 100);
                return ms.ToArray();
            }
        }

        public static BitmapImage ByteArrayToImage(byte[] byteArrayIn)
        {
            BitmapImage bitmapImage;
            if (byteArrayIn != null)
            {
                using (var memoryStream = new MemoryStream(byteArrayIn, 0, (int)byteArrayIn.Length))
                {
                    bitmapImage = new BitmapImage();
                    bitmapImage.SetSource(memoryStream);
                }
                return bitmapImage;
            }
            
            return null;
        }
    }
}
