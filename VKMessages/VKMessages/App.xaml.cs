﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using VKMessages.Tools;
using VKMessages.ViewModels;

namespace VKMessages
{
    public partial class App : Application
    {
        private static MainViewModel viewModel = null;

        /// <summary>
        /// A static ViewModel used by the views to bind against.
        /// </summary>
        /// <returns>The MainViewModel object.</returns>
        public static MainViewModel ViewModel
        {
            get
            {
                return viewModel ?? (viewModel = new MainViewModel());
            }
        }

        private static AttachmentsViewModel attachmentsViewModel = null;
        public static AttachmentsViewModel AttachmentsViewModel
        {
            get
            {
                return attachmentsViewModel ?? (attachmentsViewModel = new AttachmentsViewModel());
            }
        }

        private static SearchViewModel searchViewModel = null;
        public static SearchViewModel SearchViewModel
        {
            get
            {
                return searchViewModel ?? (searchViewModel = new SearchViewModel());
            }
        }

        private static CapchaViewModel capchaViewModel = null;
        public static CapchaViewModel CapchaViewModel
        {
            get
            {
                return capchaViewModel ?? (capchaViewModel = new CapchaViewModel());
            }
        }

        private static ContactViewModel contactViewModel = null;
        public static ContactViewModel ContactViewModel
        {
            get
            {
                return contactViewModel ?? (contactViewModel = new ContactViewModel());
            }
        }

        private static FriendRequestViewModel friendRequestViewModel = null;
        public static FriendRequestViewModel FriendRequestViewModel
        {
            get
            {
                return friendRequestViewModel ?? (friendRequestViewModel = new FriendRequestViewModel());
            }
        }

        private static CreateChatViewModel createChatViewModel = null;
        public static CreateChatViewModel CreateChatViewModel
        {
            get
            {
                return createChatViewModel ?? (createChatViewModel = new CreateChatViewModel());
            }
        }

        private static PreviewViewModel previewViewModel = null;
        public static PreviewViewModel PreviewViewModel
        {
            get
            {
                return previewViewModel ?? (previewViewModel = new PreviewViewModel());
            }
        }

        private static Dictionary<long, ConversationViewModel> conversations = new Dictionary<long, ConversationViewModel>();

        public static ConversationViewModel ConversationViewModel
        {
            get
            {
                if (conversations.ContainsKey(CrossPageParameters.FriendUId))
                {
                    var resultVM = conversations[CrossPageParameters.FriendUId];
                    resultVM.PrepareToOpen();
                    return resultVM;
                }

                var newVM = new ConversationViewModel();
                conversations.Add(CrossPageParameters.FriendUId, newVM);
                return newVM;
            }
        }

        public static ConversationViewModel GetConversationViewModelByUID(long uid)
        {
            if (conversations.ContainsKey(uid))
                return conversations[uid];

            return null;
        }

        private static Dictionary<long, ChatViewModel> chats = new Dictionary<long, ChatViewModel>();

        public static ChatViewModel ChatViewModel
        {
            get
            {
                if (chats.ContainsKey(CrossPageParameters.ChatId))
                {
                    var resultVM = chats[CrossPageParameters.ChatId];
                    resultVM.PrepareToOpen();
                    return resultVM;
                }

                var newVM = new ChatViewModel();
                chats.Add(CrossPageParameters.ChatId, newVM);
                return newVM;
            }
        }

        public static ChatViewModel GetChatViewModelByUID(long uid)
        {
            if (chats.ContainsKey(uid))
                return chats[uid];

            return null;
        }

        private static InvitationViewModel invitationViewModel = null;

        public static InvitationViewModel InvitationViewModel
        {
            get { return invitationViewModel ?? (invitationViewModel = new InvitationViewModel()); }
        }

        private static LoginViewModel loginViewModel = null;

        public static LoginViewModel LoginViewModel
        {
            get { return loginViewModel ?? (loginViewModel = new LoginViewModel()); }
        }

        private static SignUpViewModel signUpViewModel = null;

        public static SignUpViewModel SignUpViewModel
        {
            get { return signUpViewModel ?? (signUpViewModel = new SignUpViewModel()); }
        }

        private static ConfirmViewModel signUpConfirmViewModel = null;

        public static ConfirmViewModel ConfirmViewModel
        {
            get { return signUpConfirmViewModel ?? (signUpConfirmViewModel = new ConfirmViewModel()); }
        }

        private static FriendsRequestViewModel friendRequestsViewModel = null;

        public static FriendsRequestViewModel FriendsRequestViewModel
        {
            get { return friendRequestsViewModel ?? (friendRequestsViewModel = new FriendsRequestViewModel()); }
        }

        private static SettingsViewModel settingsViewModel = null;

        public static SettingsViewModel SettingsViewModel
        {
            get { return settingsViewModel ?? (settingsViewModel = new SettingsViewModel()); }
        }

        /// <summary>
        /// Provides easy access to the root frame of the Phone Application.
        /// </summary>
        /// <returns>The root frame of the Phone Application.</returns>
        public TransitionFrame RootFrame { get; private set; }

        /// <summary>
        /// Constructor for the Application object.
        /// </summary>
        public App()
        {
            // Global handler for uncaught exceptions. 
            UnhandledException += Application_UnhandledException;

            // Standard Silverlight initialization
            InitializeComponent();

            // Phone-specific initialization
            InitializePhoneApplication();

            // Show graphics profiling information while debugging.
            if (System.Diagnostics.Debugger.IsAttached)
            {
                // Display the current frame rate counters
                Application.Current.Host.Settings.EnableFrameRateCounter = true;

                // Show the areas of the app that are being redrawn in each frame.
                //Application.Current.Host.Settings.EnableRedrawRegions = true;

                // Enable non-production analysis visualization mode, 
                // which shows areas of a page that are handed off to GPU with a colored overlay.
                //Application.Current.Host.Settings.EnableCacheVisualization = true;

                // Disable the application idle detection by setting the UserIdleDetectionMode property of the
                // application's PhoneApplicationService object to Disabled.
                // Caution:- Use this under debug mode only. Application that disables user idle detection will continue to run
                // and consume battery power when the user is not using the phone.
                PhoneApplicationService.Current.UserIdleDetectionMode = IdleDetectionMode.Disabled;
            }
        }

        // Code to execute when the application is launching (eg, from Start)
        // This code will not execute when the application is reactivated
        private void Application_Launching(object sender, LaunchingEventArgs e)
        {            
        }

        // Code to execute when the application is activated (brought to foreground)
        // This code will not execute when the application is first launched
        private void Application_Activated(object sender, ActivatedEventArgs e)
        {
            ImageDownloader.Start();
            // Ensure that application state is restored appropriately
            if (!App.ViewModel.IsDataLoaded)
            {
                App.ViewModel.LoadData();
            }
        }

        // Code to execute when the application is deactivated (sent to background)
        // This code will not execute when the application is closing
        private void Application_Deactivated(object sender, DeactivatedEventArgs e)
        {
            ImageDownloader.Stop();
        }

        // Code to execute when the application is closing (eg, user hit Back)
        // This code will not execute when the application is deactivated
        private void Application_Closing(object sender, ClosingEventArgs e)
        {
            ImageDownloader.Stop();
            // Ensure that required application state is persisted here.
        }

        // Code to execute if a navigation fails
        private void RootFrame_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                // A navigation has failed; break into the debugger
                System.Diagnostics.Debugger.Break();
            }
        }

        // Code to execute on Unhandled Exceptions
        private void Application_UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                // An unhandled exception has occurred; break into the debugger
                System.Diagnostics.Debugger.Break();
            }
        }

        #region Phone application initialization

        // Avoid double-initialization
        private bool phoneApplicationInitialized = false;

        // Do not add any additional code to this method
        private void InitializePhoneApplication()
        {
            if (phoneApplicationInitialized)
                return;

            // Create the frame but don't set it as RootVisual yet; this allows the splash
            // screen to remain active until the application is ready to render.
            RootFrame = new TransitionFrame();
            RootFrame.Navigated += CompleteInitializePhoneApplication;

            // Handle navigation failures
            RootFrame.NavigationFailed += RootFrame_NavigationFailed;

            // Ensure we don't initialize again
            phoneApplicationInitialized = true;
        }

        // Do not add any additional code to this method
        private void CompleteInitializePhoneApplication(object sender, NavigationEventArgs e)
        {
            // Set the root visual to allow the application to render
            if (RootVisual != RootFrame)
                RootVisual = RootFrame;

            // Remove this handler since it is no longer needed
            RootFrame.Navigated -= CompleteInitializePhoneApplication;
        }

        #endregion
    }
}