﻿using System;
using System.IO.IsolatedStorage;

namespace VKMessages.Tools
{
    public static class SharedPreferences
    {
        private static string accessTokenKey = "access_token";
        private static string contactsSyncedKey = "contacts_synced";
        private static string soundsDisableTillKey = "sound_disale_till";
        private static string userIdKey = "user_id";
        private static string userNameKey = "user_name";
        private static string userPhotoUrlKey = "user_photo_url";
        private static string soundOnKey = "sound_on";
        private static string vibrateOnKey = "vibrate_on";
        private static string pushOnKey = "push_on";

        public const string TransfersPath = "/shared/transfers";

        static SharedPreferences()
        {
            if (!IsolatedStorageSettings.ApplicationSettings.Contains(soundOnKey))
                IsolatedStorageSettings.ApplicationSettings.Add(soundOnKey, true);

            if (!IsolatedStorageSettings.ApplicationSettings.Contains(vibrateOnKey))
                IsolatedStorageSettings.ApplicationSettings.Add(vibrateOnKey, true);

            if (!IsolatedStorageSettings.ApplicationSettings.Contains(pushOnKey))
                IsolatedStorageSettings.ApplicationSettings.Add(pushOnKey, true);
        }

        private static string BaseStringGetter(string key)
        {
            string val;
            return IsolatedStorageSettings.ApplicationSettings.TryGetValue(key, out val) ?
                   val :
                   string.Empty;
        }

        private static long BaseInt64Getter(string key)
        {
            long val;
            return IsolatedStorageSettings.ApplicationSettings.TryGetValue(key, out val) ?
                   val :
                   0;
        }

        private static DateTime BaseDateTimeGetter(string key)
        {
            DateTime val;
            return IsolatedStorageSettings.ApplicationSettings.TryGetValue(key, out val) ?
                   val :
                   DateTime.MinValue;
        }

        private static bool BaseBoolGetter(string key)
        {
            bool val;
            return IsolatedStorageSettings.ApplicationSettings.TryGetValue(key, out val) && val;
        }

        private static void BaseStringSetter(string key, string val)
        {
            if (!IsolatedStorageSettings.ApplicationSettings.Contains(key))
                IsolatedStorageSettings.ApplicationSettings.Add(key, val);
            else
                IsolatedStorageSettings.ApplicationSettings[key] = val;

            IsolatedStorageSettings.ApplicationSettings.Save();
        }

        private static void BaseInt64Setter(string key, long val)
        {
            if (!IsolatedStorageSettings.ApplicationSettings.Contains(key))
                IsolatedStorageSettings.ApplicationSettings.Add(key, val);
            else
                IsolatedStorageSettings.ApplicationSettings[key] = val;

            IsolatedStorageSettings.ApplicationSettings.Save();
        }

        private static void BaseDateTimeSetter(string key, DateTime val)
        {
            if (!IsolatedStorageSettings.ApplicationSettings.Contains(key))
                IsolatedStorageSettings.ApplicationSettings.Add(key, val);
            else
                IsolatedStorageSettings.ApplicationSettings[key] = val;

            IsolatedStorageSettings.ApplicationSettings.Save();
        }

        private static void BaseBoolSetter(string key, bool val)
        {
            if (!IsolatedStorageSettings.ApplicationSettings.Contains(key))
                IsolatedStorageSettings.ApplicationSettings.Add(key, val);
            else
                IsolatedStorageSettings.ApplicationSettings[key] = val;

            IsolatedStorageSettings.ApplicationSettings.Save();
        }

        public static string AccessToken
        {
            get { return BaseStringGetter(accessTokenKey); }
            set { BaseStringSetter(accessTokenKey, value); }
        }

        public static bool IsContactsSynced
        {
            get { return BaseBoolGetter(contactsSyncedKey); }
            set { BaseBoolSetter(contactsSyncedKey, value); }
        }

        public static long UserId
        {
            get { return BaseInt64Getter(userIdKey); }
            set { BaseInt64Setter(userIdKey, value); }
        }

        public static string UserName
        {
            get { return BaseStringGetter(userNameKey); }
            set { BaseStringSetter(userNameKey, value); }
        }

        public static string UserPhotoUrl
        {
            get { return BaseStringGetter(userPhotoUrlKey); }
            set { BaseStringSetter(userPhotoUrlKey, value); }
        }

        public static bool IsSoundOn
        {
            get { return BaseBoolGetter(soundOnKey); }
            set { BaseBoolSetter(soundOnKey, value); }
        }

        public static bool IsVibrateOn
        {
            get { return BaseBoolGetter(vibrateOnKey); }
            set { BaseBoolSetter(vibrateOnKey, value); }
        }

        public static bool IsPushOn
        {
            get { return BaseBoolGetter(pushOnKey); }
            set { BaseBoolSetter(pushOnKey, value); }
        }
    }
}
