﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows.Threading;
using VKMessages.ViewModels;

namespace VKMessages.Tools
{
    public static class ImageDownloader
    {
        static ImageDownloader()
        {
            dt = new DispatcherTimer { Interval = TimeSpan.FromSeconds(5) };
            dt.Tick += dt_Tick;
        }

        private static bool _busy = false;
        private static DispatcherTimer dt;

        private static Queue<HttpRequestParams> _queue = new Queue<HttpRequestParams>();

        public static void Start()
        {
            dt.Start();
        }

        public static void Stop()
        {
            dt.Stop();
        }

        static void dt_Tick(object sender, EventArgs e)
        {
            RunNextTask();
        }

        private static void RunNextTask()
        {
            if (_queue.Count > 0 && !_busy)
            {
                var task = _queue.Dequeue();
                if (task == null || task.Request == null)
                    return;

                _busy = true;

                task.Request.BeginGetResponse(TaskCompleted, task);
            }
        }

        private static void TaskCompleted(IAsyncResult result)
        {
            _busy = false;

            RunNextTask();

            var param = result.AsyncState as HttpRequestParams;
            if (param == null)
                return;

            if (param.Callback != null)
                param.Callback(result);
        }

        public static void AddTask(HttpRequestParams task)
        {
            _queue.Enqueue(task);
        }
    }
}
