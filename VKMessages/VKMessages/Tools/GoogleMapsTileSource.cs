﻿using System;
using System.Globalization;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls.Maps;

namespace VKMessages.Tools
{
    public class GoogleMapsTileSource : TileSource
	{
        public GoogleMapsTileSource()
            : base("http://mt{0}.google.com/vt/lyrs=m@128&hl={1}&x={2}&y={3}&z={4}")
		{
		}

		public override Uri GetUri(int x, int y, int zoomLevel)
		{
			var objArray = new object[5];
			objArray[0] = (new Random()).Next() % 4;
            objArray[1] = CultureInfo.CurrentUICulture.TwoLetterISOLanguageName;
			objArray[2] = x;
			objArray[3] = y;
			objArray[4] = zoomLevel;
			return new Uri(string.Format(base.UriFormat, objArray));
		}
	}
}
