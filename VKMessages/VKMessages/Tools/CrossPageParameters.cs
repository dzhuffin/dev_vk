﻿using System.Device.Location;
using VKMessages.API.JSON;
using System.Collections.Generic;
using VKMessages.ViewModels;

namespace VKMessages.Tools
{
    public static class CrossPageParameters
    {
        public static long FriendUId;
        public static string FriendVKName;
        public static bool FriendIsOnline;
        public static string ConfirmPhoneNumber;
        public static long ChatId;
        public static long AttachmentsCount = 0;
        public static string ChatName;
        public static JsonLongPoolData LongPoolData;
        public static JsonError LastError;
        public static readonly List<long> FirstFiveUids = new List<long>();
        public static ContactItemViewModel CurrentContactVM;
        public static FriendItemViewModel CurrentFriendVM;
        public static ChatViewModel CurrentChatVM;
        public static List<long> CurrentChatUIDs;
        public static DialogItemViewModel CurrentDialog;
        public static string PreviewImageUrl;
        public static string PreviewVideoUrl;
        public static string CurrentTitle;
        public static GeoCoordinate PickedLocation;

        private static long friendRequestsCount;
        public static long FriendRequestsCount
        {
            get { return friendRequestsCount; }
            set
            {
                if (friendRequestsCount != value)
                {
                    friendRequestsCount = value;
                    App.ViewModel.RefreshCounters();
                }
            }
        }
    }
}
