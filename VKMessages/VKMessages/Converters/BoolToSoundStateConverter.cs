﻿using System;
using System.Globalization;

namespace VKMessages.Tools
{
    public class BoolToSoundStateConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value
                       ? Resource.SETTINGS_PUSH_NOTIFY_ON
                       : Resource.SETTINGS_PUSH_NOTIFY_OFF;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // Do nothing
            return null;
        }
    }
}
