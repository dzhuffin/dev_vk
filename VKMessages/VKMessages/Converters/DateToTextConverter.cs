﻿using System;
using System.Globalization;

namespace VKMessages.Converters
{
    public class DateToTextConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var d = (DateTime)value;

            if (DateTime.Now.Year > d.Year)
            {
                return d.ToShortDateString();
            }

            var s = DateTime.Now.Subtract(d);

            var dayDiff = (int)s.TotalDays;

            if (dayDiff < 0 || dayDiff >= 2)
            {
                if (CultureInfo.CurrentUICulture.TwoLetterISOLanguageName == "ru")
                    return d.ToString("dd.MM", CultureInfo.CurrentUICulture);

                return d.ToString("MM/dd", CultureInfo.CurrentUICulture);
            }

            if (dayDiff == 0)
            {
                return d.ToString("hh:mm", CultureInfo.CurrentUICulture); ;
            }
            if (dayDiff == 1)
            {
                return Resource.DATE_YESTERDAY;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // Do nothing
            return null;
        }
    }
}
