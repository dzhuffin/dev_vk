﻿using System;
using System.Globalization;

namespace VKMessages.Tools
{
    public class BoolToVibrateStateConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value
                       ? Resource.SETTINGS_VIBRATION_ON
                       : Resource.SETTINGS_VIBRATION_OFF;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // Do nothing
            return null;
        }
    }
}
