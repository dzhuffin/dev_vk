﻿using System;
using System.Globalization;

namespace VKMessages.Tools
{
    public class FriendStatusBoolConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool) value
                       ? Resource.CONVERSATION_STATUS_ONLINE
                       : Resource.CONVERSATION_STATUS_OFFLINE;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // Do nothing
            return null;
        }
    }
}
