﻿using System;
using System.Windows;
using VKMessages.ViewModels;

namespace VKMessages.Converters
{
    public class LoginStatusToVisibilityConverterInv : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (LoginStatus)value != LoginStatus.WaitingForInput;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            // Do nothing
            return null;
        }
    }
}
