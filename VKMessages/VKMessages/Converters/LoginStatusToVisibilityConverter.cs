﻿using System;
using System.Windows;
using VKMessages.ViewModels;

namespace VKMessages.Converters
{
    public class LoginStatusToVisibilityConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (LoginStatus)value == LoginStatus.WaitingForInput ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            // Do nothing
            return null;
        }
    }
}
