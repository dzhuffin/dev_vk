﻿using System;
using System.Windows;

namespace VKMessages.Converters
{
    public class BoolToVisibilityConverterInv : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (bool)value ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            // Do nothing
            return null;
        }
    }
}
