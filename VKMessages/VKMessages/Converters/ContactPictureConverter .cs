﻿using System;
using Microsoft.Phone.UserData;

namespace VKMessages.Converters
{
    public class ContactPictureConverter : System.Windows.Data.IValueConverter
    {
        //private static StreamResourceInfo stream;

        static ContactPictureConverter()
        {
            //stream = Application.GetResourceStream(new Uri("/Images/Components/Requests_Icon.png", UriKind.Relative));            
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var c = value as Contact;
            if (c == null) 
                return null;

            var imageStream = c.GetPicture();

            return imageStream != null ? 
                Microsoft.Phone.PictureDecoder.DecodeJpeg(imageStream) :
                null;
        }
 
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            // Do nothing
            return null;
        }
    }
}
