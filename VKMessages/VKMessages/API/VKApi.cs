﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using Microsoft.Phone.UserData;
using VKMessages.API.JSON;
using VKMessages.DB.Entities;
using VKMessages.Tools;
using VKMessages.ViewModels;

namespace VKMessages.API
{
    public class VKApiOperationCompletedEventArgs : AsyncCompletedEventArgs
    {
        public bool IsSuccess { get; set; }
        public int ErrorCode { get; set; }
        public object Result { get; set; }
    }

    class ByRequestParamsAsync
    {
        public VKApi.VKApiOperationCompletedEventHandler EventHandler;
        public List<JsonErrorRequestParam> Params;
    }

    public static class VKApi
    {
        public delegate void VKApiOperationCompletedEventHandler(object sender, VKApiOperationCompletedEventArgs e);
        private static event VKApiOperationCompletedEventHandler checkTokenCompletedEventHandler;
        private static event VKApiOperationCompletedEventHandler friendsAndDialogsGetCompletedEventHandler;
        private static event VKApiOperationCompletedEventHandler friendsGetCompletedEventHandler;
        private static event VKApiOperationCompletedEventHandler friendDeleteCompletedEventHandler;
        private static event VKApiOperationCompletedEventHandler friendAddCompletedEventHandler;        
        private static event VKApiOperationCompletedEventHandler getByPhonesCompletedEventHandler;
        private static event VKApiOperationCompletedEventHandler messagesGetCompletedEventHandler;
        private static event VKApiOperationCompletedEventHandler dialogsGetCompletedEventHandler;
        private static event VKApiOperationCompletedEventHandler requestTokenCompletedEventHandler;
        private static event VKApiOperationCompletedEventHandler chechPhoneCompletedEventHandler;
        private static event VKApiOperationCompletedEventHandler signUpCompletedEventHandler;
        private static event VKApiOperationCompletedEventHandler signUpConfirmCompletedEventHandler;
        private static event VKApiOperationCompletedEventHandler messageSendCompletedEventHandler;
        private static event VKApiOperationCompletedEventHandler connectToLongPoolCompletedEventHandler;
        private static event VKApiOperationCompletedEventHandler longPoolRequestCompletedEventHandler;
        private static event VKApiOperationCompletedEventHandler registerDeviceCompletedEventHandler;
        private static event VKApiOperationCompletedEventHandler unregisterDeviceCompletedEventHandler;
        private static event VKApiOperationCompletedEventHandler setSilenceModeCompletedEventHandler;
        private static event VKApiOperationCompletedEventHandler getPushSettingsCompletedEventHandler;
        private static event VKApiOperationCompletedEventHandler getProfileUploadServerEventHandler;
        private static event VKApiOperationCompletedEventHandler getMessagesUploadServerEventHandler;
        private static event VKApiOperationCompletedEventHandler getCurrentProfileCompletedEventHandler;
        private static event VKApiOperationCompletedEventHandler saveProfilePhotoCompletedEventHandler;
        private static event VKApiOperationCompletedEventHandler saveMessagePhotoCompletedEventHandler;
        private static event VKApiOperationCompletedEventHandler getFriendRequestsCompletedEventHandler;
        private static event VKApiOperationCompletedEventHandler getFriendSuggestionsCompletedEventHandler;
        private static event VKApiOperationCompletedEventHandler friendRequestsCountCompletedEventHandler;
        private static event VKApiOperationCompletedEventHandler messageGetByIdCompletedEventHandler;
        private static event VKApiOperationCompletedEventHandler messageGetChatMsgByIdCompletedEventHandler;
        private static event VKApiOperationCompletedEventHandler unreadCountCompletedEventHandler;
        private static event VKApiOperationCompletedEventHandler createChatCompletedEventHandler;
        private static event VKApiOperationCompletedEventHandler editChatCompletedEventHandler;

        public static void ByRequestParams(string code, JsonError data, VKApiOperationCompletedEventHandler eventHandler)
        {
            var webClient = new WebClient();
            webClient.DownloadStringCompleted += data.EventHandler;
            var uri = Requests.ParseRequestParams(code, data.CaptchaSID, data.RequestParams);
            webClient.DownloadStringAsync(uri, new ByRequestParamsAsync {Params = data.RequestParams, EventHandler = eventHandler});
        }

        public static void SaveProfilePhoto(long server, string photo, string hash, VKApiOperationCompletedEventHandler eventHandler)
        {
            saveProfilePhotoCompletedEventHandler = eventHandler;

            var webClient = new WebClient();
            webClient.DownloadStringCompleted += SaveProfilePhotoCompleted;
            var uri = Requests.GetSaveProfilePhotoUri(server, photo, hash);
            webClient.DownloadStringAsync(uri);
        }

        public static void SaveMessagePhoto(long server, string photo, string hash, VKApiOperationCompletedEventHandler eventHandler)
        {
            saveMessagePhotoCompletedEventHandler = eventHandler;

            var webClient = new WebClient();
            webClient.DownloadStringCompleted += SaveMessagePhotoCompleted;
            var uri = Requests.GetSaveMessagePhotoUri(server, photo, hash);
            webClient.DownloadStringAsync(uri);
        }

        public static void MakeAsRead(IEnumerable<long> mids)
        {
            var webClient = new WebClient();
            var uri = Requests.GetMakeAsReadUri(mids);
            webClient.DownloadStringAsync(uri);
        }

        public static void GetUnreadCount(VKApiOperationCompletedEventHandler eventHandler)
        {
            unreadCountCompletedEventHandler = eventHandler;

            var webClient = new WebClient();
            webClient.DownloadStringCompleted += GetUnreadCountCompleted;
            var uri = Requests.GetExecuteUri(Requests.GetUnreadCountCodeFormat);
            webClient.DownloadStringAsync(uri);
        }

        public static void CreateChat(IEnumerable<long> uids, string title, VKApiOperationCompletedEventHandler eventHandler)
        {
            createChatCompletedEventHandler = eventHandler;

            var webClient = new WebClient();
            webClient.DownloadStringCompleted += CreateChatCompleted;
            var uri = Requests.GetCreateChatUri(uids, title);
            webClient.DownloadStringAsync(uri);
        }

        public static void EditChat(long chat_id, string title, IEnumerable<long> a_uids, IEnumerable<long> d_uids, VKApiOperationCompletedEventHandler eventHandler)
        {
            editChatCompletedEventHandler = eventHandler;

            var webClient = new WebClient();
            webClient.DownloadStringCompleted += EditChatCompleted;
            var uri = Requests.GetEditChatUri(chat_id, title, a_uids, d_uids);
            //var uri = Requests.GetEditChatUri(chat_id, title);
            webClient.DownloadStringAsync(uri);
        }

        public static void GetFriendSuggestions(long offset, VKApiOperationCompletedEventHandler eventHandler)
        {
            getFriendSuggestionsCompletedEventHandler = eventHandler;

            var webClient = new WebClient();
            webClient.DownloadStringCompleted += GetFriendSuggestionsCompleted;
            var uri = Requests.GetExecuteUri(Requests.FriendSuggestionsGetCodeFormat.Replace("%%OFFSET%%", offset.ToString()));
            webClient.DownloadStringAsync(uri);
        }

        public static void GetMessageById(long mid, VKApiOperationCompletedEventHandler eventHandler)
        {
            messageGetByIdCompletedEventHandler = eventHandler;

            var webClient = new WebClient();
            webClient.DownloadStringCompleted += GetMessageByIdCompleted;
            var uri = Requests.GetMessagesById(mid);
            webClient.DownloadStringAsync(uri);
        }

        public static void GetChatMessageById(long mid, VKApiOperationCompletedEventHandler eventHandler)
        {
            messageGetChatMsgByIdCompletedEventHandler = eventHandler;

            var webClient = new WebClient();
            webClient.DownloadStringCompleted += GetChatMessageByIdCompleted;
            var uri = Requests.GetMessagesById(mid);
            webClient.DownloadStringAsync(uri);
        }

        public static void GetFriendRequests(VKApiOperationCompletedEventHandler eventHandler)
        {
            getFriendRequestsCompletedEventHandler = eventHandler;

            var webClient = new WebClient();
            webClient.DownloadStringCompleted += GetFriendRequestsCompleted;
            var uri = Requests.GetExecuteUri(Requests.FriendRequestsGetCodeFormat);
            webClient.DownloadStringAsync(uri);
        }

        public static void GetFriendRequestsCount(VKApiOperationCompletedEventHandler eventHandler)
        {
            friendRequestsCountCompletedEventHandler = eventHandler;

            var webClient = new WebClient();
            webClient.DownloadStringCompleted += GetFriendRequestsCountCompleted;
            var uri = Requests.GetRequestsCountUri();
            webClient.DownloadStringAsync(uri);
        }

        public static void SetSilenceMode(string token, long time, VKApiOperationCompletedEventHandler eventHandler)
        {
            setSilenceModeCompletedEventHandler = eventHandler;

            var webClient = new WebClient();
            webClient.DownloadStringCompleted += SetSilenceModeCompleted;
            var uri = Requests.GetSetSilenceModeUri(token, time);
            webClient.DownloadStringAsync(uri);
        }

        public static void FriendAdd(long uid, VKApiOperationCompletedEventHandler eventHandler)
        {
            friendAddCompletedEventHandler = eventHandler;

            var webClient = new WebClient();
            webClient.DownloadStringCompleted += FriendAddCompleted;
            var uri = Requests.GetFriendAddUri(uid);
            webClient.DownloadStringAsync(uri);
        }

        public static void FriendDelete(long uid, VKApiOperationCompletedEventHandler eventHandler)
        {
            friendDeleteCompletedEventHandler = eventHandler;

            var webClient = new WebClient();
            webClient.DownloadStringCompleted += FriendDeleteCompleted;
            var uri = Requests.GetFriendDeleteUri(uid);
            webClient.DownloadStringAsync(uri);
        }

        public static void GetPushSettings(string token, VKApiOperationCompletedEventHandler eventHandler)
        {
            getPushSettingsCompletedEventHandler = eventHandler;

            var webClient = new WebClient();
            webClient.DownloadStringCompleted += GetPushSettingsCompleted;
            var uri = Requests.GetPushSettingsUri(token);
            webClient.DownloadStringAsync(uri);
        }

        public static void GetProfileUploadServer(VKApiOperationCompletedEventHandler eventHandler)
        {
            getProfileUploadServerEventHandler = eventHandler;

            var webClient = new WebClient();
            webClient.DownloadStringCompleted += GetProfileUploadServerCompleted;
            var uri = Requests.GetProfileUploadServerUri();
            webClient.DownloadStringAsync(uri);
        }

        public static void GetMessagesUploadServer(VKApiOperationCompletedEventHandler eventHandler)
        {
            getMessagesUploadServerEventHandler = eventHandler;

            var webClient = new WebClient();
            webClient.DownloadStringCompleted += GetMessagesUploadServerCompleted;
            var uri = Requests.GetMessagesUploadServerUri();
            webClient.DownloadStringAsync(uri);
        }

        public static void GetCurrentProfile(VKApiOperationCompletedEventHandler eventHandler)
        {
            getCurrentProfileCompletedEventHandler = eventHandler;

            var webClient = new WebClient();
            webClient.DownloadStringCompleted += GetCurrentProfileCompleted;
            var uri = Requests.GetCurrentProfileUri();
            webClient.DownloadStringAsync(uri);
        }

        public static void RegisterDevice(string token, VKApiOperationCompletedEventHandler eventHandler)
        {
            registerDeviceCompletedEventHandler = eventHandler;

            var webClient = new WebClient();
            webClient.DownloadStringCompleted += RegisterDeviceCompleted;
            var uri = Requests.GetRegisterDeviceUri(token);
            webClient.DownloadStringAsync(uri);
        }

        public static void UnregisterDevice(string token, VKApiOperationCompletedEventHandler eventHandler)
        {
            unregisterDeviceCompletedEventHandler = eventHandler;

            var webClient = new WebClient();
            webClient.DownloadStringCompleted += UnregisterDeviceCompleted;
            var uri = Requests.GetUnregisterDeviceUri(token);
            webClient.DownloadStringAsync(uri);
        }

        public static void RequestToLongPool(JsonLongPoolData data, VKApiOperationCompletedEventHandler eventHandler)
        {
            longPoolRequestCompletedEventHandler = eventHandler;

            var webClient = new WebClient();
            webClient.DownloadStringCompleted += LongPoolRequestCompleted;
            var uri = Requests.GetRequestToLongPoolServerUri(data.Server, data.Key, data.Ts);
            webClient.DownloadStringAsync(uri);
        }

        public static void ConnectToLongPool(VKApiOperationCompletedEventHandler eventHandler)
        {
            connectToLongPoolCompletedEventHandler = eventHandler;

            var webClient = new WebClient();
            webClient.DownloadStringCompleted += GetLongPoolServerCompleted;
            var uri = Requests.GetLongPoolServerUri();
            webClient.DownloadStringAsync(uri);
        }

        public static void SignUp(string phone, string name, string lastName, VKApiOperationCompletedEventHandler eventHandler)
        {
            signUpCompletedEventHandler = eventHandler;

            var webClient = new WebClient();
            webClient.DownloadStringCompleted += SignUpCompleted;
            var uri = Requests.GetSignUpUri(phone, name, lastName);
            webClient.DownloadStringAsync(uri);
        }

        public static void SignUpConfirm(string phone, string code, string password, VKApiOperationCompletedEventHandler eventHandler)
        {
            signUpConfirmCompletedEventHandler = eventHandler;

            var webClient = new WebClient();
            webClient.DownloadStringCompleted += SignUpConfirmCompleted;
            var uri = Requests.GetSignUpConfirmUri(phone, code, password);
            webClient.DownloadStringAsync(uri);
        }

        public static void CheckPhone(string phone, VKApiOperationCompletedEventHandler eventHandler)
        {
            chechPhoneCompletedEventHandler = eventHandler;

            var webClient = new WebClient();
            webClient.DownloadStringCompleted += CheckPhoneCompleted;
            webClient.DownloadStringAsync(Requests.GetCheckPhoneUri(phone));
        }

        public static void RequestAccessToken(string login, string password, VKApiOperationCompletedEventHandler eventHandler)
        {
            requestTokenCompletedEventHandler = eventHandler;

            var webClient = new WebClient();
            webClient.DownloadStringCompleted += RequestAccessTokenCompleted;
            webClient.DownloadStringAsync(Requests.GetOAuthUri(login, password));
        }

        public static void FriendsAndDialogsGet(VKApiOperationCompletedEventHandler eventHandler)
        {
            friendsAndDialogsGetCompletedEventHandler = eventHandler;

            var webClient = new WebClient();
            webClient.DownloadStringCompleted += FriendsAndDialogsGetCompleted;
            webClient.DownloadStringAsync(Requests.GetExecuteUri(Requests.FriendsGetCodeFormat));
        }

        public static void FriendsGet(int offset, VKApiOperationCompletedEventHandler eventHandler)
        {
            friendsGetCompletedEventHandler = eventHandler;

            var webClient = new WebClient();
            webClient.DownloadStringCompleted += FriendsGetCompleted;
            webClient.DownloadStringAsync(Requests.GetFriendsGetUri(offset));
        }

        public static void GetByPhones(string phonesList, List<Contact> contacts, VKApiOperationCompletedEventHandler eventHandler)
        {
            getByPhonesCompletedEventHandler = eventHandler;

            var webClient = new WebClient();
            webClient.DownloadStringCompleted += GetByPhonesCompleted;
            webClient.DownloadStringAsync(Requests.GetByPhonesUri(phonesList), contacts);
        }

        public static void MessageSend(long uid, string message, List<Attachment> attachments,  bool isChat, VKApiOperationCompletedEventHandler eventHandler)
        {
            messageSendCompletedEventHandler = eventHandler;

            var webClient = new WebClient();
            webClient.DownloadStringCompleted += MessageSendCompleted;
            webClient.DownloadStringAsync(Requests.GetMessagesSendUri(uid, message, attachments, isChat), new DBMessage { ToId = isChat ? uid + 2000000000 : uid, ChatId = isChat ? uid : 0, Body = message, Out = 1, ReadState = 1, Date = (long)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds });
        }

        public static void DialogsGet(int offset, VKApiOperationCompletedEventHandler eventHandler)
        {
            dialogsGetCompletedEventHandler = eventHandler;

            var webClient = new WebClient();
            webClient.DownloadStringCompleted += DialogsGetCompleted;
            webClient.DownloadStringAsync(Requests.GetExecuteUri(Requests.DialogsGetCodeFormat.Replace("%%OFFSET%%", offset.ToString())));
        }

        public static void MessagesGet(long uid, int offset, bool isChat, VKApiOperationCompletedEventHandler eventHandler)
        {
            messagesGetCompletedEventHandler = eventHandler;

            var webClient = new WebClient();
            webClient.DownloadStringCompleted += MessagesGetCompleted;
            webClient.DownloadStringAsync(Requests.GetMessagesGetUri(uid, offset, isChat), isChat ? uid + 2000000000 : uid);
        }

        public static void CheckToken(VKApiOperationCompletedEventHandler eventHandler)
        {
            checkTokenCompletedEventHandler = eventHandler;

            var webClient = new WebClient();
            webClient.DownloadStringCompleted += CheckTokenCompleted;
            webClient.DownloadStringAsync(Requests.GetCheckTokenUri());
        }


        static void SignUpConfirmCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var success = false;
            var errorCode = 0;

            if (!e.Cancelled && e.Error == null)
            {
                errorCode = CheckForError(e.Result, SignUpConfirmCompleted).Code;
                if (errorCode == 0)
                {
                    try
                    {
                        var serializer = new DataContractJsonSerializer(typeof(JsonSignUpConfirmResponse));

                        var stream = new MemoryStream(Encoding.UTF8.GetBytes(e.Result));

                        var responce = serializer.ReadObject(stream) as JsonSignUpConfirmResponse;

                        success = responce != null && 
                                  responce.Data != null && 
                                  responce.Data.Success > 0 && 
                                  responce.Data.UID > 0;

                        stream.Close();
                    }
                    catch (Exception)
                    {
                        success = false;
                    }
                }
            }

            signUpConfirmCompletedEventHandler.Invoke(null,
                            new VKApiOperationCompletedEventArgs
                            {
                                IsSuccess = success,
                                ErrorCode = errorCode
                            });
        }

        static void SignUpCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var success = false;
            var errorCode = 0;

            if (!e.Cancelled && e.Error == null)
            {
                errorCode = CheckForError(e.Result, SignUpCompleted).Code;
                if (errorCode == 0)
                {
                    try
                    {
                        var serializer = new DataContractJsonSerializer(typeof (JsonSignUpResponce));

                        var stream = new MemoryStream(Encoding.UTF8.GetBytes(e.Result));

                        var responce = serializer.ReadObject(stream) as JsonSignUpResponce;

                        success = responce != null && !string.IsNullOrEmpty(responce.SID.SID);

                        stream.Close();
                    }
                    catch (Exception)
                    {
                        success = false;
                    }
                }
            }

            signUpCompletedEventHandler.Invoke(null,
                            new VKApiOperationCompletedEventArgs
                            {
                                IsSuccess = success,
                                ErrorCode = errorCode
                            });
        }


        static void GetLongPoolServerCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var success = false;
            var errorCode = 0;
            JsonLongPoolResponce responce = null;

            if (!e.Cancelled && e.Error == null)
            {
                errorCode = CheckForError(e.Result, GetLongPoolServerCompleted).Code;
                if (errorCode == 0)
                {
                    try
                    {
                        var serializer = new DataContractJsonSerializer(typeof(JsonLongPoolResponce));

                        var stream = new MemoryStream(Encoding.UTF8.GetBytes(e.Result));

                        responce = serializer.ReadObject(stream) as JsonLongPoolResponce;

                        success = responce != null && responce.Data != null;

                        stream.Close();
                    }
                    catch (Exception)
                    {
                        success = false;
                    }
                }
            }

            connectToLongPoolCompletedEventHandler.Invoke(null,
                                                            new VKApiOperationCompletedEventArgs
                                                                {
                                                                    IsSuccess = success,
                                                                    ErrorCode = errorCode,
                                                                    Result = success ? responce.Data : null
                                                                });
        }
        
        static void LongPoolRequestCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var success = false;
            var errorCode = 0;
            var needRefresh = false;

            JsonLongPoolUpdatesResponce responce = null;

            if (!e.Cancelled && e.Error == null)
            {
                needRefresh = !e.Result.Contains("{}");
                errorCode = CheckForError(e.Result, LongPoolRequestCompleted).Code;
                if (errorCode == 0)
                {
                    try
                    {
                        var serializer = new DataContractJsonSerializer(typeof(JsonLongPoolUpdatesResponce));

                        var stream = new MemoryStream(Encoding.UTF8.GetBytes(e.Result));

                        responce = serializer.ReadObject(stream) as JsonLongPoolUpdatesResponce;

                        success = responce != null;

                        if (responce != null)
                            responce.HasAttachments = needRefresh;

                        stream.Close();
                    }
                    catch (Exception)
                    {
                        success = false;
                    }
                }
            }

            longPoolRequestCompletedEventHandler.Invoke(null,
                                                            new VKApiOperationCompletedEventArgs
                                                                {
                                                                    IsSuccess = success,
                                                                    ErrorCode = errorCode,
                                                                    Result = success ? responce : null
                                                                });
        }

        static void CheckPhoneCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var success = false;
            var errorCode = 0;

            if (!e.Cancelled && e.Error == null)
            {
                errorCode = CheckForError(e.Result, CheckPhoneCompleted).Code;
                if (errorCode == 0)
                {
                    try
                    {
                        var serializer = new DataContractJsonSerializer(typeof (JsonCheckPhoneResponce));

                        var stream = new MemoryStream(Encoding.UTF8.GetBytes(e.Result));

                        var responce = serializer.ReadObject(stream) as JsonCheckPhoneResponce;

                        success = responce != null && responce.Responce > 0;

                        stream.Close();
                    }
                    catch (Exception)
                    {
                        success = false;
                    }
                }
            }

            chechPhoneCompletedEventHandler.Invoke(null,
                            new VKApiOperationCompletedEventArgs
                            {
                                IsSuccess = success,
                                ErrorCode = errorCode
                            });
        }

        static void RequestAccessTokenCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var success = false;
            var errorCode = 0;

            if (!e.Cancelled && e.Error == null)
            {
                errorCode = CheckForError(e.Result, RequestAccessTokenCompleted).Code;
                if (errorCode == 0)
                {
                    try
                    {
                        var serializer = new DataContractJsonSerializer(typeof (JsonAccessTokenResponce));

                        var stream = new MemoryStream(Encoding.UTF8.GetBytes(e.Result));

                        var responce = serializer.ReadObject(stream) as JsonAccessTokenResponce;

                        stream.Close();

                        if (responce != null)
                        {
                            SharedPreferences.AccessToken = responce.AccessToken;
                            SharedPreferences.UserId = responce.UserId;
                            VKApi.GetCurrentProfile(requestTokenCompletedEventHandler);
                            success = true;
                        }
                    }
                    catch (Exception)
                    {
                        success = false;
                    }
                }
            }

            if (!success)
            {
                requestTokenCompletedEventHandler.Invoke(null,
                                                         new VKApiOperationCompletedEventArgs
                                                             {
                                                                 IsSuccess = success,
                                                                 ErrorCode = errorCode
                                                             });
            }
        }

        static JsonError CheckForError(string result, DownloadStringCompletedEventHandler eventHandler)
        {
            var serializer = new DataContractJsonSerializer(typeof(JsonErrorResponce));

            var stream = new MemoryStream(Encoding.UTF8.GetBytes(result));

            var responce = serializer.ReadObject(stream) as JsonErrorResponce;

            stream.Close();

            if (responce == null || responce.Error == null)
                return new JsonError { Code = 0, Message = string.Empty };

            responce.Error.EventHandler = eventHandler;

            if (responce.Error.Code == 14)
            {
                CrossPageParameters.LastError = responce.Error;
                BaseViewModel.Navigate("CapchaPage");
            }

            return responce.Error;
        }

        static void GetFriendRequestsCountCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var success = false;
            var errorCode = 0;

            JsonSimpleUIDs responce = null;

            var fixedJson = e.Result.Replace(":false", ":[]");

            if (!e.Cancelled && e.Error == null)
            {
                errorCode = CheckForError(fixedJson, GetFriendRequestsCountCompleted).Code;
                if (errorCode == 0)
                {
                    try
                    {
                        var serializer = new DataContractJsonSerializer(typeof(JsonSimpleUIDs));

                        var stream = new MemoryStream(Encoding.UTF8.GetBytes(fixedJson));

                        responce = serializer.ReadObject(stream) as JsonSimpleUIDs;

                        stream.Close();

                        success = responce != null && responce.Result != null;
                    }
                    catch (Exception)
                    {
                        success = false;
                    }

                    //if (responce != null)
                    //{
                    //    var worker = new BackgroundWorker();
                    //    worker.DoWork += (s, _e) => DB.DB.UpdateFriends(responce, friendsGetCompletedEventHandler);
                    //    worker.RunWorkerCompleted += worker_RunWorkerCompleted;
                    //    worker.RunWorkerAsync();
                    //}
                }
            }

            friendRequestsCountCompletedEventHandler.Invoke(null,
                                                    new VKApiOperationCompletedEventArgs
                                                    {
                                                        IsSuccess = success,
                                                        ErrorCode = errorCode,
                                                        Result = success ? responce.Result.Count : 0
                                                    });
        }


        static void GetChatMessageByIdCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var success = false;
            var errorCode = 0;

            JsonMessagesGetResponce responce = null;

            var fixedJson = e.Result.Replace(":false", ":[]");

            var pos1 = fixedJson.IndexOf('[') + 1;
            if (pos1 > -1)
            {
                var pos2 = fixedJson.IndexOf('{', pos1);
                if (pos2 > -1)
                    fixedJson = fixedJson.Substring(0, pos1) + fixedJson.Substring(pos2);
            }

            if (!e.Cancelled && e.Error == null)
            {
                errorCode = CheckForError(fixedJson, GetChatMessageByIdCompleted).Code;
                if (errorCode == 0)
                {
                    try
                    {
                        var serializer = new DataContractJsonSerializer(typeof(JsonMessagesGetResponce));

                        var stream = new MemoryStream(Encoding.UTF8.GetBytes(fixedJson));

                        responce = serializer.ReadObject(stream) as JsonMessagesGetResponce;

                        stream.Close();

                        success = responce != null && responce.Messages != null && responce.Messages.Count > 0;
                    }
                    catch (Exception)
                    {
                        success = false;
                    }

                    //if (responce != null)
                    //{
                    //    var worker = new BackgroundWorker();
                    //    worker.DoWork += (s, _e) => DB.DB.UpdateFriends(responce, friendsGetCompletedEventHandler);
                    //    worker.RunWorkerCompleted += worker_RunWorkerCompleted;
                    //    worker.RunWorkerAsync();
                    //}
                }
            }

            messageGetChatMsgByIdCompletedEventHandler.Invoke(null,
                                                    new VKApiOperationCompletedEventArgs
                                                    {
                                                        IsSuccess = success,
                                                        ErrorCode = errorCode,
                                                        Result = success ? responce.Messages : null
                                                    });
        }

        static void GetMessageByIdCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var success = false;
            var errorCode = 0;

            JsonMessagesGetResponce responce = null;

            var fixedJson = e.Result.Replace(":false", ":[]");

            var pos1 = fixedJson.IndexOf('[') + 1;
            if (pos1 > -1)
            {
                var pos2 = fixedJson.IndexOf('{', pos1);
                if (pos2 > -1)
                    fixedJson = fixedJson.Substring(0, pos1) + fixedJson.Substring(pos2);
            }

            if (!e.Cancelled && e.Error == null)
            {
                errorCode = CheckForError(fixedJson, GetMessageByIdCompleted).Code;
                if (errorCode == 0)
                {
                    try
                    {
                        var serializer = new DataContractJsonSerializer(typeof(JsonMessagesGetResponce));

                        var stream = new MemoryStream(Encoding.UTF8.GetBytes(fixedJson));

                        responce = serializer.ReadObject(stream) as JsonMessagesGetResponce;

                        stream.Close();

                        success = responce != null && responce.Messages != null && responce.Messages.Count > 0;
                    }
                    catch (Exception)
                    {
                        success = false;
                    }

                    //if (responce != null)
                    //{
                    //    var worker = new BackgroundWorker();
                    //    worker.DoWork += (s, _e) => DB.DB.UpdateFriends(responce, friendsGetCompletedEventHandler);
                    //    worker.RunWorkerCompleted += worker_RunWorkerCompleted;
                    //    worker.RunWorkerAsync();
                    //}
                }
            }

            messageGetByIdCompletedEventHandler.Invoke(null,
                                                    new VKApiOperationCompletedEventArgs
                                                    {
                                                        IsSuccess = success,
                                                        ErrorCode = errorCode,
                                                        Result = success ? responce.Messages : null
                                                    });
        }

        static void GetFriendSuggestionsCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var success = false;
            var errorCode = 0;

            JsonSimpleFriendsGetResponce responce = null;

            var fixedJson = e.Result.Replace(":false", ":[]");

            if (!e.Cancelled && e.Error == null)
            {
                errorCode = CheckForError(fixedJson, GetFriendSuggestionsCompleted).Code;
                if (errorCode == 0)
                {
                    try
                    {
                        var serializer = new DataContractJsonSerializer(typeof(JsonSimpleFriendsGetResponce));

                        var stream = new MemoryStream(Encoding.UTF8.GetBytes(fixedJson));

                        responce = serializer.ReadObject(stream) as JsonSimpleFriendsGetResponce;

                        stream.Close();

                        success = responce != null && responce.Friends != null && responce.Friends.Count > 0;
                    }
                    catch (Exception)
                    {
                        success = false;
                    }

                    //if (responce != null)
                    //{
                    //    var worker = new BackgroundWorker();
                    //    worker.DoWork += (s, _e) => DB.DB.UpdateFriends(responce, friendsGetCompletedEventHandler);
                    //    worker.RunWorkerCompleted += worker_RunWorkerCompleted;
                    //    worker.RunWorkerAsync();
                    //}
                }
            }

            getFriendSuggestionsCompletedEventHandler.Invoke(null,
                                                    new VKApiOperationCompletedEventArgs
                                                    {
                                                        IsSuccess = success,
                                                        ErrorCode = errorCode,
                                                        Result = success ? responce.Friends : null
                                                    });
        }


        static void GetFriendRequestsCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var success = false;
            var errorCode = 0;
            ObservableCollection<FriendCategory> result = null;

            if (!e.Cancelled && e.Error == null)
            {
                JsonFriendRequestsResponse responce = null;

                var fixedJson = e.Result.Replace(":false", ":[]");

                errorCode = CheckForError(fixedJson, GetFriendRequestsCompleted).Code;
                if (errorCode == 0)
                {
                    try
                    {
                        var serializer = new DataContractJsonSerializer(typeof(JsonFriendRequestsResponse));

                        var stream = new MemoryStream(Encoding.UTF8.GetBytes(fixedJson));

                        responce = serializer.ReadObject(stream) as JsonFriendRequestsResponse;

                        stream.Close();

                        success = responce != null && responce.Response != null;

                        if (success)
                        {
                            CrossPageParameters.FriendRequestsCount = responce.Response.Requests.Count;

                            result = new ObservableCollection<FriendCategory>
                                         {
                                             new FriendCategory
                                                 (
                                                 new ObservableCollection<FriendItemViewModel>(
                                                     responce.Response.Requests.
                                                         Select(x => new FriendItemViewModel
                                                                         {
                                                                             VKName = x.FirstName + " " + x.LastName,
                                                                             IsOnline = x.IsOnline,
                                                                             UID = x.UID,
                                                                             Phone = x.Phone,
                                                                             MobilePhone = x.MobilePhone,
                                                                             HomePhone = x.HomePhone,
                                                                             Rate = x.Rate,
                                                                             Image50X50Url =
                                                                                 x.Photo50X50Sq != null
                                                                                     ? x.Photo50X50Sq.ToString()
                                                                                     : null,
                                                                             Image100X100Url =
                                                                                 x.Photo100X100Sq != null
                                                                                     ? x.Photo100X100Sq.ToString()
                                                                                     : null
                                                                         }))
                                                 ) {Key = Resource.FRIEND_REQUESTS + string.Format(" ({0})", CrossPageParameters.FriendRequestsCount)},
                                             new FriendCategory
                                                 (
                                                 new ObservableCollection<FriendItemViewModel>(
                                                     responce.Response.Suggestions.
                                                         Select(x => new FriendItemViewModel
                                                                         {
                                                                             VKName = x.FirstName + " " + x.LastName,
                                                                             IsOnline = x.IsOnline,
                                                                             UID = x.UID,
                                                                             Phone = x.Phone,
                                                                             MobilePhone = x.MobilePhone,
                                                                             HomePhone = x.HomePhone,
                                                                             Rate = x.Rate,
                                                                             Image50X50Url =
                                                                                 x.Photo50X50Sq != null
                                                                                     ? x.Photo50X50Sq.ToString()
                                                                                     : null,
                                                                             Image100X100Url =
                                                                                 x.Photo100X100Sq != null
                                                                                     ? x.Photo100X100Sq.ToString()
                                                                                     : null
                                                                         }))
                                                 ) {Key = Resource.FRIEND_SUGGESTIONS}
                                         };
                        }
                    }
                    catch (Exception ee)
                    {
                        success = false;
                    }
                }
            }

            getFriendRequestsCompletedEventHandler.Invoke(null,
                                                    new VKApiOperationCompletedEventArgs
                                                    {
                                                        IsSuccess = success,
                                                        ErrorCode = errorCode,
                                                        Result = result
                                                    });
        }

        static void FriendsGetCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var success = false;
            var errorCode = 0;

            if (!e.Cancelled && e.Error == null)
            {
                JsonSimpleFriendsGetResponce responce = null;

                errorCode = CheckForError(e.Result, FriendsGetCompleted).Code;
                if (errorCode == 0)
                {
                    try
                    {
                        var serializer = new DataContractJsonSerializer(typeof(JsonSimpleFriendsGetResponce));

                        var stream = new MemoryStream(Encoding.UTF8.GetBytes(e.Result));

                        responce = serializer.ReadObject(stream) as JsonSimpleFriendsGetResponce;

                        stream.Close();

                        success = responce != null;
                    }
                    catch (Exception)
                    {
                        success = false;
                    }

                    if (responce != null)
                    {
                        var worker = new BackgroundWorker();
                        worker.DoWork += (s, _e) => DB.DB.UpdateFriends(responce, friendsGetCompletedEventHandler);
                        worker.RunWorkerCompleted += worker_RunWorkerCompleted;
                        worker.RunWorkerAsync();
                    }
                }
            }

            if (!success)
            {
                friendsGetCompletedEventHandler.Invoke(null,
                                                       new VKApiOperationCompletedEventArgs
                                                       {
                                                           IsSuccess = success,
                                                           ErrorCode = errorCode
                                                       });
            }
        }

        static void FriendsAndDialogsGetCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var success = false;
            var errorCode = 0;

            if (!e.Cancelled && e.Error == null)
            {
                JsonAllFriendsGetResponce responce = null;

                errorCode = CheckForError(e.Result, FriendsAndDialogsGetCompleted).Code;
                if (errorCode == 0)
                {
                    try
                    {
                        var serializer = new DataContractJsonSerializer(typeof (JsonAllFriendsGetResponce));

                        var stream = new MemoryStream(Encoding.UTF8.GetBytes(e.Result));

                        responce = serializer.ReadObject(stream) as JsonAllFriendsGetResponce;

                        if (responce != null && responce.AllFriends != null)
                        {
                            if (responce.AllFriends.DialogsUIDs != null && responce.AllFriends.DialogsUIDs.Count > 0)
                                responce.AllFriends.DialogsUIDs.RemoveAt(0);

                            if (responce.AllFriends.Bodies != null && responce.AllFriends.Bodies.Count > 0)
                                responce.AllFriends.Bodies.RemoveAt(0);

                            if (responce.AllFriends.Dates != null && responce.AllFriends.Dates.Count > 0)
                                responce.AllFriends.Dates.RemoveAt(0);

                            if (responce.AllFriends.ReadState != null && responce.AllFriends.ReadState.Count > 0)
                                responce.AllFriends.ReadState.RemoveAt(0);

                            if (responce.AllFriends.Out != null && responce.AllFriends.Out.Count > 0)
                                responce.AllFriends.Out.RemoveAt(0);

                            if (responce.AllFriends.Titles != null && responce.AllFriends.Titles.Count > 0)
                                responce.AllFriends.Titles.RemoveAt(0);

                            if (responce.AllFriends.ChatIds != null && responce.AllFriends.ChatIds.Count > 0)
                                responce.AllFriends.ChatIds.RemoveAt(0);

                            if (responce.AllFriends.ActiveChatUIds != null && responce.AllFriends.ActiveChatUIds.Count > 0)
                                responce.AllFriends.ActiveChatUIds.RemoveAt(0);
                        }

                        stream.Close();

                        success = responce != null && responce.AllFriends != null;

                        if (success)
                        {
                            CrossPageParameters.FirstFiveUids.Clear();

                            foreach (var rateFriend in responce.AllFriends.RateFriends)
                            {
                                CrossPageParameters.FirstFiveUids.Add(rateFriend.UID);
                            }
                        }
                    }
                    catch (Exception)
                    {
                        success = false;
                    }

                    if (responce != null)
                    {
                        var worker = new BackgroundWorker();
                        worker.DoWork += (s, _e) => 
                        {
                            DB.DB.UpdateFriends(responce, null);
                            DB.DB.UpdateDialogs(responce, friendsAndDialogsGetCompletedEventHandler);
                        };
                        worker.RunWorkerCompleted += worker_RunWorkerCompleted;
                        worker.RunWorkerAsync();
                    }
                }
            }

            if (!success)
            {
                friendsAndDialogsGetCompletedEventHandler.Invoke(null,
                                                       new VKApiOperationCompletedEventArgs
                                                           {
                                                               IsSuccess = success,
                                                               ErrorCode = errorCode
                                                           });
            }
        }


        static void GetByPhonesCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var success = false;
            var errorCode = 0;

            if (!e.Cancelled && e.Error == null)
            {
                JsonSimpleFriendsGetResponce responce = null;

                errorCode = CheckForError(e.Result, GetByPhonesCompleted).Code;
                if (errorCode == 0)
                {
                    try
                    {
                        var serializer = new DataContractJsonSerializer(typeof(JsonSimpleFriendsGetResponce));

                        var stream = new MemoryStream(Encoding.UTF8.GetBytes(e.Result));

                        responce = serializer.ReadObject(stream) as JsonSimpleFriendsGetResponce;

                        stream.Close();

                        success = responce != null;
                    }
                    catch (Exception)
                    {
                        success = false;
                    }

                    var contacts = e.UserState as List<Contact>;
                    if (responce != null && contacts != null)
                    {
                        var worker = new BackgroundWorker();
                        worker.DoWork += (s, _e) => DB.DB.UpdateContacts(responce, contacts, getByPhonesCompletedEventHandler);
                        worker.RunWorkerCompleted += worker_RunWorkerCompleted;
                        worker.RunWorkerAsync();
                    }
                }
            }

            if (!success)
            {
                getByPhonesCompletedEventHandler.Invoke(null,
                                                       new VKApiOperationCompletedEventArgs
                                                           {
                                                               IsSuccess = success,
                                                               ErrorCode = errorCode
                                                           });
            }
        }

        static void MessageSendCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var success = false;
            var errorCode = 0;

            JsonMessageSendResponce responce = null;

            DBMessage msgObj = null;
            if (!e.Cancelled && e.Error == null)
            {
                errorCode = CheckForError(e.Result, MessageSendCompleted).Code;
                if (errorCode == 0)
                {
                    try
                    {
                        var serializer = new DataContractJsonSerializer(typeof(JsonMessageSendResponce));

                        var stream = new MemoryStream(Encoding.UTF8.GetBytes(e.Result));

                        responce = serializer.ReadObject(stream) as JsonMessageSendResponce;

                        stream.Close();

                        success = responce != null;
                    }
                    catch (Exception ee)
                    {
                        success = false;
                    }

                    msgObj = e.UserState as DBMessage;                    
                    if (responce != null)
                    {
                        if (msgObj != null)
                        {
                            var worker = new BackgroundWorker();
                            msgObj.MID = responce.MID;
                            worker.DoWork += (s, _e) => DB.DB.InsertMessage(msgObj, responce);
                            worker.RunWorkerCompleted += worker_RunWorkerCompleted;
                            worker.RunWorkerAsync();
                        }
                        else
                        {
                            var paramsAsync = e.UserState as ByRequestParamsAsync;
                            if (paramsAsync != null)
                            {
                                var ps = paramsAsync.Params;
                                if (ps != null)
                                {
                                    long uid = -1;
                                    var message = string.Empty;
                                    foreach (var p in ps)
                                    {
                                        if (p.Key == "uid")
                                            uid = Convert.ToInt64(p.Value);
                                        if (p.Key == "message")
                                            message = Convert.ToString(p.Value);

                                        if (uid != -1 && message != string.Empty)
                                        {
                                            msgObj = new DBMessage
                                                         {
                                                             ToId = uid,
                                                             Body = message,
                                                             Out = 1,
                                                             ReadState = 1,
                                                             Date =
                                                                 (long)
                                                                 (DateTime.UtcNow - new DateTime(1970, 1, 1)).
                                                                     TotalSeconds
                                                         };

                                            var worker = new BackgroundWorker();
                                            msgObj.MID = responce.MID;
                                            worker.DoWork += (s, _e) => DB.DB.InsertMessage(msgObj, responce);
                                            worker.RunWorkerCompleted += worker_RunWorkerCompleted;
                                            worker.RunWorkerAsync();

                                            if (paramsAsync.EventHandler != null)
                                                    paramsAsync.EventHandler.Invoke(null,
                                                        new VKApiOperationCompletedEventArgs
                                                        {
                                                            IsSuccess = success,
                                                            ErrorCode = errorCode,
                                                            Result = success ? new MessageItemViewModel
                                                            {
                                                                Body = msgObj.Body,
                                                                Checked = false,
                                                                Date = (new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc) + TimeSpan.FromSeconds(msgObj.Date)).ToLocalTime(),
                                                                MID = msgObj.MID,
                                                                Out = msgObj.Out != 0,
                                                                ReadState = msgObj.ReadState != 0
                                                            } : null
                                                        });
                                        }
                                    }
                                }
                            }
                        }
                    }

                    
                }
            }

            messageSendCompletedEventHandler.Invoke(null,
                                                    new VKApiOperationCompletedEventArgs
                                                        {
                                                            IsSuccess = success,
                                                            ErrorCode = errorCode,
                                                            Result = success ? new MessageItemViewModel
                                                                         {
                                                                             Body = msgObj.Body,
                                                                             Checked = false,
                                                                             Date = (new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc) + TimeSpan.FromSeconds(msgObj.Date)).ToLocalTime(),
                                                                             MID = msgObj.MID,
                                                                             Out = msgObj.Out != 0,
                                                                             ReadState = msgObj.ReadState != 0
                                                                         } : null
                                                        });
        }

        static void DialogsGetCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var success = false;
            var errorCode = 0;

            if (!e.Cancelled && e.Error == null)
            {
                JsonDialogsGetResponce responce = null;

                errorCode = CheckForError(e.Result, DialogsGetCompleted).Code;
                if (errorCode == 0)
                {
                    try
                    {
                        var serializer = new DataContractJsonSerializer(typeof(JsonDialogsGetResponce));

                        var stream = new MemoryStream(Encoding.UTF8.GetBytes(e.Result));

                        responce = serializer.ReadObject(stream) as JsonDialogsGetResponce;

                        if (responce != null && responce.Dialogs != null)
                        {
                            if (responce.Dialogs.DialogsUIDs != null && responce.Dialogs.DialogsUIDs.Count > 0)
                                responce.Dialogs.DialogsUIDs.RemoveAt(0);

                            if (responce.Dialogs.Bodies != null && responce.Dialogs.Bodies.Count > 0)
                                responce.Dialogs.Bodies.RemoveAt(0);

                            if (responce.Dialogs.Dates != null && responce.Dialogs.Dates.Count > 0)
                                responce.Dialogs.Dates.RemoveAt(0);

                            if (responce.Dialogs.ReadState != null && responce.Dialogs.ReadState.Count > 0)
                                responce.Dialogs.ReadState.RemoveAt(0);

                            if (responce.Dialogs.Out != null && responce.Dialogs.Out.Count > 0)
                                responce.Dialogs.Out.RemoveAt(0);

                            if (responce.Dialogs.Titles != null && responce.Dialogs.Titles.Count > 0)
                                responce.Dialogs.Titles.RemoveAt(0);

                            if (responce.Dialogs.ChatIds != null && responce.Dialogs.ChatIds.Count > 0)
                                responce.Dialogs.ChatIds.RemoveAt(0);

                            if (responce.Dialogs.ActiveChatUIds != null && responce.Dialogs.ActiveChatUIds.Count > 0)
                                responce.Dialogs.ActiveChatUIds.RemoveAt(0);
                        }

                        stream.Close();

                        success = responce != null;
                    }
                    catch (Exception ee)
                    {
                        success = false;
                    }

                    if (responce != null)
                    {
                        var worker = new BackgroundWorker();
                        worker.DoWork += (s, _e) => DB.DB.UpdateDialogs(responce, dialogsGetCompletedEventHandler);
                        worker.RunWorkerCompleted += worker_RunWorkerCompleted;
                        worker.RunWorkerAsync();
                    }
                }
            }

            if (!success)
            {
                dialogsGetCompletedEventHandler.Invoke(null,
                                                       new VKApiOperationCompletedEventArgs
                                                           {
                                                               IsSuccess = success,
                                                               ErrorCode = errorCode
                                                           });
            }
        }


        static void EditChatCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var success = false;
            var errorCode = 0;
            JsonCreateChatResponse responce = null;

            if (!e.Cancelled && e.Error == null)
            {
                errorCode = CheckForError(e.Result, EditChatCompleted).Code;
                if (errorCode == 0 && !e.Result.Contains("false"))
                {
                    try
                    {
                        var serializer = new DataContractJsonSerializer(typeof(JsonCreateChatResponse));

                        var stream = new MemoryStream(Encoding.UTF8.GetBytes(e.Result));

                        responce = serializer.ReadObject(stream) as JsonCreateChatResponse;

                        stream.Close();

                        success = responce != null;
                    }
                    catch (Exception ee)
                    {
                        success = false;
                    }
                }
            }

            editChatCompletedEventHandler.Invoke(null,
                                                    new VKApiOperationCompletedEventArgs
                                                        {
                                                            IsSuccess = success,
                                                            ErrorCode = errorCode,
                                                            Result = success ? responce.ChatId : 0
                                                        });
        }

        static void CreateChatCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var success = false;
            var errorCode = 0;
            JsonCreateChatResponse responce = null;

            if (!e.Cancelled && e.Error == null)
            {
                errorCode = CheckForError(e.Result, CreateChatCompleted).Code;
                if (errorCode == 0)
                {
                    try
                    {
                        var serializer = new DataContractJsonSerializer(typeof(JsonCreateChatResponse));

                        var stream = new MemoryStream(Encoding.UTF8.GetBytes(e.Result));

                        responce = serializer.ReadObject(stream) as JsonCreateChatResponse;

                        stream.Close();

                        success = responce != null;
                    }
                    catch (Exception ee)
                    {
                        success = false;
                    }
                }
            }

            createChatCompletedEventHandler.Invoke(null,
                                                    new VKApiOperationCompletedEventArgs
                                                        {
                                                            IsSuccess = success,
                                                            ErrorCode = errorCode,
                                                            Result = success ? responce.ChatId : 0
                                                        });
        }

        static void GetUnreadCountCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var success = false;
            var errorCode = 0;
            JsonUnreadMessagesCountResponce responce = null;

            if (!e.Cancelled && e.Error == null)
            {
                errorCode = CheckForError(e.Result, GetUnreadCountCompleted).Code;
                if (errorCode == 0)
                {
                    try
                    {
                        var serializer = new DataContractJsonSerializer(typeof(JsonUnreadMessagesCountResponce));

                        var stream = new MemoryStream(Encoding.UTF8.GetBytes(e.Result));

                        responce = serializer.ReadObject(stream) as JsonUnreadMessagesCountResponce;

                        stream.Close();

                        success = responce != null;
                    }
                    catch (Exception ee)
                    {
                        success = false;
                    }
                }
            }

            unreadCountCompletedEventHandler.Invoke(null,
                                                    new VKApiOperationCompletedEventArgs
                                                        {
                                                            IsSuccess = success,
                                                            ErrorCode = errorCode,
                                                            Result = success ? responce.UnreadCount : 0
                                                        });
        }

        static void MessagesGetCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var success = false;
            var errorCode = 0;

            if (!e.Cancelled && e.Error == null)
            {
                JsonMessagesGetResponce responce = null;

                errorCode = CheckForError(e.Result, MessagesGetCompleted).Code;
                if (errorCode == 0)
                {
                    try
                    {
                        var serializer = new DataContractJsonSerializer(typeof(JsonMessagesGetResponce));

                        var fixedJson = e.Result;

                        if (fixedJson.Contains("response") && !fixedJson.Contains(","))
                        {
                            messagesGetCompletedEventHandler.Invoke(null,
                                                       new VKApiOperationCompletedEventArgs
                                                       {
                                                           IsSuccess = true,
                                                           ErrorCode = errorCode
                                                       });
                            return;
                        }

                        var pos1 = e.Result.IndexOf('[') + 1;
                        if (pos1 > -1)
                        {
                            var pos2 = e.Result.IndexOf('{', pos1);
                            if (pos2 > -1)
                                fixedJson = e.Result.Substring(0, pos1) + e.Result.Substring(pos2);
                        }

                        var stream = new MemoryStream(Encoding.UTF8.GetBytes(fixedJson));

                        responce = serializer.ReadObject(stream) as JsonMessagesGetResponce;

                        if (responce != null && responce.Messages != null && responce.Messages.Count > 0)
                        {
                            MakeAsRead(responce.Messages.Select(x => x.MID));
                        }

                        stream.Close();

                        success = responce != null && responce.Messages != null;
                    }
                    catch (Exception ee)
                    {
                        success = false;
                    }

                    if (responce != null)
                    {
                        var worker = new BackgroundWorker();
                        worker.DoWork += (s, _e) => DB.DB.UpdateMessages((long)e.UserState, responce, messagesGetCompletedEventHandler);
                        worker.RunWorkerCompleted += worker_RunWorkerCompleted;
                        worker.RunWorkerAsync();
                    }
                }
            }

            if (!success)
            {
                messagesGetCompletedEventHandler.Invoke(null,
                                                       new VKApiOperationCompletedEventArgs
                                                           {
                                                               IsSuccess = success,
                                                               ErrorCode = errorCode
                                                           });
            }
        }

        static void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //throw new NotImplementedException();
        }

        static void CheckTokenCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var success = false;
            var errorCode = 0;

            if (!e.Cancelled && e.Error == null)
            {
                errorCode = CheckForError(e.Result, CheckTokenCompleted).Code;
                if (errorCode == 0)
                {
                    try
                    {
                        var serializer = new DataContractJsonSerializer(typeof(JsonSimpleFriendsGetResponce));

                        var stream = new MemoryStream(Encoding.UTF8.GetBytes(e.Result));

                        var responce = serializer.ReadObject(stream) as JsonSimpleFriendsGetResponce;

                        stream.Close();

                        success = responce != null;
                    }
                    catch (Exception)
                    {
                        success = false;
                    }
                }
            }

            checkTokenCompletedEventHandler.Invoke(null,
                            new VKApiOperationCompletedEventArgs
                            {
                                IsSuccess = success,
                                ErrorCode = errorCode
                            });
        }


        static void GetPushSettingsCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var success = false;
            var errorCode = 0;

            JsonGetPushSettingsResponse responce = null;

            if (!e.Cancelled && e.Error == null)
            {
                errorCode = CheckForError(e.Result, GetPushSettingsCompleted).Code;
                if (errorCode == 0)
                {
                    try
                    {
                        var serializer = new DataContractJsonSerializer(typeof(JsonGetPushSettingsResponse));

                        var stream = new MemoryStream(Encoding.UTF8.GetBytes(e.Result));

                        responce = serializer.ReadObject(stream) as JsonGetPushSettingsResponse;

                        stream.Close();

                        success = responce != null && responce.Settings != null;
                    }
                    catch (Exception)
                    {
                        success = false;
                    }
                }
            }

            if (getPushSettingsCompletedEventHandler != null)
                getPushSettingsCompletedEventHandler.Invoke(null,
                                new VKApiOperationCompletedEventArgs
                                {
                                    IsSuccess = success,
                                    ErrorCode = errorCode,
                                    Result = success ? responce.Settings : null
                                });
        }


        static void GetCurrentProfileCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var success = false;
            var errorCode = 0;

            JsonSimpleFriendsGetResponce responce = null;

            if (!e.Cancelled && e.Error == null)
            {
                errorCode = CheckForError(e.Result, GetCurrentProfileCompleted).Code;
                if (errorCode == 0)
                {
                    try
                    {
                        var serializer = new DataContractJsonSerializer(typeof(JsonSimpleFriendsGetResponce));

                        var stream = new MemoryStream(Encoding.UTF8.GetBytes(e.Result));

                        responce = serializer.ReadObject(stream) as JsonSimpleFriendsGetResponce;
                        

                        stream.Close();

                        success = responce != null && responce.Friends != null && responce.Friends.Count == 1;

                        if (success)
                        {
                            SharedPreferences.UserName = responce.Friends[0].FirstName + " " +
                                                         responce.Friends[0].LastName;
                            SharedPreferences.UserPhotoUrl = responce.Friends[0].Photo100X100Sq.ToString();
                        }
                    }
                    catch (Exception)
                    {
                        success = false;
                    }
                }
            }

            if (getCurrentProfileCompletedEventHandler != null)
                getCurrentProfileCompletedEventHandler.Invoke(null,
                                new VKApiOperationCompletedEventArgs
                                {
                                    IsSuccess = success,
                                    ErrorCode = errorCode,
                                    Result = success ? responce.Friends : null
                                });
        }

        static void SaveMessagePhotoCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var success = false;
            var errorCode = 0;

            JsonSaveMessagePhotoResponse responce = null;

            if (!e.Cancelled && e.Error == null)
            {
                errorCode = CheckForError(e.Result, SaveMessagePhotoCompleted).Code;
                if (errorCode == 0)
                {
                    try
                    {
                        var serializer = new DataContractJsonSerializer(typeof(JsonSaveMessagePhotoResponse));

                        var stream = new MemoryStream(Encoding.UTF8.GetBytes(e.Result));

                        responce = serializer.ReadObject(stream) as JsonSaveMessagePhotoResponse;

                        stream.Close();

                        success = responce != null && responce.PhotoData != null;
                    }
                    catch (Exception)
                    {
                        success = false;
                    }
                }
            }

            if (saveMessagePhotoCompletedEventHandler != null)
                saveMessagePhotoCompletedEventHandler.Invoke(null,
                                new VKApiOperationCompletedEventArgs
                                {
                                    IsSuccess = success,
                                    ErrorCode = errorCode,
                                    Result = success ? responce.PhotoData : null
                                });
        }

        static void SaveProfilePhotoCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var success = false;
            var errorCode = 0;

            JsonSavePhotoResponse responce = null;

            if (!e.Cancelled && e.Error == null)
            {
                errorCode = CheckForError(e.Result, SaveProfilePhotoCompleted).Code;
                if (errorCode == 0)
                {
                    try
                    {
                        var serializer = new DataContractJsonSerializer(typeof(JsonSavePhotoResponse));

                        var stream = new MemoryStream(Encoding.UTF8.GetBytes(e.Result));

                        responce = serializer.ReadObject(stream) as JsonSavePhotoResponse;

                        stream.Close();

                        success = responce != null && responce.PhotoData != null;
                    }
                    catch (Exception)
                    {
                        success = false;
                    }
                }
            }

            if (saveProfilePhotoCompletedEventHandler != null)
                saveProfilePhotoCompletedEventHandler.Invoke(null,
                                new VKApiOperationCompletedEventArgs
                                {
                                    IsSuccess = success,
                                    ErrorCode = errorCode,
                                    Result = success ? responce.PhotoData : null
                                });
        }


        static void GetMessagesUploadServerCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var success = false;
            var errorCode = 0;

            JsonGetProfileUploadServerResponse responce = null;

            if (!e.Cancelled && e.Error == null)
            {
                errorCode = CheckForError(e.Result, GetMessagesUploadServerCompleted).Code;
                if (errorCode == 0)
                {
                    try
                    {
                        var serializer = new DataContractJsonSerializer(typeof(JsonGetProfileUploadServerResponse));

                        var stream = new MemoryStream(Encoding.UTF8.GetBytes(e.Result));

                        responce = serializer.ReadObject(stream) as JsonGetProfileUploadServerResponse;

                        stream.Close();

                        success = responce != null && responce.Server != null;
                    }
                    catch (Exception)
                    {
                        success = false;
                    }
                }
            }

            if (getMessagesUploadServerEventHandler != null)
                getMessagesUploadServerEventHandler.Invoke(null,
                                new VKApiOperationCompletedEventArgs
                                {
                                    IsSuccess = success,
                                    ErrorCode = errorCode,
                                    Result = success ? responce.Server : null
                                });
        }

        static void GetProfileUploadServerCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var success = false;
            var errorCode = 0;

            JsonGetProfileUploadServerResponse responce = null;

            if (!e.Cancelled && e.Error == null)
            {
                errorCode = CheckForError(e.Result, GetProfileUploadServerCompleted).Code;
                if (errorCode == 0)
                {
                    try
                    {
                        var serializer = new DataContractJsonSerializer(typeof(JsonGetProfileUploadServerResponse));

                        var stream = new MemoryStream(Encoding.UTF8.GetBytes(e.Result));

                        responce = serializer.ReadObject(stream) as JsonGetProfileUploadServerResponse;

                        stream.Close();

                        success = responce != null && responce.Server != null;
                    }
                    catch (Exception)
                    {
                        success = false;
                    }
                }
            }

            if (getProfileUploadServerEventHandler != null)
                getProfileUploadServerEventHandler.Invoke(null,
                                new VKApiOperationCompletedEventArgs
                                {
                                    IsSuccess = success,
                                    ErrorCode = errorCode,
                                    Result = success ? responce.Server : null
                                });
        }

        static void FriendAddCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var success = false;
            var errorCode = 0;

            if (!e.Cancelled && e.Error == null)
            {
                errorCode = CheckForError(e.Result, FriendAddCompleted).Code;
                if (errorCode == 0)
                {
                    try
                    {
                        var serializer = new DataContractJsonSerializer(typeof(JsonSimpleResponse));

                        var stream = new MemoryStream(Encoding.UTF8.GetBytes(e.Result));

                        var responce = serializer.ReadObject(stream) as JsonSimpleResponse;

                        stream.Close();

                        success = responce != null && responce.Result == 1;
                    }
                    catch (Exception)
                    {
                        success = false;
                    }
                }
            }

            if (friendAddCompletedEventHandler != null)
                friendAddCompletedEventHandler.Invoke(null,
                                new VKApiOperationCompletedEventArgs
                                {
                                    IsSuccess = success,
                                    ErrorCode = errorCode
                                });
        }

        static void FriendDeleteCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var success = false;
            var errorCode = 0;

            if (!e.Cancelled && e.Error == null)
            {
                errorCode = CheckForError(e.Result, FriendDeleteCompleted).Code;
                if (errorCode == 0)
                {
                    try
                    {
                        var serializer = new DataContractJsonSerializer(typeof(JsonSimpleResponse));

                        var stream = new MemoryStream(Encoding.UTF8.GetBytes(e.Result));

                        var responce = serializer.ReadObject(stream) as JsonSimpleResponse;

                        stream.Close();

                        success = responce != null && responce.Result == 1;
                    }
                    catch (Exception)
                    {
                        success = false;
                    }
                }
            }

            if (friendDeleteCompletedEventHandler != null)
                friendDeleteCompletedEventHandler.Invoke(null,
                                new VKApiOperationCompletedEventArgs
                                {
                                    IsSuccess = success,
                                    ErrorCode = errorCode
                                });
        }

        static void RegisterDeviceCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var success = false;
            var errorCode = 0;

            if (!e.Cancelled && e.Error == null)
            {
                errorCode = CheckForError(e.Result, RegisterDeviceCompleted).Code;
                if (errorCode == 0)
                {
                    try
                    {
                        var serializer = new DataContractJsonSerializer(typeof(JsonSimpleResponse));

                        var stream = new MemoryStream(Encoding.UTF8.GetBytes(e.Result));

                        var responce = serializer.ReadObject(stream) as JsonSimpleResponse;

                        stream.Close();

                        success = responce != null && responce.Result == 1;
                    }
                    catch (Exception)
                    {
                        success = false;
                    }
                }
            }

            if (registerDeviceCompletedEventHandler != null)
                registerDeviceCompletedEventHandler.Invoke(null,
                                new VKApiOperationCompletedEventArgs
                                {
                                    IsSuccess = success,
                                    ErrorCode = errorCode
                                });
        }

        static void UnregisterDeviceCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var success = false;
            var errorCode = 0;

            if (!e.Cancelled && e.Error == null)
            {
                errorCode = CheckForError(e.Result, UnregisterDeviceCompleted).Code;
                if (errorCode == 0)
                {
                    try
                    {
                        var serializer = new DataContractJsonSerializer(typeof(JsonSimpleResponse));

                        var stream = new MemoryStream(Encoding.UTF8.GetBytes(e.Result));

                        var responce = serializer.ReadObject(stream) as JsonSimpleResponse;

                        stream.Close();

                        success = responce != null && responce.Result == 1;
                    }
                    catch (Exception)
                    {
                        success = false;
                    }
                }
            }

            if (unregisterDeviceCompletedEventHandler != null)
                unregisterDeviceCompletedEventHandler.Invoke(null,
                                new VKApiOperationCompletedEventArgs
                                {
                                    IsSuccess = success,
                                    ErrorCode = errorCode
                                });
        }


        static void SetSilenceModeCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var success = false;
            var errorCode = 0;

            if (!e.Cancelled && e.Error == null)
            {
                errorCode = CheckForError(e.Result, SetSilenceModeCompleted).Code;
                if (errorCode == 0)
                {
                    try
                    {
                        var serializer = new DataContractJsonSerializer(typeof(JsonSimpleResponse));

                        var stream = new MemoryStream(Encoding.UTF8.GetBytes(e.Result));

                        var responce = serializer.ReadObject(stream) as JsonSimpleResponse;

                        stream.Close();

                        success = responce != null && responce.Result == 1;
                    }
                    catch (Exception)
                    {
                        success = false;
                    }
                }
            }

            if (setSilenceModeCompletedEventHandler != null)
                setSilenceModeCompletedEventHandler.Invoke(null,
                                new VKApiOperationCompletedEventArgs
                                {
                                    IsSuccess = success,
                                    ErrorCode = errorCode
                                });
        }
    }
}
