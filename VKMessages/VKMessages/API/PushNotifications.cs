﻿using System.Windows;
using Microsoft.Phone.Notification;
using VKMessages.Tools;

namespace VKMessages.API
{
    public static class PushNotifications
    {
        public const string ChannelName = "VKMessages";

        private static string channelUrl;

        private static HttpNotificationChannel _channel;

        public static void Enable()
        {
            _channel = HttpNotificationChannel.Find(ChannelName);
            if (_channel == null)
            {
                _channel = new HttpNotificationChannel(ChannelName);
                _channel.Open();

                if (!_channel.IsShellToastBound)
                    _channel.BindToShellToast();

            }
            else
            {
                channelUrl = _channel.ChannelUri.ToString();

                if (!_channel.IsShellToastBound)
                    _channel.BindToShellToast();

                VKApi.RegisterDevice(channelUrl, EnableComplited);
            }

            _channel.ShellToastNotificationReceived += ChannelToastNotificationReceived;
            _channel.ChannelUriUpdated += ChannelUriUpdated;
            _channel.ErrorOccurred += ChannelErrorOccurred;
        }

        public static void Disable()
        {
            if (_channel != null)
            {
                channelUrl = _channel.ChannelUri.ToString();

                if (_channel.IsShellToastBound)
                    _channel.UnbindToShellToast();

                VKApi.UnregisterDevice(channelUrl, DisableComplited);
            }
        }

        private static void ChannelToastNotificationReceived(object sender, NotificationEventArgs e)
        {
        }

        private static void ChannelUriUpdated(object sender, NotificationChannelUriEventArgs e)
        {
            channelUrl = e.ChannelUri.ToString();

            VKApi.RegisterDevice(channelUrl, EnableComplited);
        }

        private static void ChannelErrorOccurred(object sender, NotificationChannelErrorEventArgs e)
        {
        }

        private static void EnableComplited(object sender, VKApiOperationCompletedEventArgs e)
        {
            if (e.Cancelled || !e.IsSuccess)
            {
                SharedPreferences.IsPushOn = false;
            }
        }

        private static void DisableComplited(object sender, VKApiOperationCompletedEventArgs e)
        {
            if (e.Cancelled || !e.IsSuccess)
            {
                SharedPreferences.IsPushOn = false;
            }
        }

        public static void SetSilenceModeForHour(VKApi.VKApiOperationCompletedEventHandler eventHandler)
        {
            VKApi.SetSilenceMode(channelUrl, 60 * 60, eventHandler);
        }

        public static void SetSilenceModeFor8Hours(VKApi.VKApiOperationCompletedEventHandler eventHandler)
        {
            VKApi.SetSilenceMode(channelUrl, 8 * 60 * 60, eventHandler);
        }

        public static void DisableSilenceMode(VKApi.VKApiOperationCompletedEventHandler eventHandler)
        {
            VKApi.SetSilenceMode(channelUrl, 0, eventHandler);
        }

        public static void GetPushSettings(VKApi.VKApiOperationCompletedEventHandler eventHandler)
        {
            VKApi.GetPushSettings(channelUrl, eventHandler);
        }
    }
}
