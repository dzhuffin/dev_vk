﻿using System;
using System.Runtime.Serialization;

namespace VKMessages.API.JSON
{
    public enum FriendSex
    {
        Unknown = 0,
        Male = 2,
        Female = 1
    }

    [DataContract]
    public class JsonFriend
    {
        [DataMember(Name = "uid")]
        public long UID { get; set; }

        [DataMember(Name = "first_name")]
        public string FirstName { get; set; }

        [DataMember(Name = "last_name")]
        public string LastName { get; set; }

        [DataMember(Name = "rate")]
        public long Rate { get; set; }

        [DataMember(Name = "sex")]
        public FriendSex Sex { get; set; }

        [DataMember(Name = "bdate")]
        public string Birthday { get; set; }

        [DataMember(Name = "city")]
        public string City { get; set; }

        [DataMember(Name = "country")]
        public string Country { get; set; }

        [DataMember(Name = "photo")]
        public Uri Photo50X50 { get; set; }

        [DataMember(Name = "photo_medium")]
        public Uri Photo100X100 { get; set; }

        [DataMember(Name = "photo_medium_rec")]
        public Uri Photo100X100Sq { get; set; }

        [DataMember(Name = "photo_big")]
        public Uri Photo200X200 { get; set; }

        [DataMember(Name = "photo_rec")]
        public Uri Photo50X50Sq { get; set; }

        [DataMember(Name = "online")]
        public bool IsOnline { get; set; }

        [DataMember(Name = "has_mobile")]
        public bool HasMobile { get; set; }

        [DataMember(Name = "phone")]
        public string Phone { get; set; }

        [DataMember(Name = "mobile_phone")]
        public string MobilePhone { get; set; }

        [DataMember(Name = "home_phone")]
        public string HomePhone { get; set; }

        [DataMember(Name = "activity")]
        public string Status { get; set; }

        [DataMember(Name = "can_write_private_message")]
        public bool CanWritePrivateMessage { get; set; }
    }
}
