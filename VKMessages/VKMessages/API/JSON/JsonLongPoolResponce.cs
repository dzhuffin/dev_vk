﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace VKMessages.API.JSON
{
    [DataContract]
    public class JsonLongPoolData
    {
        [DataMember(Name = "key")]
        public string Key { get; set; }

        [DataMember(Name = "server")]
        public string Server { get; set; }

        [DataMember(Name = "ts")]
        public long Ts { get; set; }
    }

    [DataContract]
    public class JsonLongPoolResponce
    {
        [DataMember(Name = "response")]
        public JsonLongPoolData Data { get; set; }
    }

    public enum JsonUpdateType : int
    {
        MessageDeleted = 0,
        NewMessageRecieved = 4,
        FriendOnline = 8,
        FriendOffline = 9
    }

    [Flags]
    public enum JsonMessageFlagsType : long
    {
        Unread     = 1,
        Outbox     = 2,
        Replied    = 4,
        Important  = 8,
        Chat       = 16,
        Friends    = 32,
        Spam       = 64,
        Delеtеd    = 128,
        Fixed      = 256,
        Media      = 512
    }

    [DataContract]
    public class JsonLongPoolUpdatesResponce
    {
        [DataMember(Name = "ts")]
        public long Ts { get; set; }

        [DataMember(Name = "updates")]
        public List<List<object>> Updates { get; set; }

        public bool HasAttachments { get; set; }
    }
}
