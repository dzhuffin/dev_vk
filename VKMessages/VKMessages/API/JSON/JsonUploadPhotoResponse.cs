﻿using System.Runtime.Serialization;

namespace VKMessages.API.JSON
{
    [DataContract]
    public class JsonUploadPhotoResponse
    {
        [DataMember(Name = "server")]
        public long Server { get; set; }

        [DataMember(Name = "mid")]
        public long MID { get; set; }

        [DataMember(Name = "hash")]
        public string Hash { get; set; }

        [DataMember(Name = "photo")]
        public string Photo { get; set; }

        [DataMember(Name = "message_code")]
        public long MessageCode { get; set; }

        [DataMember(Name = "profile_aid")]
        public long ProfileAid { get; set; }
    }
}
