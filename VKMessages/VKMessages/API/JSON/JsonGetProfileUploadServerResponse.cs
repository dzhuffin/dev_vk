﻿using System.Runtime.Serialization;

namespace VKMessages.API.JSON
{
    [DataContract]
    public class JsonProfileUploadServer
    {
        [DataMember(Name = "upload_url")]
        public string Server { get; set; }
    }

    [DataContract]
    public class JsonGetProfileUploadServerResponse
    {
        [DataMember(Name = "response")]
        public JsonProfileUploadServer Server { get; set; }
    }
}
