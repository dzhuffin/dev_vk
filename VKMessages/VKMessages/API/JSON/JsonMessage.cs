﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace VKMessages.API.JSON
{
    [DataContract]
    public class JsonMessageAttachment
    {
        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "photo")]
        public JsonMessagePhoto Photo { get; set; }

        [DataMember(Name = "video")]
        public JsonMessageVideo Video { get; set; }

        [DataMember(Name = "audio")]
        public JsonMessageAudio Audio { get; set; }

        [DataMember(Name = "doc")]
        public JsonMessageDocument Document { get; set; }
    }

    [DataContract]
    public class JsonMessageGeo
    {
        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "coordinates")]
        public string Coordinates { get; set; }
    }

    [DataContract]
    public class JsonMessageDocument
    {
        [DataMember(Name = "did")]
        public long DID { get; set; }

        [DataMember(Name = "owner_id")]
        public long OwnerId { get; set; }

        [DataMember(Name = "size")]
        public long Size { get; set; }

        [DataMember(Name = "ext")]
        public string Ext { get; set; }

        [DataMember(Name = "url")]
        public string Url { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }
    }

    [DataContract]
    public class JsonMessageAudio
    {
        [DataMember(Name = "aid")]
        public long AID { get; set; }

        [DataMember(Name = "owner_id")]
        public long OwnerId { get; set; }

        [DataMember(Name = "url")]
        public string Url { get; set; }

        [DataMember(Name = "duration")]
        public long Duration { get; set; }

        [DataMember(Name = "performer")]
        public string Performer { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }
    }

    [DataContract]
    public class JsonMessageVideo
    {
        [DataMember(Name = "vid")]
        public long VID { get; set; }

        [DataMember(Name = "owner_id")]
        public long OwnerId { get; set; }

        [DataMember(Name = "image")]
        public string Url { get; set; }

        [DataMember(Name = "image_big")]
        public string UrlBig { get; set; }

        [DataMember(Name = "duration")]
        public long Duration { get; set; }        
    }

    [DataContract]
    public class JsonMessagePhoto
    {
        [DataMember(Name = "pid")]
        public long PID { get; set; }

        [DataMember(Name = "owner_id")]
        public long OwnerId { get; set; }

        [DataMember(Name = "src")]
        public string Url { get; set; }

        [DataMember(Name = "src_big")]
        public string UrlBig { get; set; }
    }

    [DataContract]
    public class JsonMessage
    {
        [DataMember(Name = "mid")]
        public long MID { get; set; }

        [DataMember(Name = "from_id")]
        public long FromId { get; set; }

        [DataMember(Name = "uid")]
        public long UID { get; set; }

        [DataMember(Name = "date")]
        public long Date { get; set; }

        [DataMember(Name = "body")]
        public string Body { get; set; }

        [DataMember(Name = "read_state")]
        public int ReadState { get; set; }

        [DataMember(Name = "out")]
        public int Out { get; set; }

        [DataMember(Name = "geo")]
        public JsonMessageGeo Geo { get; set; }

        public string Latitude
        {
            get
            {
                if (Geo == null || string.IsNullOrEmpty(Geo.Coordinates))
                    return string.Empty;

                var arr = Geo.Coordinates.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
                if (arr.Length != 2)
                    return string.Empty;

                return arr[0];
            }
        }

        public string Longitude
        {
            get
            {
                if (Geo == null || string.IsNullOrEmpty(Geo.Coordinates))
                    return string.Empty;

                var arr = Geo.Coordinates.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                if (arr.Length != 2)
                    return string.Empty;

                return arr[1];
            }
        }

        [DataMember(Name = "attachments")]
        public List<JsonMessageAttachment> Attachments { get; set; }
    }
}
