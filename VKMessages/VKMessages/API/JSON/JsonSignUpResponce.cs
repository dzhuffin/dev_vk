﻿using System.Runtime.Serialization;

namespace VKMessages.API.JSON
{
    [DataContract]
    public class JsonSignUpSid
    {
        [DataMember(Name = "sid")]
        public string SID { get; set; }
    }

    [DataContract]
    public class JsonSignUpResponce
    {
        [DataMember(Name = "response")]
        public JsonSignUpSid SID { get; set; }
    }
}
