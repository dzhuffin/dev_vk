﻿using System.Runtime.Serialization;

namespace VKMessages.API.JSON
{
    [DataContract]
    public class JsonCreateChatResponse
    {
        [DataMember(Name = "response")]
        public long ChatId { get; set; }
    }
}
