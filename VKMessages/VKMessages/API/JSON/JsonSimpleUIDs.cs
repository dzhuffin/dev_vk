﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace VKMessages.API.JSON
{
    [DataContract]
    public class JsonSimpleUIDs
    {
        [DataMember(Name = "response")]
        public List<int> Result { get; set; }
    }
}
