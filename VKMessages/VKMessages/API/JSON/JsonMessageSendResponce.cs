﻿using System.Runtime.Serialization;

namespace VKMessages.API.JSON
{
    [DataContract]
    public class JsonMessageSendResponce
    {
        [DataMember(Name = "response")]
        public long MID { get; set; }
    }
}
