﻿using System.Runtime.Serialization;

namespace VKMessages.API.JSON
{
    [DataContract]
    public class JsonGetPushSettings
    {
        [DataMember(Name = "disabled_until")]
        public long DisabledUntil { get; set; }
    }

    [DataContract]
    public class JsonGetPushSettingsResponse
    {
        [DataMember(Name = "response")]
        public JsonGetPushSettings Settings { get; set; }
    }
}
