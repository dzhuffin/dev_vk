﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace VKMessages.API.JSON
{
    [DataContract]
    public class JsonDialog
    {
        [DataMember(Name = "d_uid")]
        public List<long> DialogsUIDs { get; set; }

        [DataMember(Name = "p_uid")]
        public List<long> ProfilesUIDs { get; set; }

        [DataMember(Name = "first_name")]
        public List<string> FirstNames { get; set; }

        [DataMember(Name = "last_name")]
        public List<string> LastNames { get; set; }

        [DataMember(Name = "online")]
        public List<int> Status { get; set; }

        [DataMember(Name = "body")]
        public List<string> Bodies { get; set; }

        [DataMember(Name = "img_urls")]
        public List<string> Image50X50Urls { get; set; }        

        [DataMember(Name = "date")]
        public List<long> Dates { get; set; }

        [DataMember(Name = "read_state")]
        public List<int> ReadState { get; set; }

        [DataMember(Name = "out")]
        public List<int> Out { get; set; }

        [DataMember(Name = "title")]
        public List<string> Titles { get; set; }

        /////

        [DataMember(Name = "chat_id")]
        public List<long> ChatIds { get; set; }

        [DataMember(Name = "chat_active")]
        public List<string> ActiveChatUIds { get; set; }
    }
}
