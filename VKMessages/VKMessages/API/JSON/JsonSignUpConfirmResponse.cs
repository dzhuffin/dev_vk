﻿using System.Runtime.Serialization;

namespace VKMessages.API.JSON
{
    [DataContract]
    public class JsonSignUpConfirm
    {
        [DataMember(Name = "success")]
        public int Success { get; set; }

        [DataMember(Name = "uid")]
        public int UID { get; set; }
    }

    [DataContract]
    public class JsonSignUpConfirmResponse
    {
        [DataMember(Name = "response")]
        public JsonSignUpConfirm Data { get; set; }
    }
}
