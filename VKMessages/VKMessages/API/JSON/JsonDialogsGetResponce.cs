﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace VKMessages.API.JSON
{
    [DataContract]
    public class JsonDialogsGetResponce
    {
        [DataMember(Name = "response")]
        public JsonDialog Dialogs { get; set; }
    }
}
