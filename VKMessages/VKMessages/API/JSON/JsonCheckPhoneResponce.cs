﻿using System.Runtime.Serialization;

namespace VKMessages.API.JSON
{
    [DataContract]
    public class JsonCheckPhoneResponce
    {
        [DataMember(Name = "response")]
        public int Responce { get; set; }
    }
}
