﻿using System.Runtime.Serialization;

namespace VKMessages.API.JSON
{
    [DataContract]
    public class JsonUnreadMessagesCountResponce
    {
        [DataMember(Name = "response")]
        public int UnreadCount { get; set; }
    }
}
