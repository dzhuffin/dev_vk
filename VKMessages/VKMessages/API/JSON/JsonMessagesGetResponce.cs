﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace VKMessages.API.JSON
{
    [DataContract]
    public class JsonMessagesGetResponce
    {
        [DataMember(Name = "response")]
        public List<JsonMessage> Messages { get; set; }
    }
}
