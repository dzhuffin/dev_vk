﻿using System.Collections.Generic;
using System.Net;
using System.Runtime.Serialization;

namespace VKMessages.API.JSON
{
    [DataContract]
    public class JsonErrorRequestParam
    {
        [DataMember(Name = "key")]
        public string Key { get; set; }

        [DataMember(Name = "value")]
        public string Value { get; set; }
    }

    [DataContract]
    public class JsonError
    {
        [DataMember(Name = "error_code")]
        public int Code { get; set; }

        [DataMember(Name = "error_msg")]
        public string Message { get; set; }

        [DataMember(Name = "request_params")]
        public List<JsonErrorRequestParam> RequestParams { get; set; }

        [DataMember(Name = "captcha_sid")]
        public string CaptchaSID { get; set; }

        [DataMember(Name = "captcha_img")]
        public string CaptchaImageUrl { get; set; }

        public DownloadStringCompletedEventHandler EventHandler { get; set; }
    }

    [DataContract]
    public class JsonErrorResponce
    {
        [DataMember(Name = "error")]
        public JsonError Error { get; set; }
    }
}
