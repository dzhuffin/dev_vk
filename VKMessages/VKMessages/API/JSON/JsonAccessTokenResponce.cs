﻿using System.Runtime.Serialization;

namespace VKMessages.API.JSON
{
    [DataContract]
    public class JsonAccessTokenResponce
    {
        [DataMember(Name = "access_token")]
        public string AccessToken { get; set; }

        [DataMember(Name = "expires_in")]
        public int ExpiresIn { get; set; }

        [DataMember(Name = "user_id")]
        public long UserId { get; set; }
    }
}
