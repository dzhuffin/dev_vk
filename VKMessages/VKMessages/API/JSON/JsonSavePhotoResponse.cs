﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace VKMessages.API.JSON
{
    [DataContract]
    public class JsonSavePhotoData
    {
        [DataMember(Name = "photo_src")]
        public string PhotoUrl { get; set; }

        [DataMember(Name = "photo_src_big")]
        public string PhotoBigUrl { get; set; }

        [DataMember(Name = "photo_src_small")]
        public string PhotoSmallUrl { get; set; }
    }

    [DataContract]
    public class JsonSavePhotoResponse
    {
        [DataMember(Name = "response")]
        public JsonSavePhotoData PhotoData { get; set; }
    }

    [DataContract]
    public class JsonSaveMessagePhotoData
    {
        [DataMember(Name = "id")]
        public string Id { get; set; }

        [DataMember(Name = "pid")]
        public long PID { get; set; }

        [DataMember(Name = "aid")]
        public long AID { get; set; }

        [DataMember(Name = "owner_id")]
        public long OwnerId { get; set; }

        [DataMember(Name = "src")]
        public string Url { get; set; }

        [DataMember(Name = "src_big")]
        public string UrlBig { get; set; }

        [DataMember(Name = "src_small")]
        public string UrlSmall { get; set; }

        [DataMember(Name = "created")]
        public string Created { get; set; }
    }

    [DataContract]
    public class JsonSaveMessagePhotoResponse
    {
        [DataMember(Name = "response")]
        public List<JsonSaveMessagePhotoData> PhotoData { get; set; }
    }
}
