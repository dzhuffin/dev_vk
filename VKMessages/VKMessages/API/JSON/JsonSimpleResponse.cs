﻿using System.Runtime.Serialization;

namespace VKMessages.API.JSON
{
    [DataContract]
    public class JsonSimpleResponse
    {
        [DataMember(Name = "response")]
        public int Result { get; set; }
    }
}
