﻿using System;
using System.Collections.Generic;
using System.Linq;
using VKMessages.API.JSON;
using VKMessages.Tools;
using VKMessages.ViewModels;

namespace VKMessages.API
{
    public static class Requests
    {
        private static string vkAppId = "3080527";
        private static string vkAppSecretKey = "Ad9ue5a9YUmekyKy6aWt";
        //private static string vkLogin = "xaocut@bk.ru";
        //private static string vkPassword = "[fjcbn";

        private static string oauthUrlFormat = "https://oauth.vk.com/token?grant_type=password&scope=5126&client_id={0}&client_secret={1}&username={2}&password={3}";
        private static string friendsGetFormat = "https://api.vk.com/method/friends.get?fields=uid,first_name,last_name,rate,bdate,sex,city,country,photo,photo_medium,photo_medium_rec,photo_big,photo_rec,online,has_mobile,contacts,activity,can_write_private_message&order=name&access_token={0}&offset={1}";
        private static string friendDeleteFormat = "https://api.vk.com/method/friends.delete?access_token={0}&uid={1}";
        private static string friendAddFormat = "https://api.vk.com/method/friends.add?access_token={0}&uid={1}";        
        private static string checkTokenFormat = "https://api.vk.com/method/friends.get?fields=uid&count=1&access_token={0}";
        private static string checkPhoneFormat = "https://api.vk.com/method/auth.checkPhone?client_id={0}&client_secret={1}&phone={2}";
        private static string signUpFormat = "https://api.vk.com/method/auth.signup?client_id={0}&client_secret={1}&phone={2}&first_name={3}&last_name={4}";
        private static string confirmSignUpFormat = "https://api.vk.com/method/auth.confirm?client_id={0}&client_secret={1}&phone={2}&code={3}&password={4}";
        private static string messagesGetDialogsFormat = "https://api.vk.com/method/messages.getDialogs?preview_length=40&access_token={0}";
        private static string messagesGetById = "https://api.vk.com/method/messages.getById?access_token={0}&mid={1}";
        private static string messagesGetHistoryFormat = "https://api.vk.com/method/messages.getHistory?access_token={0}&count=20&uid={1}&offset={2}";
        private static string messagesGetChatHistoryFormat = "https://api.vk.com/method/messages.getHistory?access_token={0}&count=20&chat_id={1}&offset={2}";
        private static string messagesSendFormat = "https://api.vk.com/method/messages.send?access_token={0}&uid={1}&message={2}";
        private static string messagesChatSendFormat = "https://api.vk.com/method/messages.send?access_token={0}&chat_id={1}&message={2}";
        private static string messagesSendLocationFormat = "&lat={0}&long={1}";
        private static string messagesSendAttachmentsFormat = "&attachment={0}";
        private static string executeFormat = "https://api.vk.com/method/execute?access_token={0}&code={1}";
        private static string getLongPoolServerFormat = "https://api.vk.com/method/messages.getLongPollServer?access_token={0}";
        private static string requestToLongPoolServerFormat = "http://{0}?act=a_check&key={1}&ts={2}&wait=25&mode=2";
        private static string parseRequestFormat = "https://api.vk.com/method/{0}";
        private static string getByPhonesFormat = "https://api.vk.com/method/friends.getByPhones?access_token={0}&phones={1}&fields=uid,first_name,last_name,phone,mobile_phone,home_phone,online";
        private static string registerDeviceFormat = "https://api.vk.com/method/account.registerDevice?access_token={0}&token={1}";
        private static string unregisterDeviceFormat = "https://api.vk.com/method/account.unregisterDevice?access_token={0}&token={1}";
        private static string setSilenceModeFormat = "https://api.vk.com/method/account.setSilenceMode?access_token={0}&token={1}&time={2}";
        private static string getPushSettingsFormat = "https://api.vk.com/method/account.getPushSettings?access_token={0}&token={1}";
        private static string getProfileUploadServerFormat = "https://api.vk.com/method/photos.getProfileUploadServer?access_token={0}";
        private static string getMessagesUploadServerFormat = "https://api.vk.com/method/photos.getMessagesUploadServer?access_token={0}";
        private static string getCurrentProfileFormat = "https://api.vk.com/method/users.get?access_token={0}&uids={1}&fields=uid,first_name,last_name,photo_medium_rec";
        private static string setCurrentProfilePhotoFormat = "https://api.vk.com/method/photos.saveProfilePhoto?access_token={0}&server={1}&photo={2}&hash={3}";
        private static string setMessagePhotoFormat = "https://api.vk.com/method/photos.saveMessagesPhoto?access_token={0}&server={1}&photo={2}&hash={3}";
        private static string getRequestsCountFormat = "https://api.vk.com/method/friends.getRequests?access_token={0}";
        private static string makeAsReadFormat = "https://api.vk.com/method/messages.markAsRead?access_token={0}&mids={1}";
        private static string createChatFormat = "https://api.vk.com/method/messages.createChat?access_token={0}&uids={1}&title={2}";
        private static string editChatFormat = "https://api.vk.com/method/messages.editChat?access_token={0}&chat_id={1}&title={2}";

        public const string EditChatCodeFormat1 = "var res = API.messages.editChat({\"chat_id\":%%CHAT_ID%%,\"title\":\"%%TITLE%%\"});";
        public const string EditChatCodeFormat2 = " var a_count = %%A_COUNT%%; var a_uids=[%%ADD%%]; var i = 0; while (i<a_count) {API.messages.addChatUser({\"chat_id\":%%CHAT_ID%%,\"uid\":a_uids[i]}); i=i%2B1;};";
        public const string EditChatCodeFormat3 = " var d_count = %%D_COUNT%%; var d_uids=[%%DEL%%]; var j = 0; while (j<d_count) {API.messages.removeChatUser({\"chat_id\":%%CHAT_ID%%,\"uid\":d_uids[j]}); j=j%2B1;};";
        public const string EditChatCodeFormat4 = " return res;";
        public const string GetUnreadCountCodeFormat = "return API.messages.get({\"filters\":1})[0];";
        public const string DialogsGetCodeFormat = "var dialogs = API.messages.getDialogs({\"preview_length\":40,\"count\":40,\"offset\":%%OFFSET%%}); var u_all=dialogs@.uid; var profiles = API.users.get({\"uids\":u_all,\"fields\":\"uid,first_name,last_name,online,photo_rec\"});return {\"d_uid\":u_all, \"p_uid\":profiles@.uid, \"first_name\":profiles@.first_name, \"last_name\":profiles@.last_name, \"online\":profiles@.online, \"img_urls\":profiles@.photo_rec, \"body\":dialogs@.body, \"date\":dialogs@.date, \"read_state\":dialogs@.read_state, \"chat_id\":dialogs@.chat_id, \"chat_active\":dialogs@.chat_active, \"title\":dialogs@.title, \"out\":dialogs@.out};";
        public const string FriendsGetCodeFormat = "var dialogs = API.messages.getDialogs({\"preview_length\":40,\"count\":40}); var u_all=dialogs@.uid; var profiles = API.users.get({\"uids\":u_all,\"fields\":\"uid,first_name,last_name,online,photo_rec\"}); var friends = API.friends.get({\"fields\":\"uid,first_name,last_name,rate,bdate,sex,city,country,photo,photo_medium,photo_medium_rec,photo_big,photo_rec,online,has_mobile,contacts,activity,can_write_private_message\",\"order\":\"name\"}); var rate_friends = API.friends.get({\"fields\":\"uid,rate\",\"order\":\"hints\",\"count\":5}); return {\"friends\":friends,\"rate_friends\":rate_friends, \"d_uid\":u_all, \"p_uid\":profiles@.uid, \"first_name\":profiles@.first_name, \"last_name\":profiles@.last_name, \"online\":profiles@.online, \"img_urls\":profiles@.photo_rec, \"body\":dialogs@.body, \"date\":dialogs@.date, \"read_state\":dialogs@.read_state, \"chat_id\":dialogs@.chat_id, \"chat_active\":dialogs@.chat_active, \"title\":dialogs@.title, \"out\":dialogs@.out};";
        public const string FriendRequestsGetCodeFormat = "var suggestions = API.friends.getSuggestions({\"count\":20}); var requests = API.friends.getRequests({\"out\":0}); return {\"requests\":API.users.get({\"uids\":requests@.uid,\"fields\":\"uid,first_name,last_name,online,photo_rec,photo_medium_rec\"}),\"suggestions\":API.users.get({\"uids\":suggestions@.uid,\"fields\":\"uid,first_name,last_name,online,photo_rec,photo_medium_rec\"})};";
        public const string FriendSuggestionsGetCodeFormat = "var suggestions = API.friends.getSuggestions({\"count\":20,\"offset\":%%OFFSET%%}); return API.users.get({\"uids\":suggestions@.uid,\"fields\":\"uid,first_name,last_name,online,photo_rec,photo_medium_rec\"});";

        public static Uri GetExecuteUri(string code)
        {
            return new Uri(string.Format(executeFormat, SharedPreferences.AccessToken, code));
        }

        public static Uri GetEditChatUri(long chat_id, string title)
        {
            return new Uri(string.Format(editChatFormat, SharedPreferences.AccessToken, chat_id, title));
        }

        public static Uri GetEditChatUri(long chat_id, string title, IEnumerable<long> a_uids, IEnumerable<long> d_uids)
        {
            var a_list = a_uids.Aggregate(string.Empty, (list, uid) => list + "," + uid);
            if (!string.IsNullOrEmpty(a_list) && a_list[0] == ',' && a_list.Length > 1)
                a_list = a_list.Substring(1, a_list.Length - 1);

            var d_list = d_uids.Aggregate(string.Empty, (list, uid) => list + "," + uid);
            if (!string.IsNullOrEmpty(d_list) && d_list[0] == ',' && d_list.Length > 1)
                d_list = d_list.Substring(1, d_list.Length - 1);

            var code = EditChatCodeFormat1.Replace("%%CHAT_ID%%", chat_id.ToString()).Replace("%%TITLE%%", title);
            if (a_uids.Count() > 0)
                code += EditChatCodeFormat2.Replace("%%A_COUNT%%", a_uids.Count().ToString()).Replace("%%ADD%%", a_list).Replace("%%CHAT_ID%%", chat_id.ToString());
            if (d_uids.Count() > 0)
                code += EditChatCodeFormat3.Replace("%%D_COUNT%%", d_uids.Count().ToString()).Replace("%%DEL%%", d_list).Replace("%%CHAT_ID%%", chat_id.ToString());
            code += EditChatCodeFormat4;

            return new Uri(string.Format(executeFormat, SharedPreferences.AccessToken, code));
        }

        public static Uri GetMakeAsReadUri(IEnumerable<long> mids)
        {
            return new Uri(string.Format(makeAsReadFormat, SharedPreferences.AccessToken,
                mids.Aggregate(string.Empty, (list, mid) => list + "," + mid)));
        }

        public static Uri GetCreateChatUri(IEnumerable<long> uids, string title)
        {
            return new Uri(string.Format(createChatFormat, SharedPreferences.AccessToken,
                uids.Aggregate(string.Empty, (list, uid) => list + "," + uid), title));
        }

        public static Uri GetFriendAddUri(long uid)
        {
            return new Uri(string.Format(friendAddFormat, SharedPreferences.AccessToken, uid));
        }

        public static Uri GetMessagesById(long mid)
        {
            return new Uri(string.Format(messagesGetById, SharedPreferences.AccessToken, mid));
        }        

        public static Uri GetFriendDeleteUri(long uid)
        {
            return new Uri(string.Format(friendDeleteFormat, SharedPreferences.AccessToken, uid));
        }

        public static Uri GetRequestsCountUri()
        {
            return new Uri(string.Format(getRequestsCountFormat, SharedPreferences.AccessToken));
        }

        public static Uri GetSetSilenceModeUri(string token, long time)
        {
            return new Uri(string.Format(setSilenceModeFormat, SharedPreferences.AccessToken, token, time));
        }

        public static Uri GetPushSettingsUri(string token)
        {
            return new Uri(string.Format(getPushSettingsFormat, SharedPreferences.AccessToken, token));
        }

        public static Uri GetSaveProfilePhotoUri(long server, string photo, string hash)
        {
            return new Uri(string.Format(setCurrentProfilePhotoFormat, SharedPreferences.AccessToken, server, photo, hash));
        }

        public static Uri GetSaveMessagePhotoUri(long server, string photo, string hash)
        {
            return new Uri(string.Format(setMessagePhotoFormat, SharedPreferences.AccessToken, server, photo, hash));
        }        

        public static Uri GetProfileUploadServerUri()
        {
            return new Uri(string.Format(getProfileUploadServerFormat, SharedPreferences.AccessToken));
        }

        public static Uri GetMessagesUploadServerUri()
        {
            return new Uri(string.Format(getMessagesUploadServerFormat, SharedPreferences.AccessToken));
        }        

        public static Uri GetCurrentProfileUri()
        {
            return new Uri(string.Format(getCurrentProfileFormat, SharedPreferences.AccessToken, SharedPreferences.UserId));
        }

        public static Uri GetRegisterDeviceUri(string token)
        {
            return new Uri(string.Format(registerDeviceFormat, SharedPreferences.AccessToken, token));
        }

        public static Uri GetUnregisterDeviceUri(string token)
        {
            return new Uri(string.Format(unregisterDeviceFormat, SharedPreferences.AccessToken, token));
        }

        public static Uri ParseRequestParams(string code, string sid, List<JsonErrorRequestParam> requestParams)
        {
            JsonErrorRequestParam oauthParam = null;
            JsonErrorRequestParam methodParam = null;
            foreach (var jsonErrorRequestParam in requestParams)
            {
                if (jsonErrorRequestParam.Key == "oauth")
                    oauthParam = jsonErrorRequestParam;

                if (jsonErrorRequestParam.Key == "method")
                    methodParam = jsonErrorRequestParam;

                if (oauthParam != null && methodParam != null)
                    break;
            }

            if (oauthParam == null || methodParam == null)
                return new Uri(string.Empty);

            requestParams.Remove(oauthParam);
            requestParams.Remove(methodParam);

            var url = string.Format(parseRequestFormat, methodParam.Value);

            var isFirstParam = true;
            foreach (var jsonErrorRequestParam in requestParams)
            {
                if (isFirstParam)
                {
                    url += "?" + jsonErrorRequestParam.Key + "=" + jsonErrorRequestParam.Value;
                    isFirstParam = false;
                }
                else
                    url += "&" + jsonErrorRequestParam.Key + "=" + jsonErrorRequestParam.Value;
            }

            if (isFirstParam)
                url += "?captcha_sid=" + sid + "&captcha_key=" + code;
            else
                url += "&captcha_sid=" + sid + "&captcha_key=" + code;

            return new Uri(url);
        }

        public static Uri GetLongPoolServerUri()
        {
            return new Uri(string.Format(getLongPoolServerFormat, SharedPreferences.AccessToken));
        }

        public static Uri GetByPhonesUri(string phonesList)
        {
            return new Uri(string.Format(getByPhonesFormat, SharedPreferences.AccessToken, phonesList));
        }

        public static Uri GetRequestToLongPoolServerUri(string server, string key, long ts)
        {
            return new Uri(string.Format(requestToLongPoolServerFormat, server, key, ts));
        }

        public static Uri GetOAuthUri(string login, string password)
        {
            return new Uri(string.Format(oauthUrlFormat, vkAppId, vkAppSecretKey, login, password));
        }

        public static Uri GetFriendsGetUri(int offset)
        {
            return new Uri(string.Format(friendsGetFormat, SharedPreferences.AccessToken, offset));
        }

        public static Uri GetMessagesSendUri(long uid, string message, List<Attachment> attachments, bool isChat)
        {
            var locationStr = string.Empty;
            var attachmentStr = string.Empty;

            if (attachments != null && attachments.Count > 0)
            {
                attachmentStr = attachments.Where(x => x.Type == AttachmentType.Photo).Aggregate(string.Empty,
                                                                                                 (list, id) =>
                                                                                                 list + "," + id.Id);
                if (!string.IsNullOrEmpty(attachmentStr) && attachmentStr[0] == ',' && attachmentStr.Length > 1)
                    attachmentStr = attachmentStr.Substring(1, attachmentStr.Length - 1);
            }

            if (!string.IsNullOrEmpty(attachmentStr))
                attachmentStr = string.Format(messagesSendAttachmentsFormat, attachmentStr);

            if (attachments != null && attachments.Count > 0)
            {
                var location = attachments.FirstOrDefault(x => x.Type == AttachmentType.Location);
                if (location != null)
                    locationStr = string.Format(messagesSendLocationFormat, location.Latitude, location.Longitude);
            }

            return new Uri(string.Format(isChat ? messagesChatSendFormat + locationStr + attachmentStr :
                                                  messagesSendFormat + locationStr + attachmentStr, 
                            SharedPreferences.AccessToken, uid, message));
        }

        public static Uri GetMessagesGetUri(long uid, int offset, bool isChat)
        {
            return new Uri(string.Format(isChat ? 
                                         messagesGetChatHistoryFormat : 
                                         messagesGetHistoryFormat, SharedPreferences.AccessToken, uid, offset));
        }

        public static Uri GetDialogsUri()
        {
            return new Uri(string.Format(messagesGetDialogsFormat, SharedPreferences.AccessToken));
        }

        public static Uri GetCheckTokenUri()
        {
            return new Uri(string.Format(checkTokenFormat, SharedPreferences.AccessToken));
        }

        public static Uri GetCheckPhoneUri(string phone)
        {
            return new Uri(string.Format(checkPhoneFormat, vkAppId, vkAppSecretKey, phone));
        }

        public static Uri GetSignUpUri(string phone, string firstname, string lastname)
        {
            return new Uri(string.Format(signUpFormat, vkAppId, vkAppSecretKey, phone, firstname, lastname));
        }
        
        public static Uri GetSignUpConfirmUri(string phone, string code, string password)
        {
            return new Uri(string.Format(confirmSignUpFormat, vkAppId, vkAppSecretKey, phone, code, password));
        }
    }
}
