﻿using System.ComponentModel;
using System.Windows;

namespace VKMessages.Resources
{
    public class ThemeResourceDictionary : ResourceDictionary
    {
        private ResourceDictionary lightResources;
        private ResourceDictionary darkResources;

        public ResourceDictionary LightResources
        {
            get { return lightResources; }
            set
            {
                lightResources = value;
 
                if (!IsDarkTheme && value != null)
                {
                    MergedDictionaries.Add(value);
                }
            }
        }
 
        public ResourceDictionary DarkResources
        {
            get { return darkResources; }
            set
            {
                darkResources = value;
 
                if (IsDarkTheme && value != null)
                {
                    MergedDictionaries.Add(value);
                }
            }
        }

        private bool IsDarkTheme
        {
            get
            {
                if (IsDesignMode)
                {
                    return true;
                }
                else
                {
                    return (Visibility)Application.Current.Resources["PhoneDarkThemeVisibility"] == Visibility.Visible;
                }
            }
        }

        private bool IsDesignMode
        {
            get
            {
                return DesignerProperties.GetIsInDesignMode(this) || DesignerProperties.IsInDesignTool;
            }
        }
    }
}
