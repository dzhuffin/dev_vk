﻿namespace VKMessages.Resources
{
    public class LocalizedStrings
    {
        private static readonly Resource localizedResources = new Resource();

        public Resource LocalizedResources { get { return localizedResources; } }
    }
}
