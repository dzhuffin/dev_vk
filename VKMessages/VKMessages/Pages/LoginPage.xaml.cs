﻿using Microsoft.Phone.Controls;

namespace VKMessages.Pages
{
    public partial class LoginPage : PhoneApplicationPage
    {
        public LoginPage()
        {
            InitializeComponent();

            DataContext = App.LoginViewModel;
        }
    }
}