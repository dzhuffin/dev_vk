﻿using System;
using System.Windows.Controls;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using VKMessages.Tools;
using VKMessages.ViewModels;

namespace VKMessages.Pages
{
    public partial class ChatPage : PhoneApplicationPage
    {
        public ChatPage()
        {
            InitializeComponent();

            BuildApplicationBar();

            App.ChatViewModel.BuildApplicationBar = BuildApplicationBar;
            DataContext = App.ChatViewModel;
        }

        private void MapTap(object sender, Microsoft.Phone.Controls.GestureEventArgs e)
        {
            var vm = DataContext as ChatViewModel;
            var image = sender as Image;
            if (vm == null || image == null)
                return;

            vm.MapClick(image.Tag.ToString());
        }

        private void PlayTap(object sender, Microsoft.Phone.Controls.GestureEventArgs e)
        {
            var image = sender as Image;
            if (image == null)
                return;

            var vm = image.DataContext as Attachment;
            if (vm == null)
                return;

            vm.PlayClick();
        }

        private void PauseTap(object sender, Microsoft.Phone.Controls.GestureEventArgs e)
        {
            var image = sender as Image;
            if (image == null)
                return;

            var vm = image.DataContext as Attachment;
            if (vm == null)
                return;

            vm.PauseClick();
        }

        private void BuildApplicationBar()
        {
            ApplicationBar = new ApplicationBar();
            var ib = new ApplicationBarIconButton(new Uri("/Images/Components/Appbar_Icons/appbar.send.text.rest.png", UriKind.Relative));
            ib.Text = Resource.APPBAR_SEND;
            ib.Click += App.ChatViewModel.AppBarSend;
            ApplicationBar.Buttons.Add(ib);

            if (CrossPageParameters.AttachmentsCount < 1)
            {
                ib =
                    new ApplicationBarIconButton(
                        new Uri("/Images/Components/Appbar_Icons/appbar.feature.camera.rest.png", UriKind.Relative));
                ib.Text = Resource.APPBAR_ADD_PHOTO;
                ib.Click += App.ChatViewModel.AppBarAddPhoto;
                ApplicationBar.Buttons.Add(ib);

                ib =
                    new ApplicationBarIconButton(new Uri("/Images/Components/Appbar_Icons/appbar.checkin.rest.png",
                                                         UriKind.Relative));
                ib.Text = Resource.APPBAR_CHECKIN;
                ib.Click += App.ChatViewModel.AppBarChackIn;
                ApplicationBar.Buttons.Add(ib);
            }

            if (CrossPageParameters.AttachmentsCount > 0 && CrossPageParameters.AttachmentsCount < 11)
            {
                ib =
                    new ApplicationBarIconButton(
                        new Uri(string.Format("/Images/Components/Appbar_Icons/appbar.attachments-{0}.rest.png", CrossPageParameters.AttachmentsCount), UriKind.Relative));
                ib.Text = Resource.APPBAR_ATTACHMENTS;
                ib.Click += App.ChatViewModel.AppBarAddPhoto;
                ApplicationBar.Buttons.Add(ib);
            }

            var settingsItem = new ApplicationBarMenuItem(Resource.APPBAR_EDIT);
            settingsItem.Click += App.ChatViewModel.AppBarEdit;
            ApplicationBar.MenuItems.Add(settingsItem);
        }

        private void PhotoTap(object sender, Microsoft.Phone.Controls.GestureEventArgs e)
        {
            var image = sender as Image;
            if (image == null)
                return;

            var vm = image.DataContext as Attachment;
            if (vm == null)
                return;

            vm.PhotoClick();
        }

        private void VideoTap(object sender, Microsoft.Phone.Controls.GestureEventArgs e)
        {
            var image = sender as Image;
            if (image == null)
                return;

            var vm = image.DataContext as Attachment;
            if (vm == null)
                return;

            vm.VideoClick();
        }
    }
}