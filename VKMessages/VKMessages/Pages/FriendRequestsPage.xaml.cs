﻿using Microsoft.Phone.Controls;

namespace VKMessages.Pages
{
    public partial class FriendRequestsPage : PhoneApplicationPage
    {
        public FriendRequestsPage()
        {
            InitializeComponent();

            DataContext = App.FriendsRequestViewModel;
        }
    }
}