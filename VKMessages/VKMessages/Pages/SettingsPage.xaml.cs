﻿using Microsoft.Phone.Controls;

namespace VKMessages.Pages
{
    public partial class SettingsPage : PhoneApplicationPage
    {
        public SettingsPage()
        {
            InitializeComponent();

            DataContext = App.SettingsViewModel;
        }
    }
}