﻿using Microsoft.Phone.Controls;

namespace VKMessages.Pages
{
    public partial class PreviewPage : PhoneApplicationPage
    {
        public PreviewPage()
        {
            InitializeComponent();

            DataContext = App.PreviewViewModel;
        }
    }
}