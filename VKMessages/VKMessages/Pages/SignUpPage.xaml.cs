﻿using Microsoft.Phone.Controls;

namespace VKMessages.Pages
{
    public partial class SignUpPage : PhoneApplicationPage
    {
        public SignUpPage()
        {
            InitializeComponent();

            DataContext = App.SignUpViewModel;
        }
    }
}