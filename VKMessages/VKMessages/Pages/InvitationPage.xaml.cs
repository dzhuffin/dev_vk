﻿using Microsoft.Phone.Controls;

namespace VKMessages.Pages
{
    public partial class InvitationPage : PhoneApplicationPage
    {
        public InvitationPage()
        {
            InitializeComponent();

            DataContext = App.InvitationViewModel;
        }
    }
}