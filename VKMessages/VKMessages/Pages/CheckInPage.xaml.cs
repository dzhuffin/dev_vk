﻿using System;
using System.Device.Location;
using System.Windows;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Controls.Maps;
using Microsoft.Phone.Controls.Maps.Platform;
using Microsoft.Phone.Shell;
using VKMessages.Tools;
using VKMessages.ViewModels;

namespace VKMessages.Pages
{
    public partial class CheckInPage : PhoneApplicationPage
    {
        private bool IsEditEnable = true;
        private bool firstRun;
        private GeoCoordinate MyLocation;
        private readonly Pushpin pin;
        private readonly GeoCoordinateWatcher gv;

        public CheckInPage()
        {
            InitializeComponent();

            BuildApplicationBar();

            this.firstRun = true;
            this.MyLocation = new GeoCoordinate();
            var pushpin = new Pushpin {Location = new Location()};
            this.pin = pushpin;
            this.gv = new GeoCoordinateWatcher();
            this.InitializeComponent();
            this.gv.Start();
            this.gv.StatusChanged += this.gvStatusChanged;
            this.gv.PositionChanged += this.GvPositionChanged;
            this.MyMap.Children.Add(this.pin);
        }

        private void BuildApplicationBar()
        {
            ApplicationBar = new ApplicationBar();
            var ib = new ApplicationBarIconButton(new Uri("/Images/Components/Appbar_Icons/appbar.save.rest.png", UriKind.Relative));
            ib.Text = Resource.APP_OK;
            ib.Click += AppBarOk;
            ApplicationBar.Buttons.Add(ib);
        }

        public void AppBarOk(object sender, EventArgs e)
        {
            CrossPageParameters.PickedLocation = this.pin.Location;
            BaseViewModel.GoBack();
        }

        private void MyMap_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (this.IsEditEnable)
            {
                var position = e.GetPosition(this.MyMap);
                var location = this.MyMap.ViewportPointToLocation(position);
                this.MyMap.Center = location;
                this.pin.Location = location;
            }
        }

        private void GvPositionChanged(object sender, GeoPositionChangedEventArgs<GeoCoordinate> e)
        {
            this.MyLocation = e.Position.Location;
            if (this.firstRun)
            {
                this.pin.Location = this.gv.Position.Location;
                this.MyMap.Center = this.gv.Position.Location;
                this.MyMap.ViewportPointToLocation(this.MyMap.LocationToViewportPoint(this.gv.Position.Location));
                this.firstRun = false;
            }
        }

        private void gvStatusChanged(object sender, GeoPositionStatusChangedEventArgs e)
        {
            if (e.Status != GeoPositionStatus.Initializing)
            {
                if (e.Status != GeoPositionStatus.Ready)
                {
                    if (e.Status != GeoPositionStatus.NoData)
                    {
                        if (e.Status == GeoPositionStatus.Disabled)
                        {
                            MessageBox.Show(Resource.CHECK_IN_DISABLED);
                        }
                        return;
                    }
                    else
                    {
                        MessageBox.Show(Resource.CHECK_IN_FAILED);
                        return;
                    }
                }
                else
                {
                    return;
                }
            }
            else
            {
                return;
            }
        }

        private void PhoneApplicationPage_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            CrossPageParameters.PickedLocation = null;
        }
    }
}