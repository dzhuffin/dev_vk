﻿using Microsoft.Phone.Controls;

namespace VKMessages.Pages
{
    public partial class ConfirmPage : PhoneApplicationPage
    {
        public ConfirmPage()
        {
            InitializeComponent();

            DataContext = App.ConfirmViewModel;
        }
    }
}