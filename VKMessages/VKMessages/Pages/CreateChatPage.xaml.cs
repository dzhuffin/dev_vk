﻿using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using VKMessages;

namespace VKMessages.Pages
{
    public partial class CreateChatPage : PhoneApplicationPage
    {
        public CreateChatPage()
        {
            InitializeComponent();

            BuildApplicationBar();

            DataContext = App.CreateChatViewModel;
        }

        private void BuildApplicationBar()
        {
            foreach (var button in ApplicationBar.Buttons)
            {
                var ib = button as ApplicationBarIconButton;
                if (ib == null)
                    continue;

                if (ib.IconUri.ToString().Contains("appbar.save.rest.png"))
                {
                    ib.Text = Resource.APPBAR_SAVE;
                    ib.Click += App.CreateChatViewModel.AppBarSave;
                    continue;
                }
            }

            //var settingsItem = ApplicationBar.MenuItems[0] as ApplicationBarMenuItem;
            //if (settingsItem != null)
            //{
            //    settingsItem.Text = Resource.APPBAR_SETTINGS;
            //    settingsItem.Click += App.ViewModel.AppBarSettingsShow;
            //}
        }
    }
}