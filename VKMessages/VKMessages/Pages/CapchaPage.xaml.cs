﻿using Microsoft.Phone.Controls;

namespace VKMessages.Pages
{
    public partial class CapchaPage : PhoneApplicationPage
    {
        public CapchaPage()
        {
            InitializeComponent();

            DataContext = App.CapchaViewModel;
        }
    }
}