﻿using Microsoft.Phone.Controls;

namespace VKMessages.Pages
{
    public partial class SearchPage : PhoneApplicationPage
    {
        public SearchPage()
        {
            InitializeComponent();

            DataContext = App.SearchViewModel;
        }
    }
}