﻿using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace VKMessages.Pages
{
    public partial class ContactPage : PhoneApplicationPage
    {
        public ContactPage()
        {
            InitializeComponent();

            var deleteFriendItem = ApplicationBar.MenuItems[0] as ApplicationBarMenuItem;
            if (deleteFriendItem != null)
            {
                deleteFriendItem.Text = Resource.FRIEND_DELETE;
                deleteFriendItem.Click += App.ContactViewModel.DeleteFriend;
            }

            App.ContactViewModel.DeleteFriendItem = deleteFriendItem;

            DataContext = App.ContactViewModel;
        }
    }
}