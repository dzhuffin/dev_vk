﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using VKMessages.Controls;
using VKMessages.Tools;
using VKMessages.ViewModels;

namespace VKMessages.Pages
{
    public partial class ConversationPage : PhoneApplicationPage
    {
        public ConversationPage()
        {
            InitializeComponent();

            //BuildApplicationBar(0);

            App.ConversationViewModel.BuildApplicationBar = BuildApplicationBar;
            DataContext = App.ConversationViewModel;
        }

        private void BuildApplicationBar()
        {
            ApplicationBar = new ApplicationBar();
            var ib = new ApplicationBarIconButton(new Uri("/Images/Components/Appbar_Icons/appbar.send.text.rest.png", UriKind.Relative));
            ib.Text = Resource.APPBAR_SEND;
            ib.Click += App.ConversationViewModel.AppBarSend;
            ApplicationBar.Buttons.Add(ib);

            if (CrossPageParameters.AttachmentsCount < 1)
            {
                ib =
                    new ApplicationBarIconButton(
                        new Uri("/Images/Components/Appbar_Icons/appbar.feature.camera.rest.png", UriKind.Relative));
                ib.Text = Resource.APPBAR_ADD_PHOTO;
                ib.Click += App.ConversationViewModel.AppBarAddPhoto;
                ApplicationBar.Buttons.Add(ib);

                ib =
                    new ApplicationBarIconButton(new Uri("/Images/Components/Appbar_Icons/appbar.checkin.rest.png",
                                                         UriKind.Relative));
                ib.Text = Resource.APPBAR_CHECKIN;
                ib.Click += App.ConversationViewModel.AppBarChackIn;
                ApplicationBar.Buttons.Add(ib);
            }

            if (CrossPageParameters.AttachmentsCount > 0 && CrossPageParameters.AttachmentsCount < 11)
            {
                ib =
                    new ApplicationBarIconButton(
                        new Uri(string.Format("/Images/Components/Appbar_Icons/appbar.attachments-{0}.rest.png", CrossPageParameters.AttachmentsCount), UriKind.Relative));
                ib.Text = Resource.APPBAR_ATTACHMENTS;
                ib.Click += App.ConversationViewModel.AppBarAddPhoto;
                ApplicationBar.Buttons.Add(ib);
            }
        }

        private void MapTap(object sender, Microsoft.Phone.Controls.GestureEventArgs e)
        {
            var vm = DataContext as ConversationViewModel;
            var image = sender as Image;
            if (vm == null || image == null)
                return;

            vm.MapClick(image.Tag.ToString());
        }

        private void PlayTap(object sender, Microsoft.Phone.Controls.GestureEventArgs e)
        {
            var image = sender as Image;
            if (image == null)
                return;

            var vm = image.DataContext as Attachment;
            if (vm == null)
                return;

            vm.PlayClick();
        }

        private void PauseTap(object sender, Microsoft.Phone.Controls.GestureEventArgs e)
        {
            var image = sender as Image;
            if (image == null)
                return;

            var vm = image.DataContext as Attachment;
            if (vm == null)
                return;

            vm.PauseClick();
        }

        private void PhotoTap(object sender, Microsoft.Phone.Controls.GestureEventArgs e)
        {
            var image = sender as Image;
            if (image == null)
                return;

            var vm = image.DataContext as Attachment;
            if (vm == null)
                return;

            vm.PhotoClick();
        }

        private void VideoTap(object sender, Microsoft.Phone.Controls.GestureEventArgs e)
        {
            var image = sender as Image;
            if (image == null)
                return;

            var vm = image.DataContext as Attachment;
            if (vm == null)
                return;

            vm.VideoClick();
        }

        private void MessagesListBox_Loaded(object sender, RoutedEventArgs e)
        {         
        }
    }
}