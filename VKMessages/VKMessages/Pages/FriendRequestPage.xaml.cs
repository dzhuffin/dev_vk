﻿using Microsoft.Phone.Controls;

namespace VKMessages.Pages
{
    public partial class FriendRequestPage : PhoneApplicationPage
    {
        public FriendRequestPage()
        {
            InitializeComponent();

            DataContext = App.FriendRequestViewModel;
        }
    }
}