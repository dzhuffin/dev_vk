﻿using System;
using System.Windows;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace VKMessages.Pages
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();

            BuildApplicationBar();

            // Set the data context of the listbox control to the sample data
            DataContext = App.ViewModel;
            this.Loaded += MainPage_Loaded;
        }

        // Load data for the ViewModel Items
        private void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            if (!App.ViewModel.IsDataLoaded)
            {
                App.ViewModel.LoadData();
            }
        }

        private void BuildApplicationBar()
        {
            foreach (var button in ApplicationBar.Buttons)
            {
                var ib = button as ApplicationBarIconButton;
                if (ib == null)
                    continue;

                if (ib.IconUri.ToString().Contains("appbar.refresh.rest"))
                {
                    ib.Text = Resource.APPBAR_REFRESH;
                    ib.Click += App.ViewModel.AppBarRefresh;
                    continue;
                }

                if (ib.IconUri.ToString().Contains("appbar.feature.search.rest"))
                {
                    ib.Text = Resource.APPBAR_SEARCH;
                    ib.Click += App.ViewModel.AppBarSearch;
                    continue;
                }

                if (ib.IconUri.ToString().Contains("appbar.add.rest"))
                {
                    ib.Text = Resource.APPBAR_CREATE_CHAT;
                    ib.Click += App.ViewModel.AppBarAdd;
                    continue;
                }
            }

            var settingsItem = ApplicationBar.MenuItems[0] as ApplicationBarMenuItem;
            if (settingsItem != null)
            {
                settingsItem.Text = Resource.APPBAR_SETTINGS;
                settingsItem.Click += App.ViewModel.AppBarSettingsShow;
            }

            //var appBarButtonAdd = new ApplicationBarIconButton(new Uri("/img/add.png", UriKind.Relative)) { Text = AppResources.ABAdd };
            //appBarButtonAdd.Click += newEntry_Click;
            //ApplicationBar.Buttons.Add(appBarButtonAdd);
        }
    }
}