﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using VKMessages.ViewModels;

namespace VKMessages.Pages
{
    public partial class AttachmentsPage : PhoneApplicationPage
    {
        public AttachmentsPage()
        {
            InitializeComponent();

            BuildApplicationBar();

            DataContext = App.AttachmentsViewModel;

            App.AttachmentsViewModel.BuildApplicationBar = BuildApplicationBar;
        }

        private void BuildApplicationBar()
        {
            ApplicationBar = new ApplicationBar();
            ApplicationBarIconButton ib = null;

            ib = new ApplicationBarIconButton(new Uri("/Images/Components/Appbar_Icons/appbar.feature.camera.rest.png", UriKind.Relative));
            ib.Text = Resource.APPBAR_ADD_PHOTO;
            ib.Click += App.AttachmentsViewModel.AppBarAddPhoto;
            ApplicationBar.Buttons.Add(ib);

            if (!App.AttachmentsViewModel.Attachments.Any(x => x.Type == AttachmentType.Location))
            {
                ib = new ApplicationBarIconButton(new Uri("/Images/Components/Appbar_Icons/appbar.checkin.rest.png", UriKind.Relative));
                ib.Text = Resource.APPBAR_CHECKIN;
                ib.Click += App.AttachmentsViewModel.AppBarChackIn;
                ApplicationBar.Buttons.Add(ib);
            }         
        }
    }
}