﻿using System;
using System.Collections;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace VKMessages.Controls
{
    public class StretchingEventArgs : EventArgs
    {
        public bool IsTop { get; set; }
    }

    public class FixedLongListSelector : Microsoft.Phone.Controls.LongListSelector
    {
        public FixedLongListSelector()
        {
            SelectionChanged += FixedLongListSelector_SelectionChanged;
            Loaded += FixedLongListSelector_Loaded;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        private void FixedLongListSelector_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                var tlb = VisualTreeHelper.GetChild((FixedLongListSelector)sender, 0) as FrameworkElement;
                if (tlb == null)
                    return;
                var sv = VisualTreeHelper.GetChild(tlb, 0) as FrameworkElement;
                if (sv == null)
                    return;
                var here = VisualTreeHelper.GetChild(sv, 0) as FrameworkElement;
                if (here == null)
                    return;
                var groups = VisualStateManager.GetVisualStateGroups(here);
                var vc = groups.Cast<VisualStateGroup>().FirstOrDefault(g => g.Name == "VerticalCompression");
                vc.CurrentStateChanged += CurrentStateChanged;
            }
            catch (Exception)
            {
            }
        }

        private void CurrentStateChanged(object sender, VisualStateChangedEventArgs e)
        {
            if (this.Stretching == null)
                return;

            if (e.NewState.Name == "CompressionTop")
            {
                this.Stretching.Invoke(this, new StretchingEventArgs { IsTop = true });
                return;
            }

            if (e.NewState.Name == "CompressionBottom")
            {
                this.Stretching.Invoke(this, new StretchingEventArgs { IsTop = false });
                return;
            }
        }

        public delegate void StretchingEventHandler(object sender, StretchingEventArgs e);
        public event StretchingEventHandler Stretching;


        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        void FixedLongListSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectedItem = base.SelectedItem;
        }

        public IEnumerable ItemsSourceEx
        {
            get { return (IEnumerable)GetValue(ItemsSourceExProperty); }
            set { SetValue(ItemsSourceExProperty, value); }
        }

        public static readonly DependencyProperty ItemsSourceExProperty =
            DependencyProperty.Register("ItemsSourceEx", typeof(IEnumerable), typeof(FixedLongListSelector), new PropertyMetadata(null, OnItemsSourceExChanged));

        private static void OnItemsSourceExChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var selector = (FixedLongListSelector)obj;

            selector.ItemsSource = selector.ItemsSourceEx;

            if (selector.ItemsSourceChanged != null)
                selector.ItemsSourceChanged.Invoke(selector, new EventArgs());
        }

        public event EventHandler ItemsSourceChanged;

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static readonly DependencyProperty SelectedItemProperty = DependencyProperty.Register("SelectedItem", typeof(object), typeof(FixedLongListSelector),
        new PropertyMetadata(null, OnSelectedItemChanged));

        private static void OnSelectedItemChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var selector = (FixedLongListSelector)d;
            selector.SelectedItem = e.NewValue;
        }

        public new object SelectedItem
        {
            get { return GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }
}
